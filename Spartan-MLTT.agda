{-# OPTIONS --without-K --exact-split --safe #-}


-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`



module Spartan-MLTT where

open import Universes public

variable
 𝓤 𝓥 𝓦 𝓣 : Universe

data 𝟙 : 𝓤₀ * where
 ⋆ : 𝟙

𝟙-induction : (A : 𝟙 → 𝓤  * ) → A ⋆ → (x : 𝟙) → A x
𝟙-induction A a ⋆ = a

𝟙-recursion : (B : 𝓤 * ) → B → (𝟙 → B)
𝟙-recursion B b x = 𝟙-induction (λ _ → B) b x

data 𝟘 :  𝓤₀ * where

𝟘-induction : (A : 𝟘 → 𝓤 *) → (x : 𝟘) → A x
𝟘-induction A ()

𝟘-recursion : (A : 𝓤 *) → 𝟘 → A
𝟘-recursion A a = 𝟘-induction (λ _ → A) a
!𝟘 = 𝟘-recursion

is-empty : 𝓤 * →  𝓤 *
is-empty X = X → 𝟘

¬ : 𝓤 * →  𝓤 *
¬ X = X → 𝟘

¬¬ : 𝓤 * → 𝓤 *
¬¬ X = ¬ (¬ X)

¬¬¬ : 𝓤 * → 𝓤 *
¬¬¬ X = ¬ (¬ (¬ X))

data 𝔹 : 𝓤₀ * where
 𝕥 : 𝔹
 𝕗 : 𝔹

𝔹-induction : (A : 𝔹 → 𝓤 *) → (A 𝕥) → (A 𝕗) → (b : 𝔹) → A b
𝔹-induction A if else 𝕥 = if 
𝔹-induction A if else 𝕗 = else 

𝔹-recursion : (A : 𝓤 *) → A → A → (b : 𝔹) → A
𝔹-recursion A if else v = 𝔹-induction (λ - → A) if else v


data ℕ : 𝓤₀ * where
 zero : ℕ
 succ : ℕ → ℕ

ℕ-induction : (A : ℕ → 𝓤 *)
            → A zero
            → ((n : ℕ) → A n → A (succ n))
            → (n : ℕ) → A n

ℕ-induction A a₀ f = h
 where
  h : (n : ℕ) → A n
  h zero        = a₀
  h (succ n) = f n (h n)

ℕ-recursion : (X : 𝓤 *)
            → X
            → (ℕ → X → X)
            → ℕ → X

ℕ-recursion X = ℕ-induction (λ _ → X)

ℕ-iteration : (X : 𝓤 *)
            → X
            → (X → X)
            → ℕ → X

ℕ-iteration X x f = ℕ-recursion X x (λ _ x → f x)

one two : ℕ
one = succ zero
two = succ one


data _+_ {𝓤 𝓥} (X : 𝓤 *) (Y : 𝓥 *) : 𝓤 ⊔ 𝓥 * where
 inl : X → X + Y
 inr : Y → X + Y

+-induction : {X : 𝓤 *} {Y : 𝓥 *} (A : X + Y → 𝓦 *)
            → ((x : X) → A (inl x))
            → ((y : Y) → A (inr y))
            → (z : X + Y) → A z

+-induction A f g (inl x) = f x
+-induction A f g (inr y) = g y

+-recursion : {X : 𝓤 *} {Y : 𝓥 *} {A : 𝓦 *} → (X → A) → (Y → A) → X + Y → A
+-recursion {𝓤} {𝓥} {𝓦} {X} {Y} {A} = +-induction (λ _ → A)

𝟚 : 𝓤₀ *
𝟚 = 𝟙 + 𝟙

pattern ₀ = inl ⋆
pattern ₁ = inr ⋆

𝟚-induction : (A : 𝟚 → 𝓤 *) → A ₀ → A ₁ → (n : 𝟚) → A n
𝟚-induction A a₀ a₁ ₀ = a₀
𝟚-induction A a₀ a₁ ₁ = a₁

𝟚-induction' : (A : 𝟚 → 𝓤 *) → A ₀ → A ₁ → (n : 𝟚) → A n
𝟚-induction' A a₀ a₁ = +-induction A
                         (𝟙-induction (λ (x : 𝟙) → A (inl x)) a₀)
                         (𝟙-induction (λ (y : 𝟙) → A (inr y)) a₁)

record Σ {𝓤 𝓥} {X : 𝓤 *} (Y : X → 𝓥 *) : 𝓤 ⊔ 𝓥 * where
  --no-eta-equality -- To be closer to the HoTT book --removed because I get an agda error (I guess I changed versions and something happened)
  constructor
   _,_
  field
   x : X
   y : Y x

pr₁ : {X : 𝓤 *} {Y : X → 𝓥 *} → Σ Y → X
pr₁ (x , y) = x

pr₂ : {X : 𝓤 *} {Y : X → 𝓥 *} → (z : Σ Y) → Y (pr₁ z)
pr₂ (x , y) = y

-Σ : {𝓤 𝓥 : Universe} (X : 𝓤 *) (Y : X → 𝓥 *) → 𝓤 ⊔ 𝓥 *
-Σ X Y = Σ Y

+Σ : {𝓤 𝓥 : Universe} (X : 𝓤 *) (Y : X → 𝓥 *) → 𝓤 ⊔ 𝓥 *
+Σ X Y = Σ Y

syntax -Σ X (λ x → y) = Σ x ꞉ X , y

-- notation stolen from 1lab
syntax +Σ X (λ x → y) = Σ[ x ∈ X ]  y

Σ-induction : {X : 𝓤 *} {Y : X → 𝓥 *} {A : Σ Y → 𝓦 *}
            → ((x : X) (y : Y x) → A (x , y))
            → (p : Σ Y) → A p

Σ-induction g (x , y) = g x y

curry : {X : 𝓤 *} {Y : X → 𝓥 *} {A : Σ Y → 𝓦 *}
      → (((x , y) : Σ Y) → A (x , y))
      → ((x : X) (y : Y x) → A (x , y))

curry f x y = f (x , y)

_×_ : 𝓤 * → 𝓥 * → 𝓤 ⊔ 𝓥  *
X × Y = Σ x ꞉ X , Y

Σ-recursion : {X : 𝓤 *} {Y : X → 𝓥 *} {A : 𝓦 *}
            → ((x : X) (y : Y x) → A)
            → Σ Y → A
Σ-recursion {X = X} {Y = Y} {A = A} f p = Σ-induction {X = X} {Y = Y} {A = λ - → A} f p     

Π : {X : 𝓤 *} (A : X → 𝓥 *) → 𝓤 ⊔ 𝓥 *
Π {𝓤} {𝓥} {X} A = (x : X) → A x

-Π : {𝓤 𝓥 : Universe} (X : 𝓤 *) (Y : X → 𝓥 *) → 𝓤 ⊔ 𝓥 *
-Π X Y = Π Y

+Π : {𝓤 𝓥 : Universe} (X : 𝓤 *) (Y : X → 𝓥 *) → 𝓤 ⊔ 𝓥 *
+Π X Y = Π Y

syntax -Π A (λ x → b) = Π x ꞉ A , b

-- notation stolen from 1lab
syntax +Π X (λ x → y) = Π[ x ∈ X ]  y

id : {X : 𝓤 *} → X → X
id x = x

𝑖𝑑 : (X : 𝓤 *) → X → X
𝑖𝑑 X = id

_∘_ : {X : 𝓤 *} {Y : 𝓥 *} {Z : Y → 𝓦 *}
    → ((y : Y) → Z y)
    → (f : X → Y)
    → (x : X) → Z (f x)

g ∘ f = λ x → g (f x)

domain : {X : 𝓤 *} {Y : 𝓥 *} → (X → Y) →  𝓤 *
domain {𝓤} {𝓥} {X} {Y} f = X

codomain : {X : 𝓤 *} {Y : 𝓥 *} → (X → Y) → 𝓥 * 
codomain {𝓤} {𝓥} {X} {Y} f = Y

type-of : {X : 𝓤 *} → X →  𝓤 *
type-of {𝓤} {X} x = X

data Id {𝓤} (X : 𝓤 *) : X → X → 𝓤 * where
 refl : (x : X) → Id X x x

_≡_ : {X : 𝓤 *} → X → X →  𝓤 *
x ≡ y = Id _ x y

_≢_ : {X : 𝓤 *} → X → X →  𝓤 *
x ≢ y = ¬ (x ≡ y)

𝕁 : (X : 𝓤 *) (A : (x y : X) → x ≡ y → 𝓥 *)
  → ((x : X) → A x x (refl x))
  → (x y : X) (p : x ≡ y) → A x y p

𝕁 X A f x x (refl x) = f x

lhs : {X : 𝓤 *} {x y : X} → x ≡ y → X
lhs {𝓤} {X} {x} {y} p = x

rhs : {X : 𝓤 *} {x y : X} → x ≡ y → X
rhs {𝓤} {X} {x} {y} p = y

𝕁' : {X : 𝓤 *} (A : (x y : X) → x ≡ y → 𝓥 *)
  → ((x : X) → A x x (refl x))
  → {x y : X} (p : x ≡ y) → A x y p
𝕁' {X = X} C c p = 𝕁 X C c (lhs p) (rhs p) p

𝕁† : {X : 𝓤 *} (A : {x y : X} → x ≡ y → 𝓥 *)
  → ((x : X) → A (refl x))
  → {x y : X} (p : x ≡ y) → A p
𝕁† C c p = 𝕁' (λ x y p → C p) c p

ℍ : {X : 𝓤 *} (x : X) (B : (y : X) → x ≡ y → 𝓥 *)
  → B x (refl x)
  → (y : X) (p : x ≡ y) → B y p

ℍ x B b x (refl x) = b


infixr 50 _,_
infixr 30 _×_
infixr 20 _+_
infixl 70 _∘_
infix   0 Id
infix   0 _≡_
infixr -1 -Σ
infixr -1 -Π




-- For now I just consider everything below to be black box magic

record ULift {𝓤 : Universe} (𝓥 : Universe) (X : 𝓤 *) : 𝓤 ⊔ 𝓥 *   where
 constructor
  Ulift
 field
  Ulower : X

open ULift public

type-of-ULift  :             type-of (ULift  {𝓤} 𝓥)       ≡ (𝓤 * → 𝓤 ⊔ 𝓥 *)
type-of-Ulift  : {X : 𝓤 *} → type-of (Ulift  {𝓤} {𝓥} {X}) ≡ (X → ULift 𝓥 X)
type-of-Ulower : {X : 𝓤 *} → type-of (Ulower {𝓤} {𝓥} {X}) ≡ (ULift 𝓥 X → X)

type-of-ULift  = refl _
type-of-Ulift  = refl _
type-of-Ulower = refl _


-- Very basic example

-- Lifting a type
lol-fb : (𝓤₀ ⊔ 𝓤) *
lol-fb {𝓤 = 𝓤} =  ULift 𝓤 𝟙 

-- moving its elements to the lifted type
lol-fbi : lol-fb {𝓤 = 𝓤}
lol-fbi = Ulift ⋆ 
