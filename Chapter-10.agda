
{-# OPTIONS --without-K --exact-split --show-implicit #-}



module Chapter-10 where


open import Chapter-05 public




-- Lemma 10.2.9

injective : {A : 𝓤 *} {B : 𝓥 *} → (f : A → B) → ((𝓤 ⊔ 𝓥 ) )  *
injective {A = A} {B = B} f = Π[ b ∈ B ] ((fiber f b) isProp) --  λ (a₀ a₁ : A) → isequiv {A = a₀ ≡ a₁} {B = f a₀ ≡ f a₁} (ap {X = A} {Y = B} f {x₀ = a₀} {x₁ = a₁}) 

postulate injective-to-embedding :  {A : 𝓤 *} {B : 𝓥 *} → (f : A → B) → injective f → (a₀ a₁ : A) →  isequiv (ap f {x₀ = a₀} {x₁ = a₁})
postulate embedding-to-injective :  {A : 𝓤 *} {B : 𝓥 *} → (f : A → B) → ((a₀ a₁ : A) →  isequiv (ap f {x₀ = a₀} {x₁ = a₁})) → injective f

surjective : {A : 𝓤 *} {B : 𝓥 *} → (f : A → B) →  (𝓤 ⊔ 𝓥) *
surjective {A = A} {B = B} f = Π[ b ∈ B ] ∥ fiber f b ∥


left-inv-to-surjective : {A : 𝓤 *} {B : 𝓥 *} (f : A → B) (g : B → A) → (g ∘ f ∼ id) → surjective g
left-inv-to-surjective f g H b = ∣ (f b , H b) ∣

right-inv-to-injective : {A : 𝓤 *} {B : 𝓥 *} (Bset : B isSet) (f : A → B) (g : B → A) → (g ∘ f ∼ id) → injective f
right-inv-to-injective Bset f g H b (a₀ , p₀) (a₁ , p₁) =
  Σ≡→≡ ((a₀ , p₀))
       ((a₁ , p₁))
       ( (((H a₀)⁻¹) ∙ (ap g (p₀ ∙ p₁ ⁻¹)) ∙ (H a₁))
       , Bset (f a₁) (b) (tr (λ x →  f x ≡ b) ((((H a₀)⁻¹) ∙ (ap g (p₀ ∙ p₁ ⁻¹)) ∙ (H a₁))) p₀) p₁)

surjection-to-mere-injection : {A : 𝓤 *} {B : 𝓥 *} (Aset : A isSet) (Bset : B isSet)
                             → (f : A → B) → surjective f → (alt-ac : alt-AC 𝓥  (𝓤 ⊔ 𝓥))
                             → ∥ (Σ[ g ∈ (B → A) ] (injective g)) ∥
surjection-to-mere-injection {A = A} {B = B} Aset Bset f fsurj alt-ac =
  ∥∥-recursion
    ∥∥-isProp
    (λ (G : Π[ b ∈ B ] fiber f b) → ∣ to-g G , right-inv-to-injective Aset (to-g G) f (to-H G)  ∣  )
    merely-G
 where

  postulate fibers-are-sets : (b : B) →  (fiber f b) isSet
  --fibers-are-sets b (a₀ , p₀) (a₁ , p₁) e₀ e₁ =
  {-  e₀ e₁, up to equivalence, are pairs of paths: (αᵢ : a₀ = a₁, βᵢ : tr …).
      both the αᵢs and Pᵢs live in path types of A resp. B, hence must be equal.
      So we get   e₀               ≡⟨ Σ≡-α e₀ ⟩
                  (Σ≡→≡) (≡→Σ≡ e₀) ≡⟨ by def ⟩
                  (Σ≡→≡) (α₀ , β₀) ≡⟨ap Σ≡→≡ (Aset a₀ a₁ α₀ α₁ , Bset … … (tr (Aset a₀ a₁ α₀ α) P₀) (P₁) ) ⟩
                  (Σ≡→≡) (α₁ , β₁) ≡⟨ by def⟩
                  (Σ≡→≡) (≡→Σ≡ e₁) ≡⟨Σ≡-α⟩
                  e₁               ∎
  -}

  merely-G : ∥ Π[ b ∈ B ] fiber f b ∥
  merely-G = alt-ac B (λ b → fiber f b) Bset (λ b → fibers-are-sets b) fsurj

  to-g : ((b : B) →  fiber f b) → B → A
  to-g  G b = fst (G b)

  to-H : (G : (b : B) → fiber f b) →  f ∘ (to-g G) ∼ id
  to-H G b = (snd (G b))  
  




injection-to-surjection : {A : 𝓤 *} {B : 𝓥 *} (Aset : A isSet) (a₀ : A) (Bset : B isSet)
                             → (f : A → B) → injective f → (lem : LEM {𝓤 ⊔ 𝓥})
                             → (Σ[ g ∈ (B → A) ] (surjective g))
injection-to-surjection {A = A} {B = B} Aset a₀ Bset f injf lem = g , surjg
 where

  decfib = λ (b : B) → (fiber f b) + ¬(fiber f b)

  g-aux : (b : B) → decfib b → A
  g-aux b choice = +-recursion
          {X = (fiber f b)}
          {Y = ¬(fiber f b)}
          {A = A}
          (λ (x : fiber f b) → fst x)
          (λ (y : ¬ (fiber f b)) → a₀)
          choice
  g : B → A
  g b = g-aux b (lem (fiber f b) (injf b))


  surjg : surjective g
  surjg a = +-induction
            {X = fiber f (f a)}
            {Y = ¬ (fiber f (f a))}
            (λ (z : decfib (f a)) → (p : z ≡ lem (fiber f (f a)) (injf (f a))) → ∥ fiber g a ∥)
            (left a)
            (right a)
            (lem (fiber f (f a)) (injf (f a)))
            (refl (lem (fiber f (f a)) (injf (f a))))
   where
    right : (a : A) →  (z : ¬(fiber f (f a))) → (p : inr z ≡ lem (fiber f (f a)) (injf (f a))) → ∥ fiber g a ∥
    right a z p = !𝟘 (∥ fiber g a ∥) (z (a , refl (f a))) 
    left : (a : A) → (z : fiber f (f a)) → (p : inl z ≡ lem (fiber f (f a)) (injf (f a))) → ∥ fiber g a ∥
    left a z p = ∣   f a , (lol5 ∙ lol7)  ∣ 
     where
      lol5 : g (f a) ≡ fst z
      lol5 = ap (λ - → g-aux (f a) -) (p ⁻¹)
      lol6 : z ≡ (a , refl (f a))
      lol6 = injf (f a) z (a , refl (f a))
      lol7 : fst z ≡ a
      lol7 = ap fst lol6


-- Same as above but with some shortcuts taken
injection-to-surjection' : {A : 𝓤 *} {B : 𝓥 *} (Aset : A isSet) (a₀ : A) (Bset : B isSet)
                             → (f : A → B) → injective f → (lem : LEM {𝓤 ⊔ 𝓥})
                             → (Σ[ g ∈ (B → A) ] (surjective g))
injection-to-surjection' {A = A} {B = B} Aset a₀ Bset f injf lem = g , surjg
 where
  g : B → A
  g b =  +-recursion (fst) (λ - → a₀) (lem (fiber f b) (injf b))



  surjg : surjective g
  surjg a = +-induction
            (λ (z : fiber f (f a) + ¬(fiber f (f a))) → (p : z ≡ lemval a) → ∥ fiber g a ∥)
            (left a)
            (right a)
            (lemval a)
            (refl (lemval a))
   where
    lemval = λ a →  lem (fiber f (f a)) (injf (f a))

    right : (a : A) →  (z : ¬(fiber f (f a))) → (p : inr z ≡ lemval a) → ∥ fiber g a ∥
    right a z p = !𝟘 (∥ fiber g a ∥) (z (a , refl (f a))) 
    left : (a : A) → (z : fiber f (f a)) → (p : inl z ≡ lemval a) → ∥ fiber g a ∥
    left a z p = ∣ f a , (gfa-eq-fstz ∙ fstz-eq-a) ∣ 
     where
      gfa-eq-fstz : g (f a) ≡ fst z
      gfa-eq-fstz = ap (+-recursion (fst) (λ - → a₀)) (p ⁻¹)
      z-eq-a-refl : z ≡ (a , refl (f a))
      z-eq-a-refl = injf (f a) z (a , refl (f a))
      fstz-eq-a : fst z ≡ a
      fstz-eq-a = ap fst z-eq-a-refl

-- Same as above but we build a left inverse, and then use `left-inv-to-surjective`
injection-to-surjection'' : {A : 𝓤 *} {B : 𝓥 *} (Aset : A isSet) (a₀ : A) (Bset : B isSet)
                             → (f : A → B) → injective f → (lem : LEM {𝓤 ⊔ 𝓥})
                             → (Σ[ g ∈ (B → A) ] (surjective g))
injection-to-surjection'' {A = A} {B = B} Aset a₀ Bset f injf lem = g , (left-inv-to-surjective f g H) 
 where
  g : B → A
  g b =  +-recursion (fst) (λ - → a₀) (lem (fiber f b) (injf b))

  H : g ∘ f ∼ id
  H a = +-induction
            (λ (z : fiber f (f a) + ¬(fiber f (f a))) → (p : z ≡ lemval a) → g (f a) ≡ a)
            (left a)
            (right a)
            (lemval a)
            (refl (lemval a))
   where
    lemval = λ a →  lem (fiber f (f a)) (injf (f a))

    right : (a : A) →  (z : ¬(fiber f (f a))) → (p : inr z ≡ lemval a) →  g (f a) ≡ a
    right a z p = !𝟘 (g (f a) ≡ a) (z (a , refl (f a))) 
    left : (a : A) → (z : fiber f (f a)) → (p : inl z ≡ lemval a) →  g (f a) ≡ a
    left a z p = gfa-eq-fstz ∙ fstz-eq-a
     where
      gfa-eq-fstz : g (f a) ≡ fst z
      gfa-eq-fstz = ap (+-recursion (fst) (λ - → a₀)) (p ⁻¹)
      z-eq-a-refl : z ≡ (a , refl (f a))
      z-eq-a-refl = injf (f a) z (a , refl (f a))
      fstz-eq-a : fst z ≡ a
      fstz-eq-a = ap fst z-eq-a-refl


-- Corollary 10-2-13 (just a piece)
δ : {A : 𝓤₀ *} (Aset : A isSet)  (lem : LEM {𝓤₀}) → A → A → 𝔹
δ Aset lem a b = +-recursion (λ - → 𝕥) (λ - → 𝕗) (lem (a ≡ b) (Aset a b)) 

δ/𝕥 : {A : 𝓤₀ *} (Aset : A isSet)  (lem : LEM {𝓤₀}) → (a b : A) → (𝕥 ≡ δ Aset lem a b) → a ≡ b 
δ/𝕥 Aset lem a b 𝕥≡δab = +-induction (λ (z : ((a ≡ b) + (a ≢ b)) ) → (p : lem (a ≡ b) (Aset a b) ≡ z) → a ≡ b)
                                     (λ z p → z)
                                     (λ z p → !𝟘 (a ≡ b) (𝕥≢𝕗 (𝕥≡δab ∙ ap (+-recursion (λ - → 𝕥) (λ - → 𝕗)) p) ))
                                     (lem (a ≡ b) (Aset a b))
                                     (refl (lem (a ≡ b) (Aset a b)))
                                       
                                       

δ/𝕗 : {A : 𝓤₀ *} (Aset : A isSet)  (lem : LEM {𝓤₀}) → (a b : A) → (δ Aset lem a b ≡ 𝕗) → a ≢ b 
δ/𝕗 Aset lem a b δab≡𝕗 = +-induction (λ (z : ((a ≡ b) + (a ≢ b)) ) → (p : z ≡ lem (a ≡ b) (Aset a b)) → a ≢ b)
                                     (λ z p →  !𝟘 (a ≢ b) (𝕥≢𝕗 (( (ap (+-recursion (λ - → 𝕥) (λ - → 𝕗)) p) ) ∙ δab≡𝕗)))
                                     (λ z p → z)
                                     (lem (a ≡ b) (Aset a b))
                                     (refl (lem (a ≡ b) (Aset a b)))
