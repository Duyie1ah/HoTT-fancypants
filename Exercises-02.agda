{-# OPTIONS --without-K --exact-split --show-implicit #-}



module Exercises-02 where


open import Chapter-02 public

  

-- p. 136
-- Exercise 2.10

exe-2-10 : (A : 𝓤 *) (B : A → 𝓥 *) (C : (Σ B) → 𝓦 *)
         → (Σ[ x ∈ A ] (Σ[ y ∈ B x ] C (x , y)) ) ≃ (Σ C)
exe-2-10 A B C = f , (g , α) , (h , β)
 where
  L = Σ[ x ∈ A ] Σ[ y ∈ B x ] C (x , y)
  R = Σ C
  f : L → R
  f (x , (y , z)) = (x , y) , z
  g : R → L
  g ((x , y) , z) = (x , (y , z))
  h = g

  α : f ∘ g ∼ 𝑖𝑑 R
  α ((x , y) , z) = refl ((x , y) , z)
  β : h ∘ f ∼ 𝑖𝑑 L
  β (x , (y , z)) = refl (x , (y , z))
  -- hahaha ezpz

-- p. 137
-- Exercise 2.13

bool→type : 𝟚 → 𝓤₀ *
bool→type = 𝟚-induction (λ _ → 𝓤₀ *) 𝟙 𝟘

₀≢₁' : ¬ (₀ ≡ ₁)
₀≢₁' p = tr bool→type p ⋆

!₀≡₁ : (A : 𝓤 *) → (p : ₀ ≡ ₁) → A
!₀≡₁ A p = !𝟘 A (₀≢₁' p)

id≃ : (𝟚 ≃ 𝟚)
id≃ = id , (id , refl) , (id , refl)

swap : 𝟚 → 𝟚
swap ₀ = ₁
swap ₁ = ₀
swap≃ : (𝟚 ≃ 𝟚)
swap≃ = swap , (swap , lol) , (swap , lol)
 where
  lol : swap ∘ swap ∼ id
  lol ₀ = refl ₀
  lol ₁ = refl ₁

≃₀→₀-is-id : (X : 𝟚 ≃ 𝟚) → (fst X) ₀ ≡ ₀ → id ≡ (fst X)
≃₀→₀-is-id (x , (y , a) , (z , b)) p = funext id∼x
 where
  id∼x = 𝟚-induction (λ (v : 𝟚) → (q : v ≡ x ₁) → id ∼ x)
                            (λ (q : ₀ ≡ x ₁) → !𝟘 (id ∼ x) (₀≢₁ ((b ₀) ⁻¹ ∙ (ap z (p ∙ q)) ∙ (b ₁) )))
                            (λ (q : ₁ ≡ x ₁) → 𝟚-induction (λ v → v ≡ x v) (p ⁻¹) q)
                            (x ₁) (refl (x ₁))
≃₀→₁-is-swap : (X : 𝟚 ≃ 𝟚) → (fst X) ₀ ≡ ₁ → swap ≡ (fst X)
≃₀→₁-is-swap (x , (y , a) , (z , b)) p = funext swap∼x
 where
  swap∼x = 𝟚-induction (λ (v : 𝟚) → (q : v ≡ x ₁) → swap ∼ x)
                            (λ (q : ₀ ≡ x ₁) → 𝟚-induction (λ v → swap v ≡ x v) (p ⁻¹) q)
                            (λ (q : ₁ ≡ x ₁) → !𝟘 (swap ∼ x) (₀≢₁ ((b ₀) ⁻¹ ∙ (ap z (p ∙ q)) ∙ (b ₁) )))
                            (x ₁) (refl (x ₁))

postulate equal-on-₀ : (X X' : 𝟚 ≃ 𝟚) → (fst X) ₀ ≡ (fst X') ₀ → (fst X) ≡ (fst X')
postulate equal-on-₁ : (X X' : 𝟚 ≃ 𝟚) → (fst X) ₁ ≡ (fst X') ₁ → (fst X) ≡ (fst X')
-- This abomination should eventually work, once the last holes are filled, but is it worth it?
-- The idea is not complicated: we do `𝟚-induction` on each possible value of both `x` and `x'` at `₀` and `₁`,
-- but for the induction to be successful we actually induct on a new variable `u u' v v'` and take an equality path
-- as parameter.
-- Probably at least one of the levels of induction is useless, but we can't just induct on the values at `₁`, because:
-- Assuming they are not equal, we want to derive a contradiction.
-- We know that the values at `₀` are equal, by hypothesis of our function, and it is enough to get some `₀ ≡ ₁` term
-- to get the desired contradiction.
-- But now, assuming we only have `r : x ₀ ≡ x' ₀` and `q : x ₁ ≡ ₀` and `q' : x' ₁ ≡ ₀`, we don't yet know which of `q`
-- and `q'` will be the contradictory one!
{- equal-on-₀ (x , (y , a) , (z , b)) (x' , (y' , a') , (z' , b')) r = funext x∼x'
 where
  x∼x' = 𝟚-induction (λ (u : 𝟚) → (p : u ≡ x ₀) → (u' : 𝟚) → (p' : u' ≡ x' ₀) →  (v : 𝟚) → (q : v ≡ x ₁) → (v' : 𝟚) → (q' : v' ≡ x' ₁) → x ∼ x')
              (λ (p : ₀ ≡ x ₀) → 𝟚-induction (λ (u' : 𝟚) → (p' : u' ≡ x' ₀) →  (v : 𝟚) → (q : v ≡ x ₁) → (v' : 𝟚) → (q' : v' ≡ x' ₁) → x ∼ x')
                                             (λ (p' : ₀ ≡ x' ₀) →   𝟚-induction (λ (v : 𝟚) → (q : v ≡ x ₁) → (v' : 𝟚) → (q' : v' ≡ x' ₁) → x ∼ x')
                                                                                 (λ (q : ₀ ≡ x ₁) → 𝟚-induction (λ (v' : 𝟚) → (q' : v' ≡ x' ₁) → x ∼ x')
                                                                                                                (λ (q' : ₀ ≡ x' ₁) →  𝟚-induction (λ v → x v ≡ x' v) r ((q ⁻¹) ∙ q'))
                                                                                                                (λ (q' : ₁ ≡ x' ₁) →  (!₀≡₁ (x ∼ x')  ((b ₀) ⁻¹ ∙ (ap z ((p ⁻¹) ∙ q)) ∙ (b ₁) ))))
                                                                                  (λ (q : ₁ ≡ x ₁) → 𝟚-induction (λ (v' : 𝟚) → (q' : v' ≡ x' ₁) → x ∼ x')
                                                                                                                 (λ (q' : ₀ ≡ x' ₁) → (!₀≡₁ (x ∼ x') (((b' ₀) ⁻¹ ∙ (ap z' ((p' ⁻¹) ∙ q')) ∙ (b' ₁) ))) )
                                                                                                                 (λ (q' : ₁ ≡ x' ₁) →   𝟚-induction (λ v → x v ≡ x' v) r ((q ⁻¹) ∙ q'))) )
                                             (λ (p' : ₁ ≡ x' ₀) → λ _ _ _ _ → !₀≡₁ (x ∼ x') (p ∙ r ∙ p' ⁻¹)))
              (λ (p : ₁ ≡ x ₀) → 𝟚-induction (λ (u' : 𝟚) → (p' : u' ≡ x' ₀) →  (v : 𝟚) → (q : v ≡ x ₁) → (v' : 𝟚) → (q' : v' ≡ x' ₁) → x ∼ x')
                                             (λ (p' : ₀ ≡ x' ₀) →    λ _ _ _ _ → !₀≡₁ (x ∼ x') ((p ∙ r ∙ p' ⁻¹) ⁻¹) )
                                             (λ (p' : ₁ ≡ x' ₀) → 𝟚-induction (λ (v : 𝟚) → (q : v ≡ x ₁) → (v' : 𝟚) → (q' : v' ≡ x' ₁) → x ∼ x')
                                                                                 (λ (q : ₀ ≡ x ₁) → 𝟚-induction (λ (v' : 𝟚) → (q' : v' ≡ x' ₁) → x ∼ x')
                                                                                                                (λ (q' : ₀ ≡ x' ₁) → {!!} )
                                                                                                                (λ (q' : ₁ ≡ x' ₁) → {!!} ))
                                                                                  (λ (q : ₁ ≡ x ₁) → 𝟚-induction (λ (v' : 𝟚) → (q' : v' ≡ x' ₁) → x ∼ x')
                                                                                                                 (λ (q' : ₀ ≡ x' ₁) → {!!} )
                                                                                                                 (λ (q' : ₁ ≡ x' ₁) →  {!!} )) ))
              (x ₀) (refl (x ₀)) (x' ₀) (refl (x' ₀)) (x ₁) (refl (x ₁)) (x' ₁) (refl (x' ₁))
-}

equal-on-something : (X X' : 𝟚 ≃ 𝟚) → (v : 𝟚) → (fst X) v ≡ (fst X') v → (fst X) ≡ (fst X')
equal-on-something X X' v p = 𝟚-induction (λ v → (fst X) v ≡ (fst X') v → fst X ≡ fst X')
                                          (λ p → equal-on-₀ X X' p)
                                          (λ p → equal-on-₁ X X' p)
                                          v p
_isProp' : (A : 𝓤 *) → 𝓤  *
A isProp' = (x x' : A) → x ≡ x'

postulate eqvProp : {A : 𝓤 *} {B : 𝓥 *} (f : A → B) → (isequiv f) isProp'

≃-fst≡→≡ : (X X' : 𝟚 ≃ 𝟚) → (fst X) ≡ (fst X') → X ≡ X'
≃-fst≡→≡ (X , e) (X' , e')  p =  Σ≡→≡ (X , e) (X' , e') (p , q)
 where
  q : tr isequiv p e ≡ e'
  q = eqvProp X' (tr isequiv p e) e'

≃-fst₀≡→≡ : (X X' : 𝟚 ≃ 𝟚) → (fst X) ₀ ≡ (fst X') ₀ → X ≡ X'
≃-fst₀≡→≡ X X' p = ≃-fst≡→≡ X X' (equal-on-₀ X X' p)



exe-2-13 : 𝟚 ≃ (𝟚 ≃ 𝟚)
exe-2-13 = f , (g , α) , (h , β)
 where

  f : 𝟚 → (𝟚 ≃ 𝟚)
  f ₀ = id≃
  f ₁ = swap≃

  g h : (𝟚 ≃ 𝟚) → 𝟚
  g (x , e) = x ₀
  h = g
  
  α : f ∘ g ∼ id
  α (x , e) = 𝟚-induction (λ (v : 𝟚) → (p : v ≡ x ₀) → f (x ₀) ≡ (x , e))
                                    (λ (p : ₀ ≡ x ₀) →  (ap f (p ⁻¹)) ∙  (≃-fst₀≡→≡ id≃ (x , e) p)  )
                                    (λ (p : ₁ ≡ x ₀) →  (ap f (p ⁻¹)) ∙ (≃-fst₀≡→≡ swap≃ (x , e) p)  )
                                    (x ₀) (refl (x ₀))
  β : h ∘ f ∼ id
  β ₀ = refl ₀
  β ₁ = refl ₁
