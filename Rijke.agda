{-# OPTIONS --without-K --exact-split --show-implicit #-}


module Rijke where

open import Chapter-02 public
open import Chapter-03 public
open import Exercises-03 public 

module Rijke-Exercises-03 where

  U = λ (x : ℕ) (y : ℕ) →  𝓤₀ *

  Eqℕ : (x : ℕ) → (y : ℕ) → (U x y)
  Eqℕ = ℕ-induction (λ (x : ℕ) → (y : ℕ) → U x y) z s 
   where
    z : (y : ℕ) → U zero y
    z =  ℕ-induction (λ (y : ℕ) → U zero y) zz zs
     where
      zz : U zero zero
      zz = 𝟙
      zs : (y : ℕ) → U zero y → U zero (succ y)
      zs = λ y V → 𝟘

    s : (x : ℕ) → ((y : ℕ) → U x y) → ((y : ℕ) → U (succ x) y)
    s x P = ℕ-induction (λ (y : ℕ) → U (succ x) y) s-x-eqx-z s-x-eqx-s
     where
      s-x-eqx-z : U (succ x) zero
      s-x-eqx-z = 𝟘
      s-x-eqx-s : (y : ℕ) → U (succ x) y → U (succ x) (succ y)
      s-x-eqx-s y Q = P y 

  Eqℕ-refl : (x : ℕ) → Eqℕ x x
  Eqℕ-refl zero = ⋆
  Eqℕ-refl (succ n) = Eqℕ-refl n
  -- Note: we can't do 
  --   Eqℕ-refl (succ n) = ⋆ 
  -- because Eqℕ-refl (succ n) must have type Eqℕ (succ n) (succ n) which is judgementally
  -- equal to Eqℕ n n, but not to 𝟙.

  Eqℕ-symm : (x y : ℕ) → Eqℕ x y → Eqℕ y x
  Eqℕ-symm zero zero anything = anything
  Eqℕ-symm zero (succ m) anything = anything
  Eqℕ-symm (succ n) zero anything = anything
  Eqℕ-symm (succ n) (succ m) anything = Eqℕ-symm n m anything

  Eqℕ-trans : (x y z : ℕ) → Eqℕ x y → Eqℕ y z → Eqℕ x z
  Eqℕ-trans zero zero zero a b = a
  Eqℕ-trans zero zero (succ m) a b = b
  Eqℕ-trans zero (succ n) zero a b = ⋆
  Eqℕ-trans zero (succ n) (succ m) a b = a
  Eqℕ-trans (succ k) zero zero a b = a
  Eqℕ-trans (succ k) zero (succ m) a b = !𝟘 (Eqℕ k m) a
  Eqℕ-trans (succ k) (succ n) zero a b = b
  Eqℕ-trans (succ k) (succ n) (succ m) a b = Eqℕ-trans k n m a b


  Rijke-Exe-3-4 : (R : ℕ → ℕ → 𝓤 *) (ρ : (n : ℕ) → R n n) → (m n : ℕ) → Eqℕ m n → R m n
  Rijke-Exe-3-4 R ρ zero zero e = ρ zero
  Rijke-Exe-3-4 R ρ zero (succ m) e = !𝟘 (R zero (succ m)) e
  Rijke-Exe-3-4 R ρ (succ m) zero e = !𝟘 (R (succ m) zero) e
  Rijke-Exe-3-4 R ρ (succ n) (succ m) e = Rijke-Exe-3-4 (λ x y → R (succ x) (succ y)) (λ x → ρ (succ x)) n m e
  -- neat… it took me some time

  Rijke-Exe-3-5 : (m n : ℕ) → (e : Eqℕ m n) → (f : ℕ → ℕ) → Eqℕ (f m) (f n)
  Rijke-Exe-3-5 zero zero e f = Eqℕ-refl (f zero) 
  Rijke-Exe-3-5 zero (succ n) e f = !𝟘 (Eqℕ (f zero) (f (succ n))) e
  Rijke-Exe-3-5 (succ m) zero e f = !𝟘 (Eqℕ (f (succ m)) (f zero)) e
  Rijke-Exe-3-5 (succ m) (succ n) e f = Rijke-Exe-3-5 m n e (λ x → f (succ x)) 
  -- Same idea as 3-4 seemingly.

  Rijke-Exe-3-5' : (f : ℕ → ℕ) (m n : ℕ) → Eqℕ m n → Eqℕ (f m) (f n)
  Rijke-Exe-3-5' f m n e = Rijke-Exe-3-5 m n e f


  -- Exe 3.6

  _≤_ : ℕ → ℕ → 𝓤₀ *
  zero ≤ zero = 𝟙
  zero ≤ (succ m) = 𝟙
  succ n ≤ zero = 𝟘
  succ n ≤ succ m = n ≤ m

  _<_ : ℕ → ℕ → 𝓤₀ *
  zero   < zero = 𝟘
  zero   < (succ m) = 𝟙
  succ n < zero = 𝟘
  succ n < succ m = n < m

  n<succn : (n : ℕ) → n < succ n
  n<succn zero = ⋆
  n<succn (succ n) = n<succn n

  <antirefl : (n : ℕ) → ¬(n < n)
  <antirefl zero e = e
  <antirefl (succ n) e = <antirefl n e

  -- 3.8

  Eq𝔹 : 𝔹 → 𝔹 → 𝓤₀ *
  Eq𝔹 𝕥 𝕥 = 𝟙
  Eq𝔹 𝕗 𝕗 = 𝟙
  Eq𝔹 𝕥 𝕗 = 𝟘
  Eq𝔹 𝕗 𝕥 = 𝟘

  Eq𝔹-refl : (b : 𝔹) → Eq𝔹 b b
  Eq𝔹-refl 𝕥 = ⋆ 
  Eq𝔹-refl 𝕗 = ⋆
  -- Note: I am tempted to just write
  --  Eq𝔹-refl x = ⋆
  -- but this doesn't typecheck, because `x` is not judgementally either `𝕗` or `𝕥`, so that the output type is not known… it's still unsettling

  Rijke-Exe-3-8-c : (R : 𝔹 → 𝔹 → 𝓤 *) (ρ : (b : 𝔹) → R b b) → (b c : 𝔹) → Eq𝔹 b c → R b c
  Rijke-Exe-3-8-c R ρ 𝕥 𝕥 e = ρ 𝕥 
  Rijke-Exe-3-8-c R ρ 𝕗 𝕗 e = ρ 𝕗 
  Rijke-Exe-3-8-c R ρ 𝕥 𝕗 e = !𝟘 (R 𝕥 𝕗) e 
  Rijke-Exe-3-8-c R ρ 𝕗 𝕥 e = !𝟘 (R 𝕗 𝕥) e





module Rijke-Lecture-05 where

  pair_eq : {A : 𝓤 *} {B : A → 𝓥 *} {s t : Σ B} → s ≡ t → Σ[ p ∈ fst s ≡ fst t ] (tr B p (snd s) ≡ (snd t))
  pair_eq {A = A} {B} {s} {s} (refl s) = (refl (fst s)) , (refl (snd s))

  postulate eq_pair : {A : 𝓤 *} {B : A → 𝓥 *} {s t : Σ B} → (Σ[ p ∈ fst s ≡ fst t ] (tr B p (snd s) ≡ (snd t))) → s ≡ t
  -- eq_pair {A = A} {B} {(s₁ , s₂)} {(t₁ , t₂)} = {!!}



module Rijke-Exercises-05 where


  module Rijke-Exe-5-1 (A : 𝓤 *) (a : A) where
    left = 𝟙-induction (λ - → A) a
    right = λ (- : 𝟙) → a

    r∼l : left ∼ right
    r∼l = 𝟙-induction (λ x → left x ≡ right x) (refl a)

  {-
  Rijke-Exe-5-2 (A B : 𝓤 *) (b : B) (x y : A) (p : x ≡ y) → …  
    constb : A → B
    constb a = b
    left : (x y : A) →  x ≡ y → constb x ≡ constb y
    left x y p = ap constb p
    right : (x y : A) → x ≡ y → constb x ≡ constb y
    right x y p = refl b
    r∼l : (x y : A) → left ∼ right
    r∼l x x (refl x) = {!refl (refl b)!}
  -}

  module Rijke-Exe-5-3 (A : 𝓤 *) (B : A → 𝓥 *) where

    f : (x y : A) (p : x ≡ y) → B x → B y
    f x y p = tr B p 
    g : (x y : A) (p : x ≡ y) → B y → B x
    g x y p = tr B (p ⁻¹)

    -- We actually have identities "on the nose" (propositionally)
    -- by path induction
    G : (x y : A) (p : x ≡ y) → (f x y p ∘ g x y p) ≡ id
    F : (x y : A) (p : x ≡ y) → (g x y p ∘ f x y p) ≡ id
    G x x (refl x) = refl id 
    F x x (refl x) = refl id 

    id→htpy : (ϕ ψ : Π B) → ϕ ≡ ψ → ϕ ∼ ψ
    id→htpy ϕ ϕ (refl ϕ) x = refl (ϕ x)

    {- I wanted to construct the homotopy f g ∼ id by hand using the "known properties" 
    -- of transport without using judgemental equality, but agda doesn't want to be helpful
    -- so I gave up.
    G' : (x y : A) (p : x ≡ y) → (f x y p ∘ g x y p) ∼ id
    G' x y p b = (f x y p ∘ g x y p) b  ≡⟨ refl ( (f x y p ∘ g x y p) b )  ⟩
           tr B p (tr B (p ⁻¹) b)  ≡⟨ refl ( (f x y p ∘ g x y p) b ) ⟩
           tr B {! (p ⁻¹ ∙ p)!} b        ≡⟨ {!!} ⟩ -- ⟨ {! transport-∙ ? ? !} ⟩
           tr B (refl y) b        ≡⟨ {!!} ⟩ -- ap (λ - → tr B - b) {! ∙-⁻¹-left p !} ⟩
           b               ∎
    -}

  module Rijke-Exe-5-6 where

    ! : 𝔹 → 𝔹
    ! 𝕗 = 𝕥
    ! 𝕥 = 𝕗

    !-invol : ! ∘ ! ∼ id
    !-invol 𝕥 = refl 𝕥
    !-invol 𝕗 = refl 𝕗

    bad : (f : 𝔹 → 𝔹) (e : hasqinv f) → ¬ (f 𝕥 ≡ f 𝕗)
    bad f (g , α , β ) p = 𝕥≢𝕗 ( (β 𝕥) ⁻¹ ∙ (ap g p) ∙ (β 𝕗) )


  module Rijke-Exe-5-8-+ (A B : 𝓤 *) where

    f : A + B → B + A
    f (inl a) = inr a
    f (inr b) = inl b

    g : B + A → A + B
    g (inl b) = inr b
    g (inr a) = inl a

    α : (x : B + A) → f (g x) ≡ x
    α (inl b) = refl (inl b)
    α (inr a) = refl (inr a)

  module Rijke-Exe-3-8-× (A B : 𝓤 *) where

    f : A × B → B × A
    f (a , b) = (b , a)

    g : B × A → A × B
    g (b , a) = (a , b)

    α : f ∘ g ∼ id
    α (b , a) = refl (b , a)


  module Rijke-Exe-5-10 (A : 𝓤 *) (B : A → 𝓥 *) (C : (a : A) → (b : B a) → 𝓦 *) where

    L = Σ[ p ∈ Σ B ] C (fst p) (snd p)
    R = Σ[ a ∈ A ] Σ[ b ∈ B a ] C a b


    L→R : L → R
    -- L→R ((a , b) , c) = (a , b , c)
    L→R = Σ-induction {X = Σ B}
                {Y = λ p → C (fst p) (snd p)}
                {A = λ q → R}
                (λ (p : Σ B)  (c : C (fst p) (snd p)) →
                 Σ-induction {X = A}
                             {Y = B}
                             {A = λ p → (c' : C (fst p) (snd p)) → R}
                             (λ a b (c' : C a b) → (a , b , c')) p c
                )
      

    R→L : R → L
    -- R→L (a , b , c) = (a , b) , c
    R→L = Σ-induction {X = A}
                {Y = λ a → Σ[ b ∈ B a ] C a b}
                {A = λ q → L}
                (λ a r →
                 Σ-induction {X = B a}
                             {Y = C a}
                             {A = λ r → L}
                             (λ b c → ((a , b) , c))
                             r
                )
                
    α : L→R ∘ R→L ∼ 𝑖𝑑 R
    α (a , b , c) = refl (a , b , c)

    β : R→L ∘ L→R ∼ 𝑖𝑑 L
    β ((a , b) , c) = refl ((a , b) , c)
    

module Rijke-Lecture-06 where


  fib : {A : 𝓤 *} {B : 𝓥 *} (f : A → B) (y : B) → (𝓤 ⊔ 𝓥) *
  fib f y = Σ[ x ∈ domain f ] (f x ≡ y)

  fib-contr→equiv : {A : 𝓤 *} {B : 𝓥 *} (f : A → B)
            → ((y : B) → (fib f y) isContr)
            → hasqinv f
  fib-contr→equiv {A = A} {B = B} f fib-contr =  g , α , β
   where
    center : (y : B) → fib f y
    center y = fst (fib-contr y)
    centrality : (y : B) → (X : fib f y) → (center y) ≡ X
    centrality y = snd (fib-contr y)

    g : B → A
    g y = fst (center y)

    α : (y : B) → f (g y) ≡ y
    α y = snd (center y)

    β : (x : A) → g (f x) ≡ x
    β x = ap fst q
     where
      X : fib f (f x)
      X = x ,  refl (f x)
      q : center (f x) ≡ X
      q =  centrality (f x) X


module Rijke-Exercises-06 where


  fib : {A : 𝓤 *} {B : 𝓥 *} (f : A → B) (y : B) → (𝓤 ⊔ 𝓥) *
  fib f y = Σ[ x ∈ domain f ] (f x ≡ y)

  module Rijke-Exe-6-1 (A : 𝓤 *) where
    contr→path : A isContr → (x y : A) → (x ≡ y) isContr
    contr→path (a , α) x y = (c x y , γ x y)
     where
      c = λ x y →  (α x) ⁻¹ ∙ (α y)
      γ : (x y : A) → (p : x ≡ y) → c x y ≡ p
      γ x x (refl x) = ∙-⁻¹-left ((α x))


  module Rijke-Exe-6-2-a-bwd (A : 𝓤 *) (a : A) (α : (b : A) → a ≡ b) where

    c-⋆ : A → 𝟙
    c-⋆ a = ⋆
    
    c-a : 𝟙 → A
    -- c-a b = a
    c-a ⋆ = a
    
    F : (x : A) → (c-a ∘ c-⋆) x ≡ x
    F x = α x

    G : (x : 𝟙) → (c-⋆ ∘ c-a) x ≡ x
    G ⋆ = refl ⋆


  module Rijke-Exe-6-6 (A : 𝓤 *) (B : 𝓥 *) (f : A → B) where

    fibs = Σ (fib f)

    e : A → fibs
    e a = (f a , a , refl (f a))

    ϕ : fibs → A
    ϕ (b , a , p) = a

    H : (a : A) → fst (e a) ≡ f a
    H a = refl (f a)

    α : e ∘ ϕ ∼ id
    α (b , a , p) = ℍ  (f a) (λ b p → (f a , a , refl (f a)) ≡ (b , a , p)) (refl (f a , a , refl (f a))) b p 

    β : ϕ ∘ e ∼ id
    β a = refl a




module Rijke-Lecture-07 where

  fib : {A : 𝓤 *} {B : 𝓥 *} (f : A → B) (y : B) → (𝓤 ⊔ 𝓥) *
  fib f y = Σ[ x ∈ domain f ] (f x ≡ y)

  module Lemma-7-1-2 (A : 𝓤 *) (B C : A → 𝓤 *) (f : (x : A) → B x → C x) where

    totf : Σ B → Σ C
    totf (a , b) = (a , f a b)

    ϕ : (a : A) (c : C a) → fib (f a) c → fib totf (a , c)
    ϕ a c (b , p) =  ℍ (f a b) (λ c p → fib totf (a , c)) ((a , b) , refl (a , f a b)) c p


    ψ : (a : A) (c : C a)  → fib totf (a , c) → fib (f a) c
    ψ a c ((x , y) , e) = ℍ (x , f x y) (λ t e → fib (f (fst t)) (snd t)) (y , refl (f x y)) (a , c) e

    -- Not enough energy to show ψ ∘ ϕ and ϕ ∘ ψ are homotopic to the identities

    postulate α : (a : A) (c : C a) → (ϕ a c ∘ ψ a c) ∼ id
    --α a c ((x , y) , e) =  ℍ (x , ?) (λ t' e' → (ϕ a c ∘ ψ a c) (t' , e') ≡ (t' , e')) {!!} (a , c) e

    β : (a : A) (c : C a) → (ψ a c ∘ ϕ a c) ∼ id
    β a c (b , p) = ℍ (f a b) (λ c p → (ψ a c ∘ ϕ a c) (b , p) ≡ (b , p)) (refl (b , (refl (f a b)))) c p


module Rijke-Exercises-07 where


  fib : {A : 𝓤 *} {B : 𝓥 *} (f : A → B) (y : B) → (𝓤 ⊔ 𝓥) *
  fib f y = Σ[ x ∈ domain f ] (f x ≡ y)

  tot : {A : 𝓤 *} {B C : A → 𝓤 *} (f : (x : A) → B x → C x) → Σ B → Σ C
  tot f (x , b) = x , f x b

  module Rijke-Exe-7-1 (A : 𝓤 *) (B C D : A → 𝓤 *) (f f' : (x : A) → B x → C x) (g : (x : A) → C x → D x) (H : (x : A) → f x ∼ f' x) where

    a : tot f ∼ tot f'
    a (x , b) = Σ≡→≡ (tot f (x , b)) (tot f' (x , b)) (refl x , H x b)

    b : tot (λ x → g x ∘ f x) ∼ (tot g) ∘ (tot f)
    b (x , b) = refl ((x , (g x) (f x b) ))

    c : tot (λ x → 𝑖𝑑 (B x)) ∼ 𝑖𝑑 (Σ B)
    c (x , b) = refl (x , b)



  module Rijke-Exe-7-3 (A B X : 𝓤 *) (f : A → X) (h : A → B) (g : B → X) (H : f ∼ g ∘ h) where

    fib_tri : (x : X) → fib f x → fib g x
    fib_tri x (a , p) = (h a , ((H a) ⁻¹ ∙ p))

    fib_tri' : (x : X) → fib f x → fib g x
    fib_tri' x (a , p) = ℍ (f a) (λ x' p' → fib g x') (h a , ((H a) ⁻¹)) x p

  

module Rijke-Lecture-08 where

  fib : {A : 𝓤 *} {B : 𝓥 *} (f : A → B) (y : B) → (𝓤 ⊔ 𝓥) *
  fib f y = Σ[ x ∈ domain f ] (f x ≡ y)

  module fib-fst (A : 𝓤 *) (B : A → 𝓤 *) where

    fs : Σ B → A
    fs = fst

    f : (x : A) → B x → fib fs x
    f x b = (x , b) , refl x

    g : (x : A) → fib fs x → B x
    -- g x ((x' , b) , p) = tr B p b
    g x ((x' , b) , p) =  𝕁 A (λ (y' y : A) (q : y' ≡ y) → (c : B y') → B y) (λ y c → c) x' x p b
    {-g x triple = Σ-induction
                   {X = Σ B}
                   {Y = λ (pair : Σ B) → fst pair ≡ x}
                   {A = λ triple → B x}
                   (λ (pair : Σ B) (p : fst pair ≡ x) → Σ-induction
                                                          {X = A}
                                                          {Y = B}
                                                          {A = λ (pair : Σ B) → (p : fst pair ≡ x) → B x}
                                                          (λ (x' : A) (b : B x') (p : x' ≡ x) → tr B p b )
                                                          pair
                                                          p)
                   triple-}

    α : (x : A) → (f x) ∘ (g x) ∼ 𝑖𝑑 (fib fs x)
    -- at x ((x' , b) , p), need to show  ((x , tr B p b) , refl x) ≡ ((x' , b) , p) which is true by carefully applying Σ≡→≡
    -- α x ((x' , b) , p) = 𝕁 A (λ (y' y : A) (q : y' ≡ y) → (c : B y') →  ((y , tr B q c) , refl y') ≡ ((y' , c) , q)) (λ y c → refl ((y , c) , (refl (refl y)))) x' x p b 
    α x ((x , b) , refl x) = refl ((x , b), refl x)

    β : (x : A) → (g x) ∘ (f x) ∼ 𝑖𝑑 (B x)
    β x b = refl b


module Rijke-Exercises-08 (A : 𝓤 *) where

  δ : A → A × A
  δ a = a , a

  fib' : (p : A × A) → 𝓤 *
  fib' (x , y) = Σ (λ (a : A) → (a ≡ x) × (a ≡ y))
  
  -- fib' is equivalent to fib δ via Σ≡→≡

  f : (x y : A) → fib' (x , y) → x ≡ y
  f x y (a , p , q) = p ⁻¹ ∙ q

  g : (x y : A) → x ≡ y → fib' (x , y)
  g x y p = (x , refl x , p)

  α : (x y : A) → (f x y ∘ g x y) ∼ id
  α x y p = refl p

  β : (x y : A) → (g x y ∘ f x y) ∼ id
  β x y (a , p , q) =
    𝕁
      A
      (λ a x p → (y : A) → (q : a ≡ y) → (x , refl x , ( (p ⁻¹) ∙ q)) ≡ (a , p , q)) 
      (λ a y q → refl (a , refl a , q))
      a x p y q
  
