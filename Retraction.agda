{-# OPTIONS --without-K --exact-split --show-implicit #-}



module Retraction where


open import Chapter-03 public

-- Most of the "level" stuff preserved by ≃ is actually preserved by retractions.
-- This file checks that
-- I notice that I have my notation mixed up at some places… r and s swapped…

_≲_ : (A : 𝓤 *) (B : 𝓥 *) → (𝓤 ⊔ 𝓥) *
A ≲ B = Σ (λ (s : A → B) → Σ (λ (r : B → A) → r ∘ s ∼ 𝑖𝑑 A))

≲-refl : (A : 𝓤 *) → A ≲ A
≲-refl A = id , id , refl

≲-tran : {A : 𝓤 *} {B : 𝓥 *} {C : 𝓦 *}
        → A ≲ B → B ≲ C
        → A ≲ C
≲-tran (f , g , α) (h , k , β) = h ∘ f , g ∘ k , γ
 where
  γ : (g ∘ k) ∘ (h ∘ f) ∼ id
  γ a = ((g ∘ k) ∘ (h ∘ f)) a ≡⟨ ap g (β (f a)) ⟩
        g (f a)               ≡⟨ α a ⟩
        a                     ∎

{- Is that true?
≲-symm : {A : 𝓤 *} {B : 𝓥 *}
        → A ≲ B → B ≲ A
        → A ≃ B
-}
≡-preserves-≲ : {A : 𝓤 *} {B : 𝓥 *}
               → (r : A ≲ B)
               → (a₀ a₁ : A)
               → (a₀ ≡ a₁) ≲ (fst r a₀ ≡ fst r a₁) 
≡-preserves-≲ {A = A} {B = B} (r , s , β) a₀ a₁ = ( r' , s' , β')
 where


  r' : a₀ ≡ a₁ → r a₀ ≡ r a₁
  r' = ap r {x₀ = a₀} {x₁ = a₁}

  s' : r a₀ ≡ r a₁ → a₀ ≡ a₁
  s' p = (β a₀) ⁻¹ ∙ (ap s p) ∙ (β a₁)

  β' : s' ∘ r' ∼ 𝑖𝑑 (a₀ ≡ a₁)
  β' p = (s' ∘ r') p                          ≡⟨ refl ( (s' ∘ r') p) ⟩
         (β a₀)⁻¹  ∙ (ap s (ap r p)) ∙ (β a₁)  ≡⟨ ap (λ - → (β a₀)⁻¹  ∙ - ∙ (β a₁)) (ap-∘ r s p)⁻¹ ⟩ 
         (β a₀)⁻¹  ∙ (ap (s ∘ r) p) ∙ (β a₁)   ≡⟨ ∼-is-natural' (s ∘ r) (𝑖𝑑 A) β {x₀ = a₀} {x₁ = a₁} p  ⟩ 
         ap (𝑖𝑑 A) p                           ≡⟨ ap-id p ⟩
         p ∎

≲-of-Contr : {A : 𝓤 *} {B : 𝓥 *}
            → (r : A ≲ B)
            → B isContr
            → A isContr
≲-of-Contr {A = A} (s , r , α)  (b₀ , bb) = (a₀ , aa )           
 where
  a₀ : A
  a₀ = r b₀
  aa : (a₁ : A) → a₀ ≡ a₁
  aa a₁ = a₀           ≡⟨ refl a₀ ⟩
          r b₀         ≡⟨ ap r (bb (s a₁)) ⟩
          r (s a₁)     ≡⟨ α a₁ ⟩
          a₁           ∎

≲-of-Prop : {A : 𝓤 *} → {B : 𝓥 *}
           → A ≲ B
           → B isProp
           → A isProp
≲-of-Prop (f , h , β) bb a₀ a₁ = a₀        ≡⟨ (β a₀)⁻¹ ⟩
                              h (f a₀)  ≡⟨ ap h (bb (f a₀) (f a₁)) ⟩
                              h (f a₁)  ≡⟨ (β a₁) ⟩
                              a₁        ∎ 


≲-of-Level : {A : 𝓤 *} {B : 𝓤 *} {n : ℕ}
            → A ≲ B
            → B isLevel n
            → A isLevel n
≲-of-Level {n = zero} E L = ≲-of-Contr E L
≲-of-Level {n = succ n} (f , g , β) L a₀ a₁ =  ≲-of-Level E' L'
 where
  b₀ = f a₀
  b₁ = f a₁

  A' = a₀ ≡ a₁
  B' = b₀ ≡ b₁

  f' = ap f {x₀ = a₀} {x₁ = a₁}
  
  E' : A' ≲ B'
  E' = ≡-preserves-≲ (f , g , β) a₀ a₁

  L' : B' isLevel n
  L' = L b₀ b₁
