{-# OPTIONS --without-K --exact-split --safe #-}


-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`


module Basics where

open import Spartan-MLTT public

-- Define both projections in term of Σ-induction
-- Assuming Σ-induction is our primitive, we have that fst (x , y) = x and snd (x , y) = y

fst : {X : 𝓤 *} {Y : X → 𝓥 *} → Σ Y → X
fst {X = X} {Y = Y} p = Σ-induction {X = X}
                                    {Y = Y}
                                    {A = λ p → X}
                                    (λ x y → x)
                                    p

snd : {X : 𝓤 *} {Y : X → 𝓥 *} → (p : Σ Y) → Y (fst p)
snd {X = X} {Y = Y} p = Σ-induction {X = X}
                                    {Y = Y}
                                    {A = λ p → Y (fst p)}
                                    (λ (x : X) (y : Y x) → y)
                                    p

-- Defined in Chapter-01.agda already
-- Σ-uniq :  {X : 𝓤 *} {Y : X → 𝓥 *} → (p : Σ Y) → p ≡ (fst p , snd p)
-- Σ-uniq p = Σ-induction {A = λ p → p ≡ (fst p , snd p)} (λ x y → refl (x , y)) p

-- ≡ is an equivalence relation

≡-refl : {X : 𝓤 *} → (x : X) → x ≡ x
≡-refl x = refl x

≡-symm : {X : 𝓤 *} → (x y : X) → x ≡ y → y ≡ x
≡-symm {X = X} x y p = 𝕁 X (λ x y p → y ≡ x) (λ x → refl x) x y p

_⁻¹ : {X : 𝓤 *} {x y : X} → x ≡ y → y ≡ x
_⁻¹ {X = X} {x} {y} p = ≡-symm {X = X} x y p
infix  40 _⁻¹

≡-tran : {X : 𝓤 *} → (x y z : X) → x ≡ y → y ≡ z → x ≡ z
≡-tran {X = X} x y z p q = 𝕁 X (λ x y p → y ≡ z → x ≡ z) (λ x → (λ p → p)) x y p q

_∙_ : {X : 𝓤 *} → {x y z : X} → x ≡ y → y ≡ z → x ≡ z
_∙_ {X = X} {x} {y} {z} p q = ≡-tran {X = X} x y z p q
infixl 30 _∙_


_≡⟨_⟩_ : {X : 𝓤 *} (x : X) {y z : X} → x ≡ y → y ≡ z → x ≡ z
x ≡⟨ p ⟩ q = p ∙ q

_∎ : {X : 𝓤 *} (x : X) → x ≡ x
x ∎ = refl x

infixr  0 _≡⟨_⟩_
infix   1 _∎

