{-# OPTIONS --without-K --exact-split --show-implicit #-}



module Chapter-03 where


open import Chapter-02 public


≃→qinv : {A : 𝓤 *} {B : 𝓥 *}
       → A ≃ B
       → Σ (λ (f : A → B) → hasqinv f)
≃→qinv (f , e) = f , isequiv→hasqinv f e

-- define Contractible and Propositions here even though
-- they don't follow this order in the HoTT book

_isContr : (A : 𝓤 *) → 𝓤 *
A isContr = Σ (λ (a : A) → (b : A) → a ≡ b ) 

_isProp : (A : 𝓤 *) → 𝓤 *
A isProp = (x₀ x₁ : A) → x₀ ≡ x₁

-- p. 140
-- Definition 3.1.1

_isSet : (A : 𝓤 *) → 𝓤 *
A isSet = (x₀ x₁ : A) → (x₀ ≡ x₁) isProp

𝟙-isContr : 𝟙 isContr
𝟙-isContr =  ⋆ , λ x → (𝟙-uniq x) ⁻¹ 

𝟙-isProp : 𝟙 isProp
𝟙-isProp ⋆ ⋆ = refl ⋆ 

𝟙-isSet : 𝟙 isSet
𝟙-isSet x₀ x₁ p q = (β p) ⁻¹ ∙ (ap H (𝟙-isProp (F p) (F q))) ∙  (β q) 
 where
  lol : (x₀ ≡ x₁) ≃ 𝟙
  lol = ≡-≃𝟙 x₀ x₁
  -- F , ((G , α) , (H , β)) : (x₀ ≡ x₁) ≃ 𝟙
  -- F , ((G , α) , (H , β)) = ≡-≃𝟙 x₀ x₁
  F = fst lol
  H = fst (snd (snd lol))
  β = snd (snd (snd lol))


-- Given x₀ x₁ in 𝟙, we have x₀ ≡ x₁ is equivalent to 𝟙
-- Let F : x₀ ≡ x₁ → 𝟙
--     G : 𝟙 → x₀ ≡ x₁
-- be quasi-inverses, with α, β the homotopies
-- From p, q : x₀ ≡ x₁, F p ≡ F q since 𝟙 has a unique element
-- So by ap G, G F p ≡ G F q, and then use β 
-- It's very complicated for such a simple thing…

𝟘-isProp : 𝟘 isProp
𝟘-isProp x₀ x₁ = !𝟘 (x₀ ≡ x₁) x₀

𝟘-isSet : 𝟘 isSet
𝟘-isSet x₀ x₁ p q = !𝟘 (p ≡ q) x₀

≃-preserves-Contr : {A : 𝓤 *} → {B : 𝓥 *}
                   → A ≃ B
                   → B isContr
                   → A isContr
≃-preserves-Contr {A = A} {B = B} E (b₀ , bb) = (a₀ , aa)
 where
  f = fst E
  h = fst (snd (snd E))
  β = snd (snd (snd E))
 
  a₀ : A
  a₀ = h b₀
  aa : (a₁ : A) → a₀ ≡ a₁
  aa a₁ = a₀           ≡⟨ refl a₀ ⟩
          h b₀         ≡⟨ ap h (bb (f a₁)) ⟩
          h (f a₁)     ≡⟨ β a₁ ⟩
          a₁           ∎
          

≃-preserves-Prop : {A : 𝓤 *} → {B : 𝓥 *}
                   → A ≃ B
                   → B isProp
                   → A isProp
≃-preserves-Prop E p a₀ a₁ = a₀        ≡⟨ (β a₀)⁻¹ ⟩
                              h (f a₀)  ≡⟨ ap h (p (f a₀) (f a₁)) ⟩
                              h (f a₁)  ≡⟨ (β a₁) ⟩
                              a₁        ∎ 
 where
  f = fst E
  h = fst (snd (snd E))
  β = snd (snd (snd E))

≃-preserves-Set : {A : 𝓤 *} → {B : 𝓥 *}
                   → A ≃ B
                   → B isSet
                   → A isSet
≃-preserves-Set {A = A} {B = B} E s a₀ a₁ = ≃-preserves-Prop E' (s b₀ b₁)
 where
  f : A → B
  f = fst E
  e : isequiv f
  e = snd E

  b₀ = f a₀
  b₁ = f a₁

  f' = ap f {x₀ = a₀} {x₁ = a₁}
  
  E' : (a₀ ≡ a₁) ≃ (b₀ ≡ b₁)
  E' = f' , ≡-preserves-equiv f e a₀ a₁


≃-succ : (m n : ℕ) → (succ m ≡ succ n) ≃ (m ≡ n)
≃-succ m n = ≃-tran (ℕ-equiv-≡ (succ m) (succ n))  (≃-symm (ℕ-equiv-≡ m n))

-- p. 140
-- Example 3.1.4

ℕ-isSet : ℕ isSet
ℕ-isSet zero zero          = ≃-preserves-Prop (ℕ-equiv-≡ zero zero) (𝟙-isProp) 
ℕ-isSet zero (succ n)      = ≃-preserves-Prop (ℕ-equiv-≡ zero (succ n)) (𝟘-isProp) 
ℕ-isSet (succ m) zero      = ≃-preserves-Prop (ℕ-equiv-≡ (succ m) zero) (𝟘-isProp)
ℕ-isSet (succ m) (succ n)  = ≃-preserves-Prop (≃-succ m n)  (ℕ-isSet m n)
-- The fact that (succ m) ≡ (succ n) is a prop comes from the fact that this type is equivalent to n ≡ m which we know by hypothesis to be a prop

-- p. 140
-- Example 3.1.5

Σ-of-Sets : (A : 𝓤 *) → A isSet
          → (B : A → 𝓥 *) → ((x : A) → B x isSet)
          → Σ B isSet
Σ-of-Sets A Aset B Bsets (x₀ , y₀) (x₁ , y₁) p₀ p₁ = (Σ≡-β z₀ z₁ p₀) ⁻¹ ∙
                                                     (ap (Σ≡→≡ z₀ z₁) Q' ) ∙
                                                     (Σ≡-β z₀ z₁ p₁)
 where
  z₀ = (x₀ , y₀)
  z₁ = (x₁ , y₁)
  q₀ q₁ : z₀ Σ≡ z₁
  q₀ = ≡→Σ≡ z₀ z₁ p₀
  q₁ = ≡→Σ≡ z₀ z₁ p₁

  e₀ = fst q₀
  e₁ = fst q₁
  s₀ = snd q₀
  s₁ = snd q₁

  E : e₀ ≡ e₁
  E = Aset x₀ x₁ e₀ e₁
  S : tr (λ e → (tr B e y₀ ≡ y₁)) E s₀ ≡ s₁
  S = Bsets x₁ (tr B e₁ y₀) y₁ (tr (λ e → (tr B e y₀ ≡ y₁)) E s₀) s₁
  Q : q₀ Σ≡ q₁
  Q = (E , S)

  Q' : q₀ ≡ q₁
  Q' = Σ≡→≡ q₀ q₁ Q

  

-- p. 141
-- Example 3.1.6

Π-of-Sets :  {A : 𝓤 *}
           → (B : (x : A) → 𝓥 *)
           → ((x : A) → B x isSet)
           → (Π B) isSet
Π-of-Sets {A = A} B BS f₀ f₁ p₀ p₁ = p≡
 where
  α₀ α₁ : f₀ ∼ f₁
  α₀ = happly p₀
  α₁ = happly p₁

  α∼ : α₀ ∼ α₁
  α∼ x =  BS x (f₀ x) (f₁ x) (α₀ x) (α₁ x) 
  α≡ : α₀ ≡ α₁
  α≡ = funext α∼

  p≡ : p₀ ≡ p₁
  p≡ = (FE-β p₀) ⁻¹ ∙
       (ap (funext) α≡)  ∙
       (FE-β p₁)



-- p. 142
-- Example 3.1.9
-- Not all types are sets

𝓤₀-isNotSet : {UV : (𝓢 : Universe) → Univalence 𝓢} →  ¬ {𝓤 = 𝓤₀ ⁺} ((𝓤₀ *) isSet)
𝓤₀-isNotSet {UV = UV} S = ₀≢₁ ₀≡₁ 
 where

  p : 𝟚 → 𝟚
  p ₀ = ₁
  p ₁ = ₀

  -- We assume `𝓤₀` is a set, so that there is only one equality path from `𝟚`
  -- to `𝟚`.
  -- We will show that this implies that `p : 𝟚 → 𝟚` above must be the identity.
  -- But if `p ≡ 𝑖𝑑 𝟚`, then its evaluation at `₀` is equal to that of `₁`, by `ap`.
  -- From there, we are done, since we know already that `¬ (₀ ≡ ₁)`.

  -- First, we see that `p` defines an equivalence of `𝟚` with itself, easily.

  α : p ∘ p ∼ 𝑖𝑑 𝟚
  α ₀ = refl ₀
  α ₁ = refl ₁
  p≃ : 𝟚 ≃ 𝟚
  p≃ = p , ((p , α) , (p , α))

  -- Using univalence, from `p≃ : 𝟚 ≃ 𝟚` we get `p≡ : 𝟚 ≡ 𝟚`. 

  p≡ : 𝟚 ≡ 𝟚
  p≡ = ua p≃

  -- And we now use the assumption that `𝓤₀` is a set:
  -- It implies that `p≡` is `refl 𝟚`

  p≡-is-refl :  p≡ ≡ (refl 𝟚)
  p≡-is-refl = S 𝟚 𝟚 p≡ (refl 𝟚)

  
  -- Now, from `p≡ ≡ refl 𝟚`, we apply `≡→map`
  -- and get that that `≡→map p≡` is `≡→map (refl 𝟚)`,
  -- which is judgementally  `𝑖𝑑 𝟚`.
  recover-id : ≡→map p≡ ≡ 𝑖𝑑 𝟚 -- = ≡→map (refl 𝟚) -- 
  recover-id = ap ≡→map p≡-is-refl

  -- On the other hand, Univalence gives us an equivalence:
  -- Applying univalence (i.e. `ua`) to `p≃`, and then `id→eqv` results propositionally
  -- in `p≃` again, and the first component of `p≃` is just (judgementally) `p`.
  -- In conclusion, `p ≡ ≡→map p≡`.
  -- In more details, to convince myself:
  -- Let `α = WithUnivalence.UV-α UV` and `ua = WithUnivalence.ua UV`
  -- Then by definition of `α`
  --   α p≃ : id→eqv (ua p≃) ≡ p≃
  -- But `id→eqv q = (≡→map q , …)` judgementally (just by definition)
  -- and `p≃ = (p , …)` by definition again
  -- Thus
  --   ap fst (α p≃) : fst (≡→map p≡ , …) ≡ fst (p , …)
  -- hence
  --   ap fst (α p≃ ) : ≡→map p≡ ≡ p
  -- and we're happy.
  
  recover-p : p ≡ (≡→map p≡)
  recover-p = (ap fst (UV-α p≃))⁻¹

  -- Concatenating `recover-id` and `recover-p` yields an equality
  -- between `p` and `𝑖𝑑 𝟚`.

  p≡id : p ≡ 𝑖𝑑 𝟚
  p≡id = recover-p ∙ recover-id

  -- And by applying "evaluation at `₀`", we get `₀ ≡ ₁`.

  ₀≡₁ : ₀ ≡ ₁
  ₀≡₁ = ap (λ - → - ₀) p≡id ⁻¹


-- From here on (and for a while) I don't follow the HoTT book:


-- We define the levels

_isLevel_ : (A : 𝓤 *) (n : ℕ) → 𝓤 *
A isLevel zero     = A isContr
A isLevel (succ n) = (x₀ x₁ : A) → ((x₀ ≡ x₁) isLevel n)

-- Thus, by _definition_ level 0 is contractibility
-- Let's show that level 1 is the same as being a prop



-- We need `Prop→Set` for this (p. 146 Lemma 3.3.4): 
Prop→Set : (A : 𝓤 *) → A isProp → A isSet
Prop→Set A P x₀ x₁ p₀ p₁ = (lol2 x₁ x₀ x₁ p₀) ∙ (lol2 x₁ x₀ x₁ p₁)⁻¹ 
 where
  -- How they phrase it in the Hott book
  -- lol : (x y z : A) (p : y ≡ z)  → (P x y) ∙ p ≡ (P x z)
  -- lol x y y (refl y) = ∙-refl-right (P x y)
  -- Easier to use
  lol2 : (x y z : A) (p : y ≡ z)  →  p ≡ (P x y)⁻¹ ∙ (P x z)
  lol2 x y y (refl y) = (∙-⁻¹-left (P x y))⁻¹
  
  
  

  -- Goal: Given
  --   x₀ x₁ : A
  --   p₀ p₁ : x₀ ≡ x₁
  -- Show
  --   p₀ ≡ p₁
  -- By 𝕁, one may assume x₀ = x = x₁ and p₀ = refl x.
  -- In which case, we have to show p₁ ≡ refl x.
  -- 
  

Prop→Level1 : (A : 𝓤 *) → A isProp → (A isLevel one)
Prop→Level1 A G x₀ x₁ =  G x₀ x₁ , (Prop→Set A G) x₀ x₁ (G x₀ x₁)
-- If `A` is a Prop by `G`:
-- `G x₀ x₁` gives an element of `x₀ ≡ x₁`, and since `A` is also a set
-- all elements of `x₀ ≡ x₁` are equal, and in particular equal to `G x₀ x₁`
-- Thus, `x₀ ≡ x₁` is contractible, meaning that `A` is of level 1

Level1→Prop : (A : 𝓤 *) → (A isLevel one) → A isProp
Level1→Prop A F x₀ x₁ = fst (F x₀ x₁)
-- If `A` is of level one, then all `x₀ ≡ x₁` are contractible, hence inhabited.
-- Thus, `A` is a prop

-- Since Level1 is the same as Prop, it follows that Level2 is the same as Set
-- Not proved here though, I'm too lazy.

Level2→Set : (A : 𝓤 *) → (A isLevel two) → A isSet
Level2→Set A A-2 x₀ x₁ = Level1→Prop (x₀ ≡ x₁) (A-2 x₀ x₁)

Set→Level2 : (A : 𝓤 *) → A isSet → (A isLevel two)
Set→Level2 A A-isSet x₀ x₁ = Prop→Level1 (x₀ ≡ x₁) (A-isSet x₀ x₁)


Contr→Prop : (A : 𝓤 *) → A isContr → A isProp
Contr→Prop A F x₀ x₁ = ((snd F) x₀)⁻¹ ∙ ((snd F) x₁)

inhabitedProp→Contr : (A : 𝓤 *) → A → A isProp → A isContr
inhabitedProp→Contr A a f = a , f a

≃𝟙→Contr : (A : 𝓤 *) → (A ≃ 𝟙) → A isContr
≃𝟙→Contr A E = ≃-preserves-Contr E 𝟙-isContr

Contr→≃𝟙 : (A : 𝓤 *) → A isContr → (A ≃ 𝟙)
Contr→≃𝟙 A C = f , (g , α) , (g , β)
 where
  a₀ = fst C
  aa = snd C

  f : A → 𝟙
  f _ = ⋆

  g : 𝟙 → A
  g ⋆ = a₀

  α : f ∘ g ∼ 𝑖𝑑 𝟙
  β : g ∘ f ∼ 𝑖𝑑 A
  α = λ x → (𝟙-uniq x)⁻¹ 
  β a = aa a

  -- A bit more details on `α` and `β`:
  -- On `x : 𝟙`, we have `g x = a₀` and `f g x = ⋆` (judgementally)
  -- so it suffices to use `𝟙-uniq`.
  -- On `a : A`, we have `g f a = a₀` (judgementally)
  -- so we just use `aa`.
  
LevelMonotonicity : (A : 𝓤 *) (n : ℕ) → A isLevel n → A isLevel (succ n)
LevelMonotonicity A zero P =  Prop→Level1 A (Contr→Prop A P)
LevelMonotonicity A (succ n) P x₀ x₁ = LevelMonotonicity (x₀ ≡ x₁) n (P x₀ x₁)


≃-preserves-Level : (A : 𝓤 *) (B : 𝓤 *) (n : ℕ) → A ≃ B → B isLevel n → A isLevel n
≃-preserves-Level A B zero E L = ≃-preserves-Contr E L
≃-preserves-Level A B (succ n) E L a₀ a₁ =  ≃-preserves-Level A' B' n E' L'
 where
  f : A → B
  f = fst E
  e : isequiv f
  e = snd E

  b₀ = f a₀
  b₁ = f a₁


  A' = a₀ ≡ a₁
  B' = b₀ ≡ b₁

  f' = ap f {x₀ = a₀} {x₁ = a₁}
  
  E' : A' ≃ B'
  E' = f' , ≡-preserves-equiv f e a₀ a₁

  L' : B' isLevel n
  L' = L b₀ b₁

≃-Level : {A : 𝓤 *} {B : 𝓤 *} {n : ℕ} → A ≃ B → B isLevel n → A isLevel n
≃-Level {A = A} {B = B} {n = n} = ≃-preserves-Level A B n

-- Let's get back to following the book


-- p. 143
-- Theorem 3.2.2

-- Call `¬¬ A → A` "double negation elimination", i.e. DNE
DNE∞ : {𝓢 : Universe} → 𝓢 ⁺ *
DNE∞ {𝓢 = 𝓢} = (A : 𝓢 *) → ¬¬ A → A

LEM∞ : {𝓢 : Universe} → 𝓢 ⁺ *
LEM∞ {𝓢 = 𝓢} = (A : 𝓢 *) → A + ¬ A

LEM→DNE : {𝓢 : Universe} → LEM∞ {𝓢 = 𝓢} → DNE∞ {𝓢 = 𝓢}
LEM→DNE {𝓢 = 𝓢} lem = dne
 where
  dne : (A : 𝓢 *) → ¬¬ A → A
  dne A ϕ = +-recursion (𝑖𝑑 A) (λ (f : ¬ A) → !𝟘 A (ϕ f)) (lem A)

lem→dne : (A : 𝓤 *) → (A + ¬ A) → ¬¬ A → A
lem→dne A lem-A ϕ = +-recursion (𝑖𝑑 A) (λ (f : ¬ A) → !𝟘 A (ϕ f)) lem-A 

DNE→LEM∞ : {𝓢 : Universe} → DNE∞ {𝓢 = 𝓢} → LEM∞ {𝓢 = 𝓢}
DNE→LEM∞ {𝓢 = 𝓢} dne = lem
 where
  lem : (A : 𝓢 *) → A + ¬ A
  lem A = dne (A + ¬ A) ¬²-LEM-A
   where
    ¬²-LEM-A : ¬¬ (A + ¬ A)
    ¬²-LEM-A = λ f → (f ∘ inr) (f ∘ inl)

-- Note that DNE∞ A follows from LEM∞ A
-- But       LEM∞ A follows from DNE∞ (A + ¬ A)

-- p. 142
-- Section 3.2

-- Instead of proving no-LEM from no-DNE, we go the other way around and use Hedberg
-- Hedberg tells us that if a type A is not a set, then its universe cannot satisfy LEM
-- Since if the universe satisfies LEM, the equality types of A are decidable, hence A is a set


_hasHedberg : (𝓢 : Universe) →  𝓢 ⁺ *
𝓢 hasHedberg 
        = (A : 𝓢 *)
        → ((x₀ x₁ : A) → (x₀ ≡ x₁) + ¬ (x₀ ≡ x₁))
        → A isSet
        

no-LEM∞ : {UV : (𝓢 : Universe) → Univalence 𝓢}
       → {H : (𝓢 : Universe) →  𝓢 hasHedberg }
       → ¬ (LEM∞ {𝓢 = 𝓤₁})
no-LEM∞ {UV = UV}  {H = H} lem = 𝓤₀-isNotSet {UV = UV} 𝓤₀-isSet
 where
  𝓤₀-isSet :  (𝓤₀ *) isSet
  𝓤₀-isSet = (H (𝓤₁)) (𝓤₀ *)  (λ (T₀ T₁ : 𝓤₀ *) → (lem (T₀ ≡ T₁)))


-- Generalized: to show that LEM∞ doesn't hold for a universe 𝓤, all we need
-- is a non-set in 𝓤.

no-LEM∞'' : {H : (𝓢 : Universe) →  _hasHedberg 𝓢 }
         → (A : 𝓤 *) → ¬(A isSet)
       → ¬ (LEM∞ {𝓢 = 𝓤})
no-LEM∞'' {𝓤 = 𝓤} {H = H} A A-isNotSet lem = A-isNotSet A-isSet
 where
  A-isSet : A isSet
  A-isSet = (H (𝓤)) A  (λ (a₀ a₁ : A) → (lem (a₀ ≡ a₁)))
  -- If `𝓤` satisfies LEM∞
  -- and `A : 𝓤` is not a set
  -- Since `𝓤` satisfies LEM∞, the types `a₀ ≡ a₁` (for `a₀ a₁ : A`)
  -- have decidable equality, which implies that `A` is a set

-- Actually:
-- We could define a function that takes "𝓤 satisfying LEM∞" as an argument
-- and returns sethood for all types in 𝓤
-- The crucial thing here is that for `a₀ a₁ : A` and `A : 𝓤`, we have `a₀ ≡ a₁ : 𝓤`

LEM∞-means-Sets : {H : (𝓢 : Universe) → _hasHedberg 𝓢 }
               → (LEM∞ {𝓢 = 𝓤})
               → (A : 𝓤 *) → (A isSet)
LEM∞-means-Sets {𝓤 = 𝓤} {H = H} lem A = A-isSet
 where
  A-isSet : A isSet
  A-isSet = (H (𝓤)) A  (λ (a₀ a₁ : A) → (lem (a₀ ≡ a₁)))


-- and then define `no-LEM''` in terms of `LEM-means-Sets`

-- p. 145
-- Section 3.3

-- p. 145
-- Definition 3.3.1
-- See definition of `_isProp` above

-- p. 145
-- Lemma 3.3.2
-- See `Contr→Prop`, `inhabitedProp→Contr`, `Contr→≃𝟙` and `≃𝟙→Contr`

-- p. 146
-- Lemma 3.3.3

bi-impliedProps→≃ : (A : 𝓤 *) → A isProp
                  →  (B : 𝓥 *) → B isProp
                  → (f : A → B)
                  → (g : B → A)
                  → A ≃ B
bi-impliedProps→≃ A PA B PB f g = (f , (g , α) , (g , β))
 where
  α : f ∘ g ∼ 𝑖𝑑 B
  β : g ∘ f ∼ 𝑖𝑑 A
  α b = PB (f (g b)) b
  β a = PA (g (f a)) a

impliedContr→≃ : (A : 𝓤 *) → A isContr
               → (B : 𝓥 *) → B isContr
               → (f : A → B)
               → isequiv f
impliedContr→≃ A (a₀ , aa) B (b₀ , bb) f = ((g , α) , (h , β))
 where
  g h : B → A
  α : f ∘ g ∼ 𝑖𝑑 B
  β : h ∘ f ∼ 𝑖𝑑 A

  g b = a₀
  h b = a₀
  α b = bb (f a₀) ⁻¹ ∙ bb (b)
  β a = aa a 



-- p. 146
-- Lemma 3.3.4
-- See Prop→Set



isContr-isProp : (A : 𝓤 *) → (A isContr) isProp
isContr-isProp  A (a₀ , α₀) (a₁ , α₁) = Σ≡→≡  (a₀ , α₀) (a₁ , α₁) (p , q)
 where
  p : a₀ ≡ a₁
  p = α₀ a₁
  -- need to show tr (λ b → (a : A) a ≡ b) p α₀ ≡ α₁

  α₀' : (a : A) → a₁ ≡ a
  α₀' =  tr (λ b → (a : A) → b ≡ a) p α₀

  q∼ : α₀' ∼ α₁
  q∼ a = Prop→Set A (Contr→Prop A (a₀ , α₀)) a₁ a (α₀' a) (α₁ a)
  -- To show that α₀' is homotopic to α₁ means to show that the
  -- paths are equal.
  -- But all paths are equal since a Contractible is a Prop is a Set!!!

  q : α₀' ≡ α₁
  q = funext q∼



Π-of-Props : {A : 𝓤 *}
           → (B : (x : A) → 𝓥 *)
           → ((x : A) → B x isProp)
           → (Π B) isProp
Π-of-Props  {A = A} B BP f₀ f₁ = funext F
 where
  F : f₀ ∼ f₁
  F x = BP x (f₀ x) (f₁ x)



Π²-of-Props :  {A : 𝓤 *}
           → {B : (x y : A) → 𝓥 *}
           → ((x y : A) → (B x y) isProp)
           → ((x y : A) → B x y) isProp
Π²-of-Props  {A = A} {B = B} BP f₀ f₁ = funext F
 where
  F : f₀ ∼ f₁
  F x = funext Fx
   where
    Fx : f₀ x ∼ f₁ x
    Fx y = BP x y (f₀ x y) (f₁ x y)

isLevel-isProp : (A : 𝓤 *)
                → (n : ℕ)
                → (A isLevel n) isProp
isLevel-isProp  A zero = isContr-isProp  A
isLevel-isProp  A (succ n) = Π²-of-Props 
                                                  {A = A}
                                                  (λ (x₀ x₁ : A) → isLevel-isProp  (x₀ ≡ x₁) n)
  
  
 
-- p. 146
-- Lemma 3.3.5

isProp-isProp : (A : 𝓤 *)
               → (A isProp) isProp
isProp-isProp  A α β = Π²-of-Props  s α β
 where
  -- Need to show α ≡ β
  -- Suffices to show α a₀ ≡ β a₀ for all a₀ (by funext)
  --   Suffices to show α a₀ a₁ ≡ β a₀ a₁ for all a₁ (by funext)
  -- This holds since A being a prop, by α, it is also a set.

  -- How we do it
  s : A isSet
  s = Prop→Set A α
  -- We know then that for all x₀ x₁ the type x₀ ≡ x₁ is a Prop
  -- To show that `α ≡ β` it then suffices to use the fact that
  -- `(x₀ x₁ : A) → x₀ ≡ x₁` is a `Π²` type over props.

isSet-isProp : (A : 𝓤 *)
              → (A isSet) isProp
isSet-isProp  A s t = Π²-of-Props  σ s t
 where
  -- Want to use that Π² of props are props.
  --   A isSet =  (x₀ x₁ : A) (p₀ p₁ : x₀ ≡ x₁) → p₀ ≡ p₁
  -- So we need to show that, given any `x₀ x₁`, we have
  --   ((p₀ p₁ : x₀ ≡ x₁) → p₀ ≡ p₁) isProp
  σ : (x₀ x₁ : A) → ((p₀ p₁ : x₀ ≡ x₁) → (p₀ ≡ p₁)) isProp
  σ x₀ x₁ = Π²-of-Props   τ
   -- We again use that Π² of props are props.
   -- `x₀ x₁` are now fixed, and it suffices to show that
   -- for all `p₀ p₁`, `p₀ ≡ p₁` is a prop
   -- But since A is a Set, it is a 1 type (book naming)
   -- and hence `p₀ ≡ p₁` is a prop
   where
    τ : (p₀ p₁ : x₀ ≡ x₁) → (p₀ ≡ p₁) isProp
    τ = Prop→Set (x₀ ≡ x₁) (s x₀ x₁)

DNE : {𝓢 : Universe} → 𝓢 ⁺ *
DNE {𝓢 = 𝓢} = (A : 𝓢 *) → A isProp → ¬¬ A → A

LEM : {𝓢 : Universe} → 𝓢 ⁺ *
LEM {𝓢 = 𝓢} = (A : 𝓢 *) → A isProp → A + ¬ A


-- p. 149
-- Section 3.5 Subsets and propositional resizing

-- p. 149
-- Lemma 3.5.1

Σ-of-Props : {A : 𝓤 *}
           → {P : A → 𝓥 *}
           → ((x : A) → (P x) isProp)
           → (u₀ u₁ : Σ P)
           → fst u₀ ≡ fst u₁
           → u₀ ≡ u₁
-- Σ-of-Props {A = A} {P = P} props (x , y₀) (x , y₁) (refl x) = Σ≡→≡ (x , y₀) (x , y₁) (refl x , props x y₀ y₁)
Σ-of-Props {A = A} {P = P} props (x₀ , y₀) (x₁ , y₁) p = 𝕁 A type base x₀ x₁ p y₀ y₁
 where
  type = λ x₀ x₁ p → (y₀ : P x₀) → (y₁ : P x₁) → (x₀ , y₀) ≡ (x₁ , y₁)
  base : (x : A) → (y₀ : P x) → (y₁ : P x) → (x , y₀) ≡ (x , y₁)
  base x y₀ y₁ = Σ≡→≡ (x , y₀) (x , y₁) (refl x , props x y₀ y₁)

  -- By Σ-induction, we may assume both points `u₀ u₁` are pairs `(xᵢ , yᵢ)`.
  -- By 𝕁, we may assume `x₀ = x = x₁` and `p` is reflectivity.
  -- Then we can use the fact that `P x` is a proposition

Sets : {𝓢 : Universe} → 𝓢 ⁺ *
Sets {𝓢 = 𝓢} = Σ (λ (A : 𝓢 *) → A isSet)
Props : {𝓢 : Universe} → 𝓢 ⁺ *
Props {𝓢 = 𝓢} = Σ _isProp -- shortened notation it's clean 
  

-- p. 151
-- Section 3.6 The logic of mere propositions

-- p. 151
-- Example 3.6.1

Σ-of-props : (A : 𝓤 *)
           → A isProp
           → (P : A → 𝓥 *)
           → ((x : A) → P x isProp)
           → Σ P isProp
Σ-of-props A p P ps (x₀ , y₀) (x₁ , y₁) = Σ≡→≡ (x₀ , y₀) (x₁ , y₁) (e , s) 
 where
  e : x₀ ≡ x₁
  e = (p x₀ x₁)
  s : tr P e y₀ ≡ y₁
  s = ps x₁ (tr P e y₀) y₁

×-of-Props : {A : 𝓤 *} → A isProp
           → {B : 𝓥 *} → B isProp
           → (A × B) isProp
×-of-Props {A = A} A-isProp {B = B} B-isProp = Σ-of-props A A-isProp (λ _ → B) (λ _ → B-isProp)

-- p. 152
-- Example 3.6.2

-- c.f. Π-of-Props


Σ-of-contrs : (A : 𝓤 *)
            → A isContr
            → (P : A → 𝓥 *)
            → ((x : A) → P x isContr)
            → Σ P isContr
Σ-of-contrs A (c , γ) P CΓ = center , centrality
 where
  d = fst (CΓ c)
  center : Σ P
  center = c , d
  centrality : (p : Σ P) → center ≡ p
  centrality (x , y) = Σ≡→≡ center (x , y) (e , s) 
   where
    e : c ≡ x
    e = γ x
    s : tr P e d ≡ y
    lol : fst (CΓ x) ≡ tr P e d 
    lol  = (snd (CΓ x) (tr P e d))
    lol2 : fst (CΓ x) ≡ y
    lol2 = snd (CΓ x) y
    s = lol ⁻¹ ∙ lol2

Π-of-contrs : (A : 𝓤 *)
            → (P : A → 𝓥 *)
            → ((x : A) → P x isContr)
            → Π P isContr
Π-of-contrs  A P C = center , centrality
 where
  center : (x : A) → P x
  center = λ x → fst (C x)
  centrality∼ : (f : Π P) → center ∼ f 
  centrality∼ f x = snd (C x) (f x)
  centrality : (f : Π P) → center ≡ f
  centrality f = funext (centrality∼ f) 

¬-isProp : (A : 𝓤 *) → (¬ A) isProp
¬-isProp  A = Π-of-Props  (λ _ → 𝟘) (λ _ → 𝟘-isProp)
¬¬-isProp : (A : 𝓤 *) → (¬¬ A) isProp
¬¬-isProp  A = ¬-isProp  (¬ A) 

-- p. 152
-- Section 3.7 Propositional truncation

-- Stolen from Escardo
record propositional-truncation : 𝓤ω where
 field
  ∥_∥                  : {𝓤 : Universe} → 𝓤 * → 𝓤 *
  ∥∥-isProp            : {𝓤 : Universe} {X : 𝓤 *} → ∥ X ∥ isProp
  ∣_∣                   : {𝓤 : Universe} {X : 𝓤 *} → X → ∥ X ∥
  ∥∥-recursion         : {𝓤 𝓥 : Universe} {X : 𝓤 *} {P : 𝓥 *}
                       → P isProp → (X → P) → ∥ X ∥ → P
 infix 0 ∥_∥

postulate pt : propositional-truncation
open propositional-truncation pt public


-- I think from now on ‌we can use truncation as in the book!



Prop≃truncation : {A : 𝓤 *}
                → A isProp
                → A ≃ ∥ A ∥
Prop≃truncation {A = A} p = bi-impliedProps→≃ A
                                               p
                                               (∥_∥ A)
                                               (∥∥-isProp)
                                               (∣_∣)
                                               (∥∥-recursion p id) 


-- p. 154
-- Section 3.8 The axiom of choice

-- The axiom of choice as given in the book
-- Equation 3.8.1

AC : (𝓡 𝓢 𝓣 : Universe) → ( 𝓡 ⊔ 𝓢 ⊔ 𝓣 ) ⁺ *
AC 𝓡 𝓢 𝓣 = (X : 𝓡 *) → (A : X → 𝓢 *) → (P : (x : X) → A x → 𝓣 *)
          → X isSet  → ((x : X) → A x isSet) → ((x : X) → (a : A x) → P x a isProp)
          -----------------------------------------------------------------------------------
          → Π[ x ∈ X ] ∥ (Σ[ a ∈ A x ] P x a) ∥ → ∥ Σ[ g ∈  Π A ] Π[ x ∈ X ] P x (g x) ∥

-- Restatement using the “type-theoretical” ac
-- We use this one in the proof of Lemma 3.8.2 (to be precise, we /would/ use this one if we were to actually do the proof)

AC' : (𝓡 𝓢 𝓣 : Universe) → ( 𝓡 ⊔ 𝓢 ⊔ 𝓣 ) ⁺ *
AC'  𝓡 𝓢 𝓣 = (X : 𝓡 *) → (A : X → 𝓢 *) → (P : (x : X) → A x → 𝓣 *)
            → X isSet → ((x : X) → A x isSet) → ((x : X) → (a : A x) → P x a isProp)
            -----------------------------------------------------------------------------
            → Π[ x ∈ X ] ∥ Σ[ a ∈ A x ] P x a ∥ → ∥ Π[ x ∈ X ] (Σ[ a ∈ A x ] P x a) ∥

-- needed elsewhere
bi-implied→∥∥ : (A : 𝓤 *) → (B : 𝓥 *) → (A → B) → (B → A) → ∥ A ∥ ≃ ∥ B ∥
bi-implied→∥∥ A B f g = bi-impliedProps→≃ (∥ A ∥)
                                                   (∥∥-isProp {X = A})
                                                   (∥ B ∥)
                                                   (∥∥-isProp {X = B})
                                                   (∥∥-recursion (∥∥-isProp {X = B}) (λ a → ∣ f a ∣)) 
                                                   (∥∥-recursion (∥∥-isProp {X = A}) (λ b → ∣ g b ∣)) 

≃→∥∥ : {A : 𝓤 *} → {B : 𝓥 *} → A ≃ B → ∥ A ∥ ≃ ∥ B ∥
≃→∥∥ {A = A} {B = B} (f , (g , α) , (h , β)) = bi-implied→∥∥ A B f g 

-- The “converse” direction of AC holds
AC-bwd : (X : 𝓤 *)
          → (A : X → 𝓥 *)
          → (P : (x : X) → A x → 𝓦 *)
          → X isSet
          → ((x : X) → A x isSet)
          → ((x : X) → (a : A x) → P x a isProp)
          → ∥ Σ[ g ∈  Π A ] Π[ x ∈ X ] P x (g x) ∥
          → Π[ x ∈ X ] ∥ Σ[ a ∈ A x ] P x a ∥
AC-bwd {𝓤} X A P X-isSet Ax-isSet Pxa-isProp = ∥∥-recursion codomain-isProp fun
 where
  codomain-isProp :  (Π[ x ∈ X ] ∥ Σ[ a ∈ A x ] P x a ∥) isProp
  codomain-isProp = Π-of-Props {𝓤} {A = X} (λ x → (∥ Σ[ a ∈ A x ] (P x a) ∥)) (λ x →  ∥∥-isProp {X = (Σ[ a ∈ A x ] (P x a))}) 
  fun : Σ[ g ∈  Π A ] Π[ x ∈ X ] (P x (g x)) → Π[ x ∈ X ] ∥ Σ[ a ∈ A x ] P x a ∥ 
  fun (g , F) = λ x → ∣ (g x , F x) ∣ 

-- The “converse” direction of AC' holds
AC'-bwd : (X : 𝓤 *)
          → (A : X → 𝓥 *)
          → (P : (x : X) → A x → 𝓦 *)
          → X isSet
          → ((x : X) → A x isSet)
          → ((x : X) → (a : A x) → P x a isProp)
          → ∥ Π[ x ∈ X ] Σ[ a ∈ A x ] P x a ∥
          → Π[ x ∈ X ] ∥ Σ[ a ∈ A x ] P x a ∥
AC'-bwd {𝓤} X A P X-isSet Ax-isSet Pxa-isProp = ∥∥-recursion codomain-isProp fun
 where
  codomain-isProp :  (Π[ x ∈ X ] ∥ Σ[ a ∈ A x ] P x a ∥) isProp
  codomain-isProp = Π-of-Props {𝓤} {A = X} (λ x → (∥ Σ[ a ∈ A x ] (P x a) ∥)) (λ x →  ∥∥-isProp {X = (Σ[ a ∈ A x ] (P x a))}) 
  fun : (Π[ x ∈ X ] Σ[ a ∈ A x ] P x a) →  Π[ x ∈ X ] ∥ Σ[ a ∈ A x ] P x a ∥
  fun g = λ x → ∣ g x ∣ 


-- p. 155
-- Equation 3.8.3
-- The simplified version equivalent to AC

alt-AC : (𝓡 𝓢 : Universe) → (𝓡 ⊔ 𝓢 )⁺ *
alt-AC 𝓡 𝓢 = (X : 𝓡 *)
            → (Y : X → 𝓢 *)
            → X isSet
            → ((x : X) → Y x isSet)
            → (Π[ x ∈ X ] ∥ Y x ∥)
            → ∥ Π Y ∥

-- Its converse holds

alt-ac-bwd : (X : 𝓤 *)
            → (Y : X → 𝓥 *)
            → X isSet
            → ((x : X) → Y x isSet)
            → ∥ Π Y ∥
            → (Π[ x ∈ X ] ∥ Y x ∥)
alt-ac-bwd {𝓤} X Y X-isSet Yx-isSet = ∥∥-recursion codomain-isProp fun
 where
  codomain-isProp : (Π[ x ∈ X ] ∥ Y x ∥) isProp
  codomain-isProp = Π-of-Props {𝓤} {A = X} (λ x → ∥ Y x ∥) (λ x → (∥∥-isProp {X = Y x}))
  fun : (Π[ x ∈ X ] Y x) → (Π[ x ∈ X ] ∥ Y x ∥) 
  fun f = λ x → ∣ f x ∣  

-- If this simplified AC form holds, then we actually have an equivalence
alt-AC-≃ : (X : 𝓤 *)
            → (Y : X → 𝓥 *)
            → X isSet
            → ((x : X) → Y x isSet)
            → alt-AC 𝓤 𝓥
            → (Π[ x ∈ X ] ∥ Y x ∥) ≃  ∥ Π Y ∥
alt-AC-≃ X Y X-isSet Yx-isSet alt-ac-fwd =  bi-impliedProps→≃ L L-isProp R R-isProp L→R R→L
 where
 L = Π[ x ∈ X ] ∥ Y x ∥
 R = ∥ Π Y ∥
 L-isProp = Π-of-Props {A = X} (λ x → ∥ Y x ∥) (λ x → (∥∥-isProp {X = Y x}))
 R-isProp = ∥∥-isProp {X = Π Y}
 L→R = alt-ac-fwd X Y X-isSet Yx-isSet
 R→L = alt-ac-bwd X Y X-isSet Yx-isSet

-- p. 155
-- Lemma 3.8.2

-- The UMP of Σ (Theorem 2.15.7) says that
--      Π[ x ∈ X ]  (Σ[ a ∈ A x ] P x a)   ≃    Σ[ g ∈  Π A ] Π[ x ∈ X ] P x (g x)
-- which means that
--   ∥ Π[ x ∈ X ]  (Σ[ a ∈ A x ] P x a) ∥ ≃ ∥ Σ[ g ∈  Π A ] Π[ x ∈ X ] P x (g x) ∥
-- In particular we have                    `↔`
-- and `AC` and `AC'` are equivalent.

-- I'm too lazy to do this right now…
postulate AC→AC' : AC' 𝓤 𝓥 𝓦 → AC' 𝓤 𝓥 𝓦
postulate AC'→AC : AC' 𝓤 𝓥 𝓦 → AC 𝓤 𝓥 𝓦


-- Used later on.
-- We will see at some point that this works in general when replacing `𝟙`
-- by a family over `A` of contractibles.
Σ𝟙≃ : (A : 𝓤 *) → (Σ (λ (a : A) → 𝟙)) ≃ A
Σ𝟙≃ A = f , (g , α) , (h , β)
 where
  Σ𝟙 = Σ (λ (a : A) → 𝟙)
  f : Σ𝟙 → A 
  f = fst {X = A} {Y = λ _ → 𝟙}
  g h : A → Σ𝟙
  g a = (a , ⋆)
  h a = (a , ⋆)

  α : f ∘ g ∼ 𝑖𝑑 A
  α b = refl b

  β : g ∘ f ∼ 𝑖𝑑 Σ𝟙
  β (a , ⋆) = refl (a , ⋆)

-- TODO! (used later on)
≃→Π : {X : 𝓤 *} →  {Y₀ Y₁ : (x : X) → 𝓥 *} → ((x : X) → (Y₀ x) ≃ (Y₁ x)) → Π Y₀ ≃ Π Y₁
≃→Π {X = X} {Y₀} {Y₁} E = (f , (g , α) , (h , β))
 where
  F : (x : X) → Y₀ x →  Y₁ x
  F = λ x → fst (E x)
  G H : (x : X) → Y₁ x → Y₀ x
  G = λ x → (fst (fst (snd (E x))))
  H = λ x → (fst (snd (snd (E x))))
  A : (x : X) → (F x) ∘ (G x) ∼ 𝑖𝑑 (Y₁ x)
  B : (x : X) → (H x) ∘ (F x) ∼ 𝑖𝑑 (Y₀ x)
  A = λ x → (snd (fst (snd (E x))))
  B = λ x → (snd (snd (snd (E x))))

  A≡ : (x : X) → (F x) ∘ (G x) ≡ 𝑖𝑑 (Y₁ x)
  B≡ : (x : X) → (H x) ∘ (F x) ≡ 𝑖𝑑 (Y₀ x)
  A≡ x = funext (A x)
  B≡ x = funext (B x)

  f : Π Y₀ → Π Y₁
  g h : Π Y₁ → Π Y₀
  α : f ∘ g ∼ 𝑖𝑑 (Π Y₁)
  β : h ∘ f ∼ 𝑖𝑑 (Π Y₀)

  f ϕ x = (F x) (ϕ x)
  g ψ x = (G x) (ψ x)
  h ψ x = (H x) (ψ x)

  α ϕ = funext lol
   where
    lol : f (g ϕ) ∼ ϕ
    lol x = ap (λ - → - (ϕ x) ) (A≡ x) 

  β ψ = funext lol
   where
    lol : h (f ψ) ∼ ψ
    lol x = ap (λ - → - (ψ x) ) (B≡ x) 


-- Yes I'm still stuck at 𝓤₀ because I can't lift
AC'→alt-AC :  AC' 𝓤₀ 𝓤₀ 𝓤₀ →  alt-AC 𝓤₀ 𝓤₀ 
AC'→alt-AC ac X Y X-isSet Yx-isSet f = alt-aced f
 where

  -- First, need to construct some shim types
  A = Y
  Ax-isSet = Yx-isSet
  P = λ (x : X) (_ : A x) →  𝟙
  Pxa-isProp = λ x a →  𝟙-isProp
  Σ𝟙 = λ x →  Σ[ a ∈ Y x ] 𝟙
  Σ𝟙≃Y : (x : X) → (Σ𝟙 x) ≃ (Y x)
  Σ𝟙≃Y x = Σ𝟙≃ (Y x)
  ∥∥Σ𝟙≃Y : (x : X) → ∥(Σ𝟙 x)∥ ≃ ∥ Y x ∥
  ∥∥Σ𝟙≃Y x = (≃→∥∥ (Σ𝟙≃Y x))

  ac'ed : (Π[ x ∈ X ] ∥ Σ𝟙 x ∥) → (∥  Π[ x ∈ X ] Σ𝟙 x ∥)
  ac'ed =  ac X A P X-isSet Ax-isSet Pxa-isProp
  -- Then we have to use Σ𝟙≃ above to convert to
  --   Π[ x ∈ X ] ∥  Y x ∥ → ∥  Π[ x ∈ X ] Y x ∥
  alt-aced : (Π[ x ∈ X ] ∥  Y x ∥) → (∥  Π[ x ∈ X ] Y x ∥)
  alt-aced f = g
   where
    f' : Π[ x ∈ X ] ∥ Σ𝟙 x ∥
    f' x =  ≃→bwd {A = ∥ Σ𝟙 x ∥} {B =  ∥ Y x ∥} (∥∥Σ𝟙≃Y x) (f x)
    -- it's a mouthful, but simple: we just go from `Σ𝟙 x ≃ Y x` down to an equivalence
    -- of truncations, and from this we can map `f x` to something in the desired type.
    g' : ∥  Π[ x ∈ X ] Σ𝟙 x ∥
    g' = ac'ed f'
    g  : ∥ Π[ x ∈ X ] Y x ∥
    g = ≃→fwd (≃→∥∥ (≃→Π Σ𝟙≃Y)) g'


-- This one is easier to deal with generic universes, but who cares now…
alt-AC→AC' : alt-AC 𝓤₀ 𝓤₀ → AC' 𝓤₀ 𝓤₀ 𝓤₀
alt-AC→AC' alt-ac X A P X-isSet Ax-isSet Pxa-isProp = alt-ac X Y X-isSet Yx-isSet
 where
  Y = λ x → Σ[ a ∈ A x ] P x a
  Yx-isSet : Π[ x ∈ X ] (Y x isSet)
  Yx-isSet x = Σ-of-Sets (A x)(Ax-isSet x) (P x) (λ a → Prop→Set (P x a) (Pxa-isProp x a)) 

alt-AC≃AC' : alt-AC 𝓤₀ 𝓤₀ ≃ AC' 𝓤₀ 𝓤₀ 𝓤₀
alt-AC≃AC' = bi-impliedProps→≃ L L-isProp R R-isProp L→R R→L 
 where
  L = (alt-AC 𝓤₀ 𝓤₀)
  R = AC' 𝓤₀ 𝓤₀ 𝓤₀
  postulate L-isProp : L isProp
  postulate R-isProp : R isProp
  -- We postulate by laziness: both types are function types ending with a truncation
  -- which implies that they are props.

  L→R : L → R
  L→R = alt-AC→AC' 
  R→L = AC'→alt-AC 

-- p. 157
-- Section 3.9 The principle of unique choice

-- Lemma 3.9.1
-- See Prop≃truncation

-- p. 157
-- Corollary 3.9.2

Π-of-merely-inhabited-props : {A : 𝓤 *}
                            → {P : A → 𝓥 *}
                            → ((x : A) → P x isProp)
                            → ((x : A) → ∥(P x)∥)
                            → Π P
Π-of-merely-inhabited-props {A = A} {P} props m-inhab x = ϕ p where
 p : ∥(P x)∥ 
 p = (m-inhab x)
 e : P x ≃ (∥(P x)∥ )
 e = Prop≃truncation (props x)
 ϕ : (∥(P x)∥) → P x
 ϕ = fst (fst (snd e))


-- p. 161
-- Section 3.11 Contractibility

-- Definition 3.11.1
-- See `isContr`

-- p. 162

-- Lemma 3.11.3
-- See `≃-preserves-Contr` , `𝟙-isContr` , etc

-- Lemma 3.11.4
-- See `isContr-isProp`.

-- Lemma 3.11.6
-- See `Π-of-Contr`

-- p. 163

-- Lemma 3.11.7
-- See `≲-of-Contr` 


-- Lemma 3.11.8
-- Already seen it in the equivalence between 𝕁 and ℍ

Lem-3-11-8 : {A : 𝓤 *} → (a : A) → Σ (a ≡_) isContr
Lem-3-11-8 {A = A} a = center , centrality
 where
  center : Σ (a ≡_)
  center = (a , refl a)
  centrality : (p : Σ (a ≡_)) → center ≡ p
  centrality (b , p) = Σ≡→≡ (a , refl a) (b , p) (p , transport-path-start p (refl a))

-- not the energy to do it separately, but can probably obtained from the above through the fact that the types are equivalent/
-- halalallalalalalalla I'm getting stuck

Lem-3-11-8' : {A : 𝓤 *} → (a : A) → Σ (_≡ a) isContr
Lem-3-11-8' {A = A} a = center , centrality
 where
  center : Σ (_≡ a)
  center = (a , refl a)
  centrality : (p : Σ (_≡ a)) → center ≡ p
  centrality (b , p) = (Σ≡→≡ (b , p) (a , refl a)  (p , (transport-path-self p))) ⁻¹

-- Toying with `abstract`ing stuff away
abstract 
  Lem-3-11-8'' : {A : 𝓤 *} → (a : A) →  (pp : Σ (_≡ a)) → (a , refl a) ≡ pp
  Lem-3-11-8'' {A = A} a = centrality
   where
    center : Σ (_≡ a)
    center = (a , refl a)
    centrality : (p : Σ (_≡ a)) → center ≡ p
    centrality (b , p) = (Σ≡→≡ (b , p) (a , refl a)  (p , (transport-path-self p))) ⁻¹



Σa≡-isContr = Lem-3-11-8

-- Lemma 3.11.9

Lem-3-11-9-i : {A : 𝓤 *}
             → {P : A → 𝓥 *}
             → ((a : A) → P a isContr)
             → Σ P ≃ A
Lem-3-11-9-i {A = A} {P = P} Pa-isContr = (f , (g , α) , (h , β))
 where

  c : (a : A) → P a
  c a = fst (Pa-isContr a)

  γ : (a : A) → (b : P a) → c a ≡ b
  γ a b = snd (Pa-isContr a) b

  f : Σ P → A
  f = fst

  g : A → Σ P
  g a = (a , c a)
  h : A → Σ P
  h = g

  α : f ∘ g ∼ 𝑖𝑑 A
  β : h ∘ f ∼ 𝑖𝑑 (Σ P)

  α = refl
  β (a , b) = Σ≡→≡ (a , c a) (a , b) (refl a , γ a b)



Lem-3-11-9-ii : {A : 𝓤 *}
              → {P : A → 𝓥 *}
              → (c : A isContr)
              → Σ P ≃ P (fst c)
Lem-3-11-9-ii {A = A} {P = P} (a , aa) = (f , (g , α) , (h , β))
 where
  f : Σ P → P a
  g h : P a → Σ P
  α : f ∘ g ∼ 𝑖𝑑 (P a)
  β : h ∘ f ∼ 𝑖𝑑 (Σ P)

  f (a' , b') = tr P ((aa a') ⁻¹) b'
  g b = (a , b)
  h = g

  -- α b : tr P ((aa a) ⁻¹) b ≡ b
  α b = tr P ((aa a) ⁻¹) b ≡⟨ ap (λ - → tr P - b) all-is-refl ⟩
        tr P (refl a) b    ≡⟨ refl b ⟩
        b ∎
   where
    all-is-refl : (aa a) ⁻¹ ≡ refl a
    all-is-refl = (Prop→Set A (Contr→Prop A (a , aa))) a a ((aa a) ⁻¹) (refl a)
    
  -- β (a' , b') : (a , tr P ((aa a') ⁻¹) b') ≡ (a' , b')
  β (a' , b') = Σ≡→≡ (a , tr P ((aa a') ⁻¹) b') (a' , b') (aa a' , q)
   where
    q : tr P (aa a')  (tr P ((aa a') ⁻¹) b') ≡ b'
    q = ap-tr-⁻¹' (aa a') b'

-- p. 164
-- Lemma 3.11.10
-- This says that level 1 is the same as isProp
-- See `Prop→Level1` and `Level1→Prop`
