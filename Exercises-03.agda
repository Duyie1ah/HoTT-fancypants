{-# OPTIONS --without-K --exact-split --show-implicit #-}



module Exercises-03 where


open import Chapter-03 public
open import Retraction public

-- p. 165
-- Exercise 3.1
-- See `≃-preserves-Set`

-- Exercise 3.2
-- Because the `code` code for `+` is not universe agnostic, I can't be here either
+-preserves-Set : (A : 𝓤₀ *) → A isSet
                → (B : 𝓤₀ *) → B isSet
                → (A + B) isSet
+-preserves-Set A A-isSet B B-isSet (inl a₀) (inl a₁) = inla₀≡inla₁-isProp
 where
  a₀≡a₁-isProp : (a₀ ≡ a₁) isProp
  a₀≡a₁-isProp = A-isSet a₀ a₁
  inla₀≡inla₁-isProp : ((inl a₀) ≡ (inl a₁)) isProp
  inla₀≡inla₁-isProp = ≃-preserves-Prop (Eq-2-12-1 a₀ a₁) a₀≡a₁-isProp
+-preserves-Set A A-isSet B B-isSet (inl a₀) (inr b₁) = ≃-preserves-Prop (Eq-2-12-3 a₀ b₁)  𝟘-isProp 
+-preserves-Set A A-isSet B B-isSet (inr b₀) (inl a₁) = ≃-preserves-Prop (Eq-2-12-4 b₀ a₁)  𝟘-isProp
+-preserves-Set A A-isSet B B-isSet (inr b₀) (inr b₁) = inrb₀≡inrb₁-isProp
 where
  b₀≡b₁-isProp : (b₀ ≡ b₁) isProp
  b₀≡b₁-isProp = B-isSet b₀ b₁
  inrb₀≡inrb₁-isProp : ((inr b₀) ≡ (inr b₁)) isProp
  inrb₀≡inrb₁-isProp = ≃-preserves-Prop (Eq-2-12-2 b₀ b₁) b₀≡b₁-isProp

-- useful
𝟘-level-sn : (n : ℕ) → 𝟘 isLevel (succ n)
𝟘-level-sn zero = Prop→Level1 𝟘 𝟘-isProp
𝟘-level-sn (succ n) x y = !𝟘 ((x ≡ y) isLevel (succ n)) x

-- In fact, `_+_` preserves all higher levels than prop.
+-preserves-Level : (n : ℕ)
                → (A : 𝓤₀ *) → A isLevel (succ (succ n))
                → (B : 𝓤₀ *) → B isLevel(succ (succ n))
                → (A + B) isLevel(succ (succ n))
+-preserves-Level n A A-islvlss B B-islvlss (inl a₀) (inl a₁) = inla₀≡inla₁-islvls
 where
  a₀≡a₁-islvls : (a₀ ≡ a₁) isLevel (succ n)
  a₀≡a₁-islvls = A-islvlss a₀ a₁
  inla₀≡inla₁-islvls : ((inl a₀) ≡ (inl a₁)) isLevel (succ n)
  inla₀≡inla₁-islvls = ≃-Level (Eq-2-12-1 a₀ a₁) a₀≡a₁-islvls
+-preserves-Level n A A-islvlss B B-islvlss (inl a₀) (inr b₁) = ≃-Level  (Eq-2-12-3 a₀ b₁)  (𝟘-level-sn n)
+-preserves-Level n A A-islvlss B B-islvlss (inr b₀) (inl a₁) = ≃-Level (Eq-2-12-4 b₀ a₁)  (𝟘-level-sn n)
+-preserves-Level n A A-islvlss B B-islvlss (inr b₀) (inr b₁) = inrb₀≡inrb₁-islvls
 where
  b₀≡b₁-islvls : (b₀ ≡ b₁) isLevel (succ n)
  b₀≡b₁-islvls = B-islvlss b₀ b₁
  inrb₀≡inrb₁-islvls : ((inr b₀) ≡ (inr b₁)) isLevel (succ n)
  inrb₀≡inrb₁-islvls = ≃-Level (Eq-2-12-2 b₀ b₁) b₀≡b₁-islvls


-- p. 165

-- Exercise 3.3
-- See `Σ-of-Sets`

-- Exercise 3.4
isProp→EndoisContr : (A : 𝓤 *) → A isProp → (A → A) isContr
isProp→EndoisContr A A-isProp = center , centrality
 where
  center : A → A
  center = 𝑖𝑑 A
  centrality∼ : (f : A → A) → center ∼ f
  centrality∼ f a = A-isProp a (f a)
  centrality : (f : A → A) → center ≡ f
  centrality f = funext (centrality∼ f) 

EndoisContr→isProp : (A : 𝓤 *) → (A → A) isContr → A isProp
EndoisContr→isProp A contr a₀ a₁ = a₀     ≡⟨ refl a₀ ⟩
                                     𝑖𝑑 A a₀ ≡⟨ ap (λ - → - a₀) id≡ca₁ ⟩
                                     ca₁ a₀ ≡⟨ refl a₁ ⟩
                                     a₁ ∎
 where
  ca₁ = λ (_ : A) → a₁
  id≡ca₁ : 𝑖𝑑 A ≡ ca₁
  id≡ca₁ = (Contr→Prop (A → A) contr) (𝑖𝑑 A)  ca₁


-- Exercise 3.5

Exe-3-5 : (A : 𝓤 *) → (A isProp) ≃ (A → A isContr)
Exe-3-5 A = bi-impliedProps→≃ L L-isProp R R-isProp L→R R→L
 where
  L = A isProp
  R = A → A isContr
  L-isProp : L isProp
  L-isProp = isProp-isProp A
  R-isProp : R isProp
  R-isProp = Π-of-Props (λ _ → A isContr) (λ _ → isContr-isProp A ) 

  L→R : L → R
  L→R A-isProp a = inhabitedProp→Contr A a A-isProp
  R→L : R → L 
  R→L →Contr a₀ a₁ = Contr→Prop A (→Contr a₀) a₀ a₁


-- p. 166
-- Exercise 3.6 (skipped in favour of 3.7)

-- once again: need to restrict our universes to 𝓤₀ :(
Exe-3-7 : (A : 𝓤₀ *) → A isProp
        → (B : 𝓤₀ *) → B isProp
        → ¬ (A × B)
        → (A + B) isProp 
Exe-3-7 A A-isProp B B-isProp notboth (inl a₀) (inl a₁) = ap inl (A-isProp a₀ a₁)  
Exe-3-7 A A-isProp B B-isProp notboth (inr b₀) (inr b₁) = ap inr (B-isProp b₀ b₁)  
Exe-3-7 A A-isProp B B-isProp notboth (inl a₀) (inr b₁) = !𝟘 (inl a₀ ≡ inr b₁) (notboth (a₀ , b₁))  
Exe-3-7 A A-isProp B B-isProp notboth (inr b₀) (inl a₁) = !𝟘 (inr b₀ ≡ inl a₁) (notboth (a₁ , b₀))  
-- Easy peasy: The only thing failing for A + B to be a prop if both A and B are would be the fact that
-- left is not equal to right, so we just ensure this never happens

-- p. 166
-- Exercise 3.8
-- The 3 properties mentioned in the exercise are:
--  i. there is a map from hasqinv f to isequiv f
--  ii. there is a map from isequiv f to hasqinv f
--  iii. isequiv f is a prop
-- Only the third one is not already known

Exe-3-8 : {A : 𝓤 *}
        → {B : 𝓥 *}
        → (f : A → B)
        → (isequiv f) isProp                        -- third property
        → ∥ hasqinv f ∥ ≃ (isequiv f) 
Exe-3-8 f p = bi-impliedProps→≃ L L-isProp R R-isProp L→R R→L
 where
  L = ∥ hasqinv f ∥
  L-isProp : L isProp
  L-isProp = ∥∥-isProp 
  R = isequiv f
  R-isProp : R isProp
  R-isProp = p
  L→R = ∥∥-recursion p (λ q → hasqinv→isequiv f q)
  R→L = λ e → ∣ isequiv→hasqinv f e ∣ 


inhabitedProp≡𝟙 : (A : 𝓤₀ *) → A isProp → A → A ≡ 𝟙
inhabitedProp≡𝟙 A A-isProp a = ua A≃𝟙
 where
  A≃𝟙 : A ≃ 𝟙
  A≃𝟙 = (f , (g , α) , (h , β))
   where
    f : A → 𝟙
    f _ = ⋆ 
    g : 𝟙 → A
    g _ = a
    h = g
    α : f ∘ g ∼ 𝑖𝑑 𝟙
    α ⋆ = refl ⋆
    β : h ∘ f ∼ 𝑖𝑑 A
    β a' = A-isProp a a'

inhabitedProp≡𝟙' : (𝔸 : Props {𝓤₀}) → (fst 𝔸) → 𝔸 ≡ (𝟙 , 𝟙-isProp)
inhabitedProp≡𝟙' (A , A-isProp) a = Σ≡→≡ (A , A-isProp) (𝟙 , 𝟙-isProp) (p , q)
 where
  p : A ≡ 𝟙
  p = inhabitedProp≡𝟙 A A-isProp a
  q : tr _isProp p A-isProp ≡ 𝟙-isProp
  q = isProp-isProp 𝟙 (tr _isProp p A-isProp) 𝟙-isProp

negatedProp≡𝟘 : (A : 𝓤₀ *) → A isProp → ¬ A → A ≡ 𝟘
negatedProp≡𝟘 A A-isProp negA = ua A≃𝟘
 where
  A≃𝟘 : A ≃ 𝟘
  A≃𝟘 = (f , (g , α) , (h , β))
   where
    f = negA
    g = !𝟘 A
    h = g
    α = λ (x : 𝟘) → !𝟘 (f (g x) ≡ x) x
    β = λ (a : A) → !𝟘 (h (f a) ≡ a) (f a)


-- Can remove the `_isProp` assumption: from a map to `𝟘` propositionality
-- follows.
negatedProp≡𝟘' : (𝔸 : Props {𝓤₀}) → ¬ (fst 𝔸) → 𝔸 ≡ (𝟘 , 𝟘-isProp)
negatedProp≡𝟘' (A , A-isProp) neg-A = Σ≡→≡ (A , A-isProp) (𝟘 , 𝟘-isProp) (p , q)
 where
  p : A ≡ 𝟘
  p = negatedProp≡𝟘 A A-isProp neg-A
  q : tr _isProp p A-isProp ≡ 𝟘-isProp
  q = isProp-isProp 𝟘 (tr _isProp p A-isProp) 𝟘-isProp

  
𝔹→type : 𝔹 → 𝓤₀ *
𝔹→type = 𝔹-induction (λ _ → 𝓤₀ *) 𝟙 𝟘

𝕥≢𝕗 : ¬ (𝕥 ≡ 𝕗)
𝕥≢𝕗 p = tr 𝔹→type p ⋆

!𝕥≡𝕗 : (A : 𝓤 *) → (p : 𝕥 ≡ 𝕗) → A
!𝕥≡𝕗 A p = !𝟘 A (𝕥≢𝕗 p)

Exe-3-9 : LEM {𝓤₀} → Props {𝓤₀} ≃ 𝔹
Exe-3-9 lem' = (f , (g , α) , (h , β))
 where


  -- Just make `lem` into a function that takes `ℙ : Props`
  lem : (ℙ : Props {𝓢 = 𝓤₀}) → fst ℙ + ¬ (fst ℙ)
  lem (P , P-isProp) = lem' P P-isProp

  
  𝟘𝟘 = (𝟘 , 𝟘-isProp)
  𝟙𝟙 = (𝟙 , 𝟙-isProp)

  -- Given a proposition and a choice of whether or not it holds, spit out the result
  f* : (ℙ : Props {𝓢 = 𝓤₀}) → (fst ℙ + ¬ (fst ℙ)) → 𝔹
  f* ℙ lemP = +-recursion (λ _ → 𝕥) (λ _ → 𝕗) lemP

  -- Use `f*` to map any proposition to a boolean
  -- We make the detour through `f*` because we need to be able to handle the choice `lem ℙ` explicitely later on
  f : Props {𝓢 = 𝓤₀} → 𝔹
  f ℙ = f* ℙ (lem ℙ)

  -- Applying `f*` to `𝟘𝟘` yields `𝕗`, as expected.
  -- Here we need to do this thing they call "inspect idiom".
  -- I.e. we "pattern match" on the result of `lem 𝟘𝟘`, but by looking at an arbitrary `x : 𝟘 + ¬ 𝟘`, plus an equality of this `x` with `lem 𝟘𝟘`
  f*𝟘≡𝕗 : f* 𝟘𝟘 (lem 𝟘𝟘) ≡ 𝕗
  f*𝟘≡𝕗 = +-induction (λ (x : 𝟘 + ¬ 𝟘) → (p : lem 𝟘𝟘 ≡ x) → f* 𝟘𝟘 (lem 𝟘𝟘) ≡ 𝕗)
                    (λ (x : 𝟘) → λ _ → !𝟘 (f* 𝟘𝟘 (lem 𝟘𝟘) ≡ 𝕗) x )                    -- from `x: 𝟘` we're happy no matter what
                    (λ (x : ¬ 𝟘) → λ (p : lem 𝟘𝟘 ≡ inr x) →  ap (f* 𝟘𝟘) p )           -- from an equality `p : lem 𝟘𝟘 ≡ inr x` with `x : ¬ 𝟘`, we apply (f* 𝟘𝟘), and the case definition of f* 𝟘𝟘 ensures that the result (at `x`) is `𝕗`.
                    (lem 𝟘𝟘)
                    (refl (lem 𝟘𝟘))
  f*𝟙≡𝕥 : f* 𝟙𝟙 (lem 𝟙𝟙) ≡ 𝕥
  f*𝟙≡𝕥 = +-induction (λ (x : 𝟙 + ¬ 𝟙) → (p : lem 𝟙𝟙 ≡ x) → f* 𝟙𝟙 (lem 𝟙𝟙) ≡ 𝕥)
                    (λ (x : 𝟙) → λ (p : lem 𝟙𝟙 ≡ inl x) →  ap (f* 𝟙𝟙) p )
                    (λ (x : ¬ 𝟙) → λ _ → !𝟘 (f* 𝟙𝟙 (lem 𝟙𝟙) ≡ 𝕥) (x ⋆))
                    (lem 𝟙𝟙)
                    (refl (lem 𝟙𝟙))


  -- By def of `f`, the types are the same
  f𝟘≡𝕗 : f 𝟘𝟘 ≡ 𝕗
  f𝟘≡𝕗 = f*𝟘≡𝕗
  f𝟙≡𝕥 : f 𝟙𝟙 ≡ 𝕥
  f𝟙≡𝕥 = f*𝟙≡𝕥

  -- `g,h` defined as expected
  g : 𝔹 → Props {𝓢 = 𝓤₀}
  g 𝕗 = 𝟘𝟘
  g 𝕥 = 𝟙𝟙
  h : 𝔹 → Props {𝓢 = 𝓤₀}
  h = g

  -- Here we inspect again
  α : f ∘ g ∼ 𝑖𝑑 𝔹
  α 𝕥 = 𝔹-induction (λ b' → (p : f (g 𝕥) ≡ b') → f (g 𝕥) ≡ 𝕥)
                    (λ (p : f (g 𝕥) ≡ 𝕥) → p)
                    (λ (p : f (g 𝕥) ≡ 𝕗) → !𝕥≡𝕗 ((f (g 𝕥)) ≡ 𝕥) ((f𝟙≡𝕥 ⁻¹) ∙ p)) -- if `p : fg𝕥 ≡ 𝕗`, recall also that `f𝟙 ≡ 𝕥` by above, and that `g𝕥 ≡ 𝟙` by def --> get a contradiction
                    (f (g 𝕥))
                    (refl (f (g 𝕥)))
  α 𝕗 = 𝔹-induction (λ b' → (p : f (g 𝕗) ≡ b') → f (g 𝕗) ≡ 𝕗)
                    (λ (p : f (g 𝕗) ≡ 𝕥) → !𝕥≡𝕗 ((f (g 𝕗)) ≡ 𝕗) ((p ⁻¹) ∙ f𝟘≡𝕗) )
                    (λ (p : f (g 𝕗) ≡ 𝕗) → p )
                    (f (g 𝕗))
                    (refl (f (g 𝕗)))
                    
  
  -- Here we inspect on the result of `lem ℙ`
  β' : (ℙ : Props {𝓢 = 𝓤₀}) →  h (f* ℙ (lem ℙ)) ≡ ℙ
  β' (P , Pr) = +-induction (λ (x : P + ¬ P) → (lemP ≡ x) → h (f ℙ) ≡ ℙ)
                           (λ (x : P) (p : lemP ≡ inl x) → ( ap h ( (ap (f* ℙ) p))  ∙  (inhabitedProp≡𝟙' ℙ x) ⁻¹ ) )
                           (λ (x : ¬ P) (p : lemP ≡ inr x) → ( ap h (ap (f* ℙ) p)  ∙ (negatedProp≡𝟘' ℙ x) ⁻¹ ))
                           lemP
                           (refl lemP)
   where
    ℙ = (P , Pr)
    lemP = lem ℙ
  

  β : h ∘ f ∼ 𝑖𝑑 (Props {𝓢 = 𝓤₀})
  β = β'
  
-- Exercise 3.10
-- Can't do in agda because I'm confused about lifting and prop resizing :(

-- Exercise 3.11

postulate Exe-3-11 : {𝓤 𝓥 : Universe} →  ¬ (Π[ A ∈ 𝓥 * ] (∥ A ∥ → A))
-- informally I'd see it like that:
-- From the hypothesis, we get that for any `X : 𝓤` and `Y : (x : X) → 𝓥`,
-- any element of `Π ∥ Yx ∥` can be mapped to an element of `Π Y` simply by
-- post-composition with the map `∥ Y x ∥ → Y x` given by the hypothesis.
-- It follows that
--   Π[ x : X ] ∥ Y x ∥ → ∥ Π[ x : X ] Y x ∥
-- is inhabited for any `X` and `Y` sufficiently low in our universe.
-- But this type is in general not inhabited.
-- A counter-example is Lemma 3.8.5
{-
Exe-3-11 F = {!!}
 where
  alt-ac : alt-AC 𝓤 𝓥
  alt-ac X Y X-isSet Yx-isSet f = ∣ f' ∣
   where
    f' : (x : X) → Y x
    f' x = {! (F ({! Y x !})) {! (f x) !} !}
-}

-- p. 166
-- Exercise 3.12
-- Exercise 3.13
-- TODO

-- p. 166
-- Exercise 3.14

at : {A : 𝓤 *} → (a : A) → ¬¬ A
at a f = f a

exe-3-14 : LEM {𝓢 = 𝓤₀} → (A : 𝓤₀ *) → (( (¬¬ A) isProp) × ( Σ[ l ∈( A → ¬¬ A) ] ((B : 𝓤₀ *) → B isProp → (f : A → B) → Σ[ f' ∈ ((¬¬ A) → B) ] Π[ a ∈ A ] ( f' (l a) ≡ (f a) ) )))
exe-3-14 lem A = (¬-isProp (¬ A)) , at {A = A} , ϕ
 where
  ϕ : ((B : 𝓤₀ *) → B isProp → (f : A → B) → Σ[ f' ∈ ((¬¬ A) → B) ] Π[ a ∈ A ] ( f' (at {A = A} a) ≡ (f a) ))
  ϕ B B-isProp f = f' , ψ
   where
    f' : ¬¬ A → B
    f' = +-recursion 
                     (λ (b : B) → λ _ → b)
                     ( λ (β : ¬ B) → λ (x : ¬¬ A) → !𝟘 B (x (β ∘ f)) )
                     (lem B B-isProp) 
    ψ : Π[ a ∈ A ] ( f' (at {A = A} a) ≡ (f a) )
    ψ a = B-isProp (f' (at a)) (f a) 
                                
-- p. 166
-- Exercise 3.15
-- TODO


-- Exercise 3.16
exe-3-16 : DNE {𝓢 = 𝓥} → (X : 𝓥 *) → X isSet → (Y : X → 𝓥 *) → ((x : X) → Y x isProp)
         → (Π[ x ∈  X ] ¬¬ (Y x) ) ≃ ( ¬¬ (Π[ x ∈ X ] Y x))
exe-3-16 dne X X-isSet Y Yx-isProp = bi-impliedProps→≃ L L-isProp R R-isProp L→R R→L
 where
  L = Π[ x ∈ X ] ¬¬ (Y x)
  R = ¬¬ (Π[ x ∈ X ] Y x)
  L-isProp : L isProp
  L-isProp = Π-of-Props (λ x → ¬¬ (Y x)) (λ x → ¬¬-isProp (Y x))
  R-isProp = ¬¬-isProp (Π[ x ∈ X ] Y x)
  L→R : L → R
  L→R f = at (λ x → dne (Y x) (Yx-isProp x) (f x))
  R→L : R → L
  R→L g = λ x → at ((dne (Π[ x ∈ X ] Y x) (Π-of-Props Y (λ x → Yx-isProp x))) g x)
  -- Essentially just converting back and forth using DNE

-- p. 167
-- Exercise 3.17
exe-3-17 : (A : 𝓤 *) → (B : ∥ A ∥ → 𝓥 *) → ((x : ∥ A ∥) → (B x) isProp)
         → (Π[ a ∈ A ]  B ∣ a ∣) → (Π[ x ∈ ∥ A ∥ ] B x)
exe-3-17 {𝓤} {𝓥} A B Ba-isProp f = g
 where
  ΣB = Σ B
  ΣB-isProp : ΣB isProp
  ΣB-isProp = Σ-of-props ∥ A ∥  ∥∥-isProp B Ba-isProp

  f' : A → ΣB
  f' a = ∣ a ∣ , f a

  g' : ∥ A ∥ → ΣB
  g' = ∥∥-recursion ΣB-isProp f'

  postulate g : Π B
  -- g x = snd (g' x)

-- p. 167
-- Exercise 3.18
-- done elsewhere (DNE→LEM and friends)

-- p. 167
-- Exercise 3.19
-- TODO

-- Exercise 3.20 Done elsewhere

-- p. 167
-- Exercise 3.21
exe-3-21 : (P : 𝓤 *) → (P isProp) ≃ (P ≃ ∥ P ∥)
exe-3-21 P = bi-impliedProps→≃ L L-isProp R R-isProp L→R R→L
 where
  L = P isProp 
  R =  (P ≃ ∥ P ∥)
  L-isProp : L isProp
  L-isProp = isProp-isProp P
  postulate R-isProp : R isProp -- P ≃ ∥ P ∥ is Σ[ f : P → ∥ P ∥ ] (isequiv f) .
                                -- The type P → ∥P∥ is a prop since ∥P∥ is, and (isequiv f) is a prop (not yet checked, but assumed in the book)
                                -- Thus the Σ is a prop.
                                -- This is actually general: the type P ≃ Q is a prop if Q is a prop, and so is _ ≃ Q, since it's a product over props!! (if I'm not mistaken) 
  postulate L→R : L → R -- If P is a Prop, then the identity on P factors through ∥P∥ and we get a maps forward/backward, and since both are props, an equivalence
  postulate R→L : R → L -- ≃ preserves Prop, and ∥P∥ is a prop
  


-- p. 167
-- Exercise 3.22
-- Exercise 3.23
-- TODO
