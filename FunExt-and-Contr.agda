{-# OPTIONS --without-K --exact-split --show-implicit #-}

-- This file follows the 1lab partially at least, too


module FunExt-and-Contr where


open import Chapter-03 public
open import Retraction public



ProductContractibility : (𝓢 𝓣 : Universe) → ((𝓢 ⁺) ⊔ (𝓣 ⁺)) *
ProductContractibility 𝓢 𝓣 = (A : 𝓢 *) (B : A → 𝓣 *)
                            → ((x : A) → B x isContr) → Π B isContr
ProductProposition : (𝓢 𝓣 : Universe) → ((𝓢 ⁺) ⊔ (𝓣 ⁺)) *
ProductProposition 𝓢 𝓣 = (A : 𝓢 *) (B : A → 𝓣 *)
                            → ((x : A) → B x isProp) → Π B isProp

-- This is done in the `Based-Path-Induction` file also.
Σ-≡-isContr : {A : 𝓤 *} → (a : A) → Σ (a ≡_) isContr
Σ-≡-isContr a = center , centrality
 where
  center = (a , refl a)
  centrality : (x : Σ (a ≡_)) → center ≡ x
  centrality (a , refl a) = refl (a , refl a)


fiberwise   : {A : 𝓤 *}
              {B₀ : A → 𝓥 *}
              {B₁ : A → 𝓥 *}
              (f : (x : A) → B₀ x → B₁ x)
            → Σ B₀ → Σ B₁
fiberwise f (x , b) = (x , f x b)



Σ≃-fiber : {A : 𝓤 *}
              {B₀ : A → 𝓥 *}
              {B₁ : A → 𝓥 *}
              {f : (x : A) → B₀ x → B₁ x}
             → (isequiv (fiberwise f))
             → (a : A)
             → isequiv (f a)
Σ≃-fiber {A = A} {B₀} {B₁} {f} ((g , α) , (h , β)) a =  (ga , αa) , (ha , βa)
 where
  fa : B₀ a → B₁ a
  postulate 
    ga ha : B₁ a → B₀ a
    αa : fa ∘ ga ∼ 𝑖𝑑 (B₁ a)
    βa : ha ∘ fa ∼ 𝑖𝑑 (B₀ a)
  fa = f a
  -- Rest TODO



PC→FE : (𝓢 𝓣 : Universe)
      → ProductContractibility 𝓢 𝓣
      → FunctionExtensionality 𝓢 𝓣 
PC→FE 𝓢 𝓣 PC {A = A} {B = B} {f} {g}  = Σ≃-fiber e g
 where
  hap = λ g → happly {A = A} {B = B} {f} {g}

  f∼ = Σ (f ∼_)
  f≡ = Σ (f ≡_)

  f∼-isContr : f∼ isContr
  f∼-isContr =  ≲-of-Contr ret yolo-isContr
   where
    yolo = (x : A) → Σ ( (f x) ≡_ )
    yolo-isContr : yolo isContr
    yolo-isContr = PC A (λ (x : A) → Σ ((f x) ≡_)) (λ x → Σ-≡-isContr (f x)) 
    -- yolo is the Π-type `Π[x : A] Σ[y : B x] f x ≡ y`.
    -- By assumption that products of contractibles are contractible,
    -- it suffices to check that `Σ[y : B x] f x ≡ y` is contractible.
    -- But we know this to be true
    ret : f∼ ≲ yolo
    ret = (s , r , α)
     where
      s : f∼ → yolo
      r : yolo → f∼
      α : r ∘ s ∼ 𝑖𝑑 f∼

      s (g , H) = λ a → g a , H a
      r ϕ =  (λ x → fst (ϕ x) ) , (λ x → snd (ϕ x)) 
      α (g , H) = refl (g , H)
      -- Note: we can't just say `α p = refl p` without pattern matching
      -- because of the lack of η for sums.
      -- Assuming pattern-matched, `α` is just `refl` :
      --   r (s (g , H))
      --   = r (λ a → (g a , H a)) (definitionally)
      --   = λ a → fst ((λ a → (g a , H a)) (a)) , λ a → snd ((λ a → (g a , H a)) (a))
      --   = λ a → fst ((g a , H a)) , λ a → snd ((g a , H a))
      --   = λ a → g a , λ a → H a)
      --   = g         , H
      --   by eta on functions

  f≡-isContr : f≡ isContr
  f≡-isContr = Σ-≡-isContr f 

  h : f≡ → f∼
  h = fiberwise (λ g → happly {A = A} {B = B} {f} {g})
  e : isequiv h
  e = impliedContr→≃ f≡ f≡-isContr  f∼ f∼-isContr  h
  
PP→FE : (𝓢 𝓣 : Universe)
      → ProductProposition 𝓢 𝓣
      → ProductContractibility 𝓢 𝓣
PP→FE 𝓢 𝓣 PP A B Ct  = center , centrality
 where
  center : Π B
  center = λ a → fst (Ct a)
  centrality : (f : Π B) → center ≡ f
  centrality f =   PP A B (λ a → Contr→Prop (B a) (Ct a)) center f
 




