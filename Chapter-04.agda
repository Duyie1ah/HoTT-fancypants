{-# OPTIONS --without-K --exact-split --show-implicit #-}



module Chapter-04 where


open import Chapter-03 public
open import Exercises-03 public
open import Exercises-02 public


_≃⟨_⟩_ : (A : 𝓤 *) {B : 𝓥 *} {C : 𝓦 *} → A ≃ B → B ≃ C → A ≃ C
A ≃⟨ p ⟩ q = ≃-tran p q

_≃∎ :  (A : 𝓤 *) → A ≃ A
A ≃∎ = ≃-refl

infixr  0 _≃⟨_⟩_
infix   1 _≃∎




hasqinv→precomp-hasqinv : {A : 𝓤 *} {B : 𝓥 *} {C : 𝓦 *} (f : A → B) → (hasqinv f) → hasqinv (λ (h : B → C) → h ∘ f)
hasqinv→precomp-hasqinv {A = A} {B = B} {C = C} f (g , α , β) = ∘g , ∘α , ∘β
 where

  ∘f = λ (h : B → C) → h ∘ f
  ∘g = λ (h : A → C) → h ∘ g

  a : f ∘ g ≡ id
  a = funext α
  b : g ∘ f ≡ id
  b = funext β

  ∘α : ∘f ∘ ∘g ∼ id 
  ∘α h = (∘f ∘ ∘g) h ≡⟨ refl ((∘f ∘ ∘g) h) ⟩
         (h ∘ g) ∘ f ≡⟨ refl ((h ∘ g) ∘ f) ⟩
         h ∘ (g ∘ f) ≡⟨ ap (h ∘_ ) b ⟩
         h ∎

  ∘β : ∘g ∘ ∘f ∼ id
  ∘β h = ap (h ∘_) a

hasqinv→postcomp-hasqinv : {A : 𝓤 *} {B : 𝓥 *} {C : 𝓦 *} (f : A → B) → (hasqinv f) → hasqinv (λ (h : C → A) → f ∘ h)
hasqinv→postcomp-hasqinv {A = A} {B = B} {C = C} f (g , α , β) = g∘ , α∘ , β∘ 
 where

  f∘ = λ (h : C → A) → f ∘ h
  g∘ = λ (h : C → B) → g ∘ h

  a : f ∘ g ≡ id
  a = funext α
  b : g ∘ f ≡ id
  b = funext β

  α∘ : f∘ ∘ g∘ ∼ id 
  α∘ h = ap (_∘ h) a

  β∘ : g∘ ∘ f∘ ∼ id
  β∘ h = ap (_∘ h) b


isequiv→postcomp-isequiv : {A : 𝓤 *} {B : 𝓥 *} {C : 𝓦 *} (f : A → B) → (isequiv f) → isequiv (λ (k : C → A) → f ∘ k)
isequiv→postcomp-isequiv {A = A} {B = B} {C = C} f ((g , α) , (h , β)) = ((g∘ , α∘) , (h∘ , β∘))
 where
  
  g∘ = λ (k : C → B) → g ∘ k
  h∘ = λ (k : C → B) → h ∘ k
  f∘ = λ (k : C → A) → f ∘ k
  
  a : f ∘ g ≡ id
  a = funext α
  b : h ∘ f ≡ id
  b = funext β

  α∘ : f∘ ∘ g∘ ∼ id 
  α∘ k = ap (_∘ k) a

  β∘ : h∘ ∘ f∘ ∼ id
  β∘ k = ap (_∘ k) b

-- Note: `isequiv→precomp-isequiv` is not as easy, because of the "twist" it unduces between `α` and `β`

Σ-of-≃ : {A : 𝓤 *} {B₀ B₁ : A → 𝓥 *}  → ((x : A) → B₀ x ≃ B₁ x) → Σ B₀ ≃ Σ B₁
Σ-of-≃ {A = B} {B₀} {B₁} e = f , ((g , α) , (h , β))
 where
  f : Σ B₀ → Σ B₁
  g h  : Σ B₁ → Σ B₀

  α : f ∘ g ∼ id
  β : h ∘ f ∼ id

  f (x , b) = (x , fst (e x) b)
  g (x , b) = (x , (fst (fst (snd (e x)))) b)
  h (x , b) = (x , (fst (snd (snd (e x)))) b)

  α (x , b) = Σ≡→≡' (refl x) (snd (fst (snd (e x))) b)
  β (x , b) = Σ≡→≡' (refl x) (snd (snd (snd (e x))) b)
 

-- p. 170
-- Lemma 4.1.1

lem-4-1-1-base₀ : (A : 𝓤 *) → hasqinv (𝑖𝑑 A) ≃ Π (λ (x : A) → x ≡ x)
lem-4-1-1-base₀ A = hasqinv (𝑖𝑑 A)                                          ≃⟨ ≃-refl ⟩
                    (Σ[ g ∈ (A → A) ] ((g ∼ id) × (g ∼ id)))               ≃⟨  missingpiece1 ⟩ -- For each `g`, have `(g ∼ id) ≃ (g ≡ id)`, so passes to the sum space
                    (Σ[ g ∈ (A → A) ] ((g ≡ id) × (g ≡ id)))                ≃⟨  missingpiece2 ⟩          
                    (Σ[ G ∈ (Σ[ g ∈ (A → A) ] (g ≡ id)) ] ((fst G) ≡ id))   ≃⟨ Lem-3-11-9-ii (contra) ⟩ -- Contra has the equality backwards… should be easy to correct but I get confused
                    (fst (fst contra) ≡ 𝑖𝑑 A)                                 ≃⟨ ≃-refl ⟩ -- Lemma 3-11-8 has a good center
                    (𝑖𝑑 A ≡ 𝑖𝑑 A)                                             ≃⟨  (happly {f = 𝑖𝑑 A} {g = 𝑖𝑑 A} , FE) ⟩
                    (𝑖𝑑 A ∼ 𝑖𝑑 A)                                             ≃⟨ ≃-refl ⟩
                    (Π[ x ∈ A ] (x ≡ x))                                    ≃∎
 where
  contra : (Σ[ g ∈ (A → A) ] (g ≡ id)) isContr
  contra = (𝑖𝑑 A , refl (𝑖𝑑 A)) , Lem-3-11-8'' (𝑖𝑑 A)
  contra-good : fst (fst contra) ≡ id
  contra-good = refl (𝑖𝑑 A)

  {-

exe-2-10 : (A : 𝓤 *) (B : A → 𝓥 *) (C : (Σ B) → 𝓦 *)
         → (Σ[ x ∈ A ] (Σ[ y ∈ B x ] C (x , y)) ) ≃ (Σ C)

 -}
  
  postulate missingpiece1 : (Σ[ g ∈ (A → A) ] ((g ∼ id) × (g ∼ id))) ≃ (Σ[ g ∈ (A → A) ] ((g ≡ id) × (g ≡ id)))
  -- funext plus that Σ preserves equivalence in its fibers
  missingpiece2 :  (Σ[ g ∈ (A → A) ] ((g ≡ id) × (g ≡ id))) ≃  (Σ[ G ∈ (Σ[ g ∈ (A → A) ] (g ≡ id)) ] ((fst G) ≡ id))
  missingpiece2 = exe-2-10 (A → A) (λ (g : A → A) → g ≡ id) (λ G → fst G ≡ id)        
                    

               

lem-4-1-1-base : {A B : 𝓤 *} (p : A ≡ B) → hasqinv (fst (id→eqv p)) ≃ Π (λ (x : A) → x ≡ x)
lem-4-1-1-base {A = A} {B = B} p = 𝕁 (type-of A)
                                     (λ (A B : type-of A) (p : A ≡ B) →  hasqinv (fst (id→eqv p)) ≃ Π (λ (x : A) → x ≡ x))
                                     lem-4-1-1-base₀
                                     A B p

lemma-4-1-1 : {A B : 𝓤 *} (E : A ≃ B) → hasqinv (fst E) ≃ Π (λ (x : A) → x ≡ x)
lemma-4-1-1 {A = A} {B = B} (f , e) =  tr (λ (g : A → B) → (hasqinv g ≃ Π (λ (x : A) → x ≡ x))) α₁ (lem-4-1-1-base p)
 where
  p : A ≡ B
  p = ua (f , e)
  α : id→eqv p ≡ (f , e)
  α = UV-α (f , e)
  α₁ : fst (id→eqv p) ≡ f
  α₁ = ap fst α

lemma-4-1-1-proper : {A B : 𝓤 *} (f : A → B) → hasqinv f → hasqinv f ≃ Π (λ (x : A) → x ≡ x)
lemma-4-1-1-proper f Q = lemma-4-1-1 (f , hasqinv→isequiv f Q)

-- p. 171
-- Lemma 4.1.2

lemma-4-1-2-pt1 : {A : 𝓤 *} {a : A} (q : a ≡ a) → (a ≡ a) isSet → ( (x : A) →  ∥ a ≡ x ∥ ) → ( ( x y : A ) →  (x ≡ y) isSet )
lemma-4-1-2-pt1 {A = A} {a} q a-set g x y = k'' (g x) (g y)
 where
  k : (p : a ≡ x) → (q : a ≡ y) → (x ≡ y) isSet
  k p q = ≃-preserves-Set lol a-set
   where
    lol : (x ≡ y) ≃ (a ≡ a)
    lol = (x ≡ y) ≃⟨ ∙-right→equiv (q ⁻¹) ⟩
          (x ≡ a) ≃⟨ ∙-left→equiv p ⟩
          (a ≡ a) ≃∎
  k' :  (a ≡ x) → ∥ a ≡ y ∥ → (x ≡ y) isSet
  k' p = ∥∥-recursion (isSet-isProp (x ≡ y)) (k p)
  k'' : ∥ a ≡ x ∥ → ∥ a ≡ y ∥ → (x ≡ y) isSet
  k'' = ∥∥-recursion (Π-of-Props (λ - → (x ≡ y) isSet) (λ - → isSet-isProp (x ≡ y))  ) k'



-- p. 176
-- Definition 4.2.7

linv :  {A : 𝓤 *} {B : 𝓥 *} (f : A → B) → (𝓤 ⊔ 𝓥)*
linv {A = A} {B = B} f = Σ[ g ∈ (B → A) ] ((g ∘ f) ∼ id)
rinv :  {A : 𝓤 *} {B : 𝓥 *} (f : A → B) → (𝓤 ⊔ 𝓥)*
rinv {A = A} {B = B} f = Σ[ g ∈ (B → A) ] (f ∘ g ∼ id)


-- p. 173
-- Definition 4.2.1
-- I cheat and use concepts introduced later on

lcoh :  {A : 𝓤 *} {B : 𝓥 *} {f : A → B} (G : linv f)  → (𝓤 ⊔ 𝓥) *
lcoh {A = A} {B = B} {f} (g , η) = Σ[ ε ∈ (f ∘ g ∼ id) ] (Π[ y ∈ B ] (ap g (ε y) ≡ η (g y)))


rcoh :  {A : 𝓤 *} {B : 𝓥 *} {f : A → B} (G : rinv f)  → (𝓤 ⊔ 𝓥) *
rcoh {A = A} {B = B} {f} (g , ε) = Σ[ η ∈ (g ∘ f ∼ id) ] (Π[ x ∈ A ]  (ap f (η x) ≡ ε (f x)))

{-
ishaer : {A : 𝓤 *} {B : 𝓥 *} (f : A → B) → (𝓤 ⊔ 𝓥)*  
ishaer {A = A} {B = B} f = Σ[ G ∈ rinv f ] (rcoh {f = f} G)


ishael : {A : 𝓤 *} {B : 𝓥 *} (f : A → B) → (𝓤 ⊔ 𝓥)*  
ishael {A = A} {B = B} f = Σ[ G ∈ linv f ] (lcoh {f = f} G)
-}

ishaer : {A : 𝓤 *} {B : 𝓥 *} (f : A → B) → (𝓤 ⊔ 𝓥)*
ishaer {A = A} {B = B} f = Σ[ G ∈ (hasqinv f) ] (Π[ x ∈ A ] (ap f ((snd (snd G)) x) ≡ (fst (snd G)) (f x)) )
--  g = fst G
--  η = snd (snd G)
--  ε = fst (snd G)
ishael : {A : 𝓤 *} {B : 𝓥 *} (f : A → B) → (𝓤 ⊔ 𝓥)*
ishael {A = A} {B = B} f = Σ[ G ∈ (hasqinv f) ] (Π[ x ∈ B ] (ap (fst G) ((fst (snd G)) x) ≡ (snd (snd G)) (fst G x)) )
--  g = fst G
--  η = snd (snd G)
--  ε = fst (snd G)

ishae = ishaer

-- Lemma 4.2.2



--

-- pffh

-- p. 175
-- Definition 4.2.4
fiber :  {A : 𝓤 *} {B : 𝓥 *} (f : A → B) (y : B) → (𝓤 ⊔ 𝓥)*
fiber {A = A} {B = B} f y = Σ (λ x → f x ≡ y)
              



-- p. 189
-- Lemma 4.9.2

lem-4-9-2₀ : (A X : 𝓤 *) → isequiv (λ (f : X → A) → (id) ∘ f)
lem-4-9-2₀ A X = hasqinv→isequiv id id-is-qinv

lem-4-9-2₁ : (A B X : 𝓤 *) → (p : A ≡ B) → isequiv (λ (f : X → A) → (fst (id→eqv p)) ∘ f)
lem-4-9-2₁ A B X p = 𝕁 (type-of A)
                         (λ (A B : type-of A) (p : A ≡ B) → (X : type-of A) → isequiv (λ (f : X → A) → (fst (id→eqv p)) ∘ f))
                         (λ A X → lem-4-9-2₀ A X)
                         A B p X 

lem-4-9-2 : (A B X : 𝓤 *) (E : A ≃ B) → isequiv (λ (f : X → A) → (fst E) ∘ f)
lem-4-9-2 A B X (f , e) =  tr (λ (g : A → B) → isequiv (λ (f : X → A) → g ∘ f)) α₁ (lem-4-9-2₁ A B X p)
 where
   p : A ≡ B
   p = ua (f , e)
   α : id→eqv p ≡ (f , e)
   α = UV-α (f , e)
   α₁ : fst (id→eqv p) ≡ f
   α₁ = ap fst α
