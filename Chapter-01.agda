{-# OPTIONS --without-K --exact-split --safe #-}

module Chapter-01 where

open import Basics public



Σ-uniq : {X : 𝓤 *} {Y : X → 𝓥 *} (p : Σ Y) → p ≡ fst p , snd p
Σ-uniq {X = X} {Y = Y} p = Σ-induction {X = X}
                                       {Y = Y}
                                       {A = λ p → p ≡ fst p , snd p}
                                       (λ x y → refl (x , y))
                                       p

𝟙-uniq : (p : 𝟙) → p ≡ ⋆
𝟙-uniq p = 𝟙-induction (λ p → p ≡ ⋆)
                       (refl ⋆)
                       p

𝔹-uniq : (p : 𝔹) → (p ≡ 𝕥) + (p ≡ 𝕗)
𝔹-uniq p = 𝔹-induction (λ p → (p ≡ 𝕥) + (p ≡ 𝕗)) (inl (refl 𝕥)) (inr (refl 𝕗)) p

-- See also end of Chapter 2 (Section on universal properties)
-- where we'll see that `tt-ac` is actually part of an equivalence :D
tt-ac : (A : 𝓤 *) (B : 𝓥 *) (R : A → B → 𝓦 *)
      → (Π a ꞉ A , (Σ b  ꞉ B , R a b))
      → (Σ f ꞉ (A → B) , (Π a ꞉ A , R a (f a)))
tt-ac A B R F = ( λ a →  fst (F a) ) , (λ a →  snd (F a))

-- p. 56

→-¬¬ : {A : 𝓤 *} → A → ¬¬ A
-- Given a : A
-- Need to produce ϕ : (A → 𝟘) → 𝟘
-- ϕ defined by: given f : A → 𝟘, evaluate at a
→-¬¬ {A = A} a  = λ f → f a

-- p. 56

¬×¬-→ : {A B : 𝓤 *} → (¬ A) × (¬ B) → ¬ (A + B)

-- Pattern matching
-- ¬×¬-→ {A = A} {B = B} (ϕ , ψ) (inl a) = ϕ a 
-- ¬×¬-→ {A = A} {B = B} (ϕ , ψ) (inr b) = ψ b

-- "By hand"
¬×¬-→ {A = A} {B = B} p x = Σ-recursion {A = Σ-type} Σ-base p x
 where
  Σ-type = ¬ (A + B)
  Σ-base = λ (ϕ : ¬ A) (ψ : ¬ B) → +-recursion ϕ ψ

-- p. 58

+¬-→ : {A B : 𝓤 *} → ¬ (A + B) → (¬ A) × (¬ B)
+¬-→ {A = A} {B = B} ϕ = (λ a → ϕ (inl a)) , (λ b → ϕ (inr b))

-- p. 59

Π-distributes-over-× : {A : 𝓤 *} {P : A → 𝓥 *} {Q : A → 𝓦 *}
                    → (Π x ꞉ A , (P x) × (Q x) )
                    → (Π x ꞉ A , P x) × (Π x ꞉ A , Q x)
Π-distributes-over-× {A = A} {P = P} {Q = Q} α = (λ x → fst (α x)) ,
                                                 (λ x → snd (α x))

