{-# OPTIONS --without-K --exact-split --safe #-}

-- That based and unbased path can be obtained from each other 

module Based-Path-Induction where



open import Basics public

-- Unbased path induction:
𝕁₁ 𝕁₂ : {X : 𝓤 *}
      → {C : (x y : X) (p : x ≡ y) → 𝓥 *}
      → (c : (x : X) → C x x (refl x))
      → (x y : X) → (p : x ≡ y) → C x y p
𝕁₁ {X = X} {C = C} c x x (refl x) = c x

-- Based path induction
ℍ₁ ℍ₂ : {X : 𝓤 *}
       → (x : X)
       → {C : (y : X) (p : x ≡ y) → 𝓥 *}
       → (c : C x (refl x))
       → (y : X) → (p : x ≡ y) → C y p
ℍ₁ {X = X} x {C = C} c x (refl x) = c

-- 𝕁 from ℍ :
𝕁₂ {X = X} {C = C} c x y p = ℍ₁ {X = X} x {C = λ y p → C x y p} (c x) y p

-- On x = y and p = refl x, we get by the defining equation of ℍ₁ that 𝕁₂ evaluates to c x


-- ℍ from 𝕁 :
-- We define the type of “equalities with x”

Sing : {X : 𝓤 *} (x : X) → 𝓤 *
Sing {X = X} x = Σ (x ≡_)

-- This type is contractible of center (x , refl x)

SingCtr : {X : 𝓤 *} (x : X) (e : Sing x) → (x , refl x) ≡ e
-- SingCtr x (x , refl x) = refl (x , refl x)
SingCtr x (y , p) = 𝕁₁ {C = λ x y p → (x , refl x) ≡ (y , p)} (λ x → refl (x , refl x)) x y p

-- Define path lifting just to have it here
ap₂ : {X : 𝓤 *} {A : X → 𝓥 *} {x y : X} (p : x ≡ y) → A x → A y
-- ap₂ (refl x) = id
ap₂ {X = X} {A} {x} {y} p u = 𝕁₁ {C = λ x y p → A x → A y} (λ x → 𝑖𝑑 (A x)) x y p u

-- Any (y , p) in SingCtr is equal to (x , refl x)
-- Fix the input of ℍ:
--  {X : 𝓤 *}
--  (x : X)
--  {C : (y : X) (p : x ≡ y) → 𝓥 *}
--  (c : C x (refl x))
--  (y : X)
--  (p : x ≡ y)
-- To construct an element of C y p, it suffices thus to construct an element of C x (refl x),
-- and use ap₂ to lift the equality (x , refl x) ≡ (y , p) to a map C x (refl x) → C y p.
ℍ₂ {X = X} x {C = C} c y p = ap₂ {X = Sing x}
                                  {A = λ e → C (fst e) (snd e)}
                                  {x = (x , refl x)}
                                  {y = (y , p)}
                                  (SingCtr x (y , p))
                                  c



 
