{-# OPTIONS --without-K --exact-split --show-implicit #-}



module Chapter-02 where


open import Chapter-01 public


transport : {X : 𝓤 *} (Y : X → 𝓥 *) {x₀ x₁ : X} → x₀ ≡ x₁ → Y x₀ → Y x₁
transport Y p = 𝕁' (λ x₀ x₁ p → Y x₀ → Y x₁) (λ x₀ → 𝑖𝑑 (Y x₀)) p
tr = transport

ap : {X : 𝓤 *} {Y : 𝓥 *} (f : X → Y) {x₀ x₁ : X} → x₀ ≡ x₁ → (f x₀) ≡ (f x₁)
ap {X = X} {Y = Y} f {x₀} {x₁} p = transport (λ x₁ → (f x₀) ≡ (f x₁)) p (refl (f x₀))


apd : {A : 𝓤 *} {P : A → 𝓥 *} (f : (x : A) → P x) {x₀ x₁ : A} (p : x₀ ≡ x₁) → transport P p (f x₀) ≡ f x₁
-- apd {A = A} {P} f {x} {x} (refl x) = refl (f x)
apd {A = A} {P} f {x₀} {x₁} p = 𝕁† type base p
 where
  type = λ {x₀ x₁ : A} (p : x₀ ≡ x₁) → transport P p (f x₀) ≡ f x₁
  base : (x : A) → transport P (refl x) (f x) ≡ f x
  base x = refl (f x)

-- This will get defined later on, but we need it for `apd₂` now,
-- hence the early definition.
Σ≡→≡' : {A : 𝓤 *} {P : A → 𝓥 *} {p₀ p₁ : Σ P}
     → (e : fst p₀ ≡ fst p₁)
     → tr P e (snd p₀) ≡ snd p₁
     → p₀ ≡ p₁
Σ≡→≡' {A = A} {P = P} {p₀} {p₁} e f = (Σ-uniq p₀) ∙ (𝕁 A type base x₀ x₁ e u₀ u₁ f) ∙ (Σ-uniq p₁) ⁻¹ 
 where
  x₀ = fst p₀
  u₀ = snd p₀
  x₁ = fst p₁
  u₁ = snd p₁

  type = λ (x₀ x₁ : A) (e : x₀ ≡ x₁) → (u₀ : P x₀) (u₁ : P x₁) (f : (transport P e u₀) ≡ u₁) → (x₀ , u₀) ≡ (x₁ , u₁)
  base : (x : A) → (u₀ u₁ : P x) (f : u₀ ≡ u₁) → (x , u₀) ≡ (x , u₁)
  base x u₀ u₁ f = 𝕁 (P x) type' base' u₀ u₁ f
   where
    type' = λ (u₀ u₁ : P x) (f : u₀ ≡ u₁) →  (x , u₀) ≡ (x , u₁)
    base' : (u : P x) → (x , u) ≡ (x , u)
    base' u = refl (x , u)


-- Thanks {ames} on ##dependent
-- What we do is essentially go from a function of two arguments (the second depending on the first)
-- to a function taking Σ types, and thus need to use our knowledge of equivalence for paths in Σ-types.
apd₂ : {X : 𝓤 *} {Y : X → 𝓥 *} {Z : (x : X) → Y x → 𝓦 *}
     → (f : (x : X) → (y : Y x) → Z x y)
     → {x₀ x₁ : X} → (p : x₀ ≡ x₁)
     → {y₀ : Y x₀} {y₁ : Y x₁} → (q : tr Y p y₀ ≡ y₁)
     → (tr {X = Σ Y} (λ (x : Σ Y) → Z (fst x) (snd x)) {(x₀ , y₀)} {(x₁ , y₁)} (Σ≡→≡' p q) (f x₀ y₀)) ≡ (f x₁ y₁)  
apd₂ f (refl x) (refl y) = refl (f x y)

-- ∙ makes identity into a groupoid

∙-refl-left : {X : 𝓤 *} {x y : X} (p : x ≡ y) → (refl x) ∙ p ≡ p
∙-refl-left p = refl p

∙-refl-right : {X : 𝓤 *} {x y : X} (p : x ≡ y) → p ∙ (refl y) ≡ p
∙-refl-right p = 𝕁' (λ x y p → p ∙ (refl y) ≡ p) (λ x → refl (refl x)) p
-- Comment:
-- When x = y and p = refl x, we have: p ∙ refl y = refl x ∙ refl x = refl x, all of it judgementally

∙-⁻¹-left : {X : 𝓤 *} {x y : X} (p : x ≡ y) → p ⁻¹ ∙ p ≡ (refl y)
∙-⁻¹-left p = 𝕁' (λ x y p → p ⁻¹ ∙ p ≡ (refl y)) (λ x → refl (refl x)) p

∙-⁻¹-right : {X : 𝓤 *} {x y : X} (p : x ≡ y) → p ∙ p ⁻¹ ≡ (refl x)
∙-⁻¹-right p = 𝕁† (λ p → p ∙ p ⁻¹ ≡ refl (lhs p)) (λ x → refl (refl x)) p

∙-⁻¹-involution : {X : 𝓤 *} {x y : X} (p : x ≡ y) → (p ⁻¹) ⁻¹ ≡ p
∙-⁻¹-involution p = 𝕁† (λ p → (p ⁻¹) ⁻¹ ≡ p) (λ x → refl (refl x)) p

∙-assoc : {X : 𝓤 *} {x y z w : X} (p : x ≡ y) (q : y ≡ z) (r : z ≡ w) → (p ∙ q) ∙ r ≡ p ∙ (q ∙ r)
∙-assoc p q r = 𝕁'
                  (λ x y p → (z w : type-of x) (q : y ≡ z) (r : z ≡ w) → (p ∙ q) ∙ r ≡ p ∙ (q ∙ r))
                  (λ x → λ z w q r → refl (q ∙ r))
                  p (rhs q) (rhs r) q r
                  

-- Alternative proof just to do it properly with 𝕁
∙-assoc' : {X : 𝓤 *} (x y z w : X) (p : x ≡ y) (q : y ≡ z) (r : z ≡ w) → (p ∙ q) ∙ r ≡ p ∙ (q ∙ r)
∙-assoc' {X = X} x y z w p q r = 𝕁' type-1 base-1 p z w q r
 where
  type-1 = λ x y p → (z w : X) (q : y ≡ z) (r : z ≡ w) → (p ∙ q) ∙ r ≡ p ∙ (q ∙ r)
  base-1 : (x : X) → (z w : X) (q : x ≡ z) (r : z ≡ w) → ((refl x) ∙ q) ∙ r ≡ (refl x) ∙ (q ∙ r)
  base-1 x z w q r = 𝕁' type-2 base-2 q w r
   where
    type-2 = λ x z q → (w : X) (r : z ≡ w) → ((refl x) ∙ q) ∙ r ≡ (refl x) ∙ (q ∙ r)
    base-2 : (x : X) → (w : X) (r : x ≡ w) → ((refl x) ∙ (refl x)) ∙ r ≡ (refl x) ∙ ((refl x) ∙ r)
    base-2 x w r = 𝕁' type-3 base-3 r
     where
      type-3 = λ x w r → ((refl x) ∙ (refl x)) ∙ r ≡ (refl x) ∙ ((refl x) ∙ r)
      base-3 : (x : X) → ((refl x) ∙ (refl x)) ∙ (refl x) ≡ (refl x) ∙ ((refl x) ∙ (refl x))
      base-3 x = refl (refl x)


  
-- p. 89
-- Etc

Ptd : {𝓤 : Universe} → 𝓤 ⁺ *
Ptd {𝓤} =  Σ (λ (X : 𝓤 *) → X)

_● : (𝓤 : Universe) → 𝓤 ⁺ *
𝓤 ● = Ptd {𝓤}

Ptd-type : Ptd → 𝓤 *
Ptd-type (X , x) = X

#_# : Ptd → 𝓤 *
# X # = Ptd-type X

Ptd-point : {𝓤 : Universe} (X : Ptd {𝓤}) → (fst X)
Ptd-point (X , x) = x

Ω : {𝓤 : Universe} → 𝓤 ● → 𝓤 ●
Ω (X , x) = (x ≡ x) , refl x  

Ω² : {𝓤 : Universe} → 𝓤 ● → 𝓤 ●
Ω² A = Ω (Ω A)

{-
Eckmann-Hilton : {A : Ptd} (α β : # Ω² A #) → (α ∙ β) ≡ (β ∙ α)
Eckmann-Hilton = {!!}
-}


-- p. 93

-- ap behaves well

ap-∘ : {X : 𝓤 *} {Y : 𝓥 *} {Z : 𝓦 *} → (f : X → Y) → (g : Y → Z) {x₀ x₁ : X} → (p : x₀ ≡ x₁) → ap (g ∘ f) p ≡ ap g (ap f p)
ap-∘  f g  p = 𝕁† (λ p → ap (g ∘ f) p ≡ ap g (ap f p)) (λ x → refl (refl (g (f x)))) p
-- When x = x₀ = x₁ and p = refl x, then ap f p = refl f x, etc

ap-id : {X : 𝓤 *} {x₀ x₁ : X} → (p : x₀ ≡ x₁) → ap (𝑖𝑑 X) p ≡ p
ap-id {X = X} p = 𝕁† (λ p → ap (𝑖𝑑 X) p ≡ p) (λ x → refl (refl x)) p  

--
-- ap f (p ∙ q) = (ap f p) ∙ (ap f q) 
-- ap f (refl x) = refl (f x)

ap-refl : {X : 𝓤 *} {Y : 𝓥 *} (f : X → Y) (x : X) → ap f (refl x) ≡ refl (f x)
ap-refl f x = refl (refl (f x))
-- because of the definitional equation of ap


ap-∙ : {X : 𝓤 *} {Y : 𝓥 *} (f : X → Y) {x₀ x₁ x₂ : X} → (p : x₀ ≡ x₁) → (q : x₁ ≡ x₂) → ap f (p ∙ q) ≡ (ap f p) ∙ (ap f q)
ap-∙ {X = X} f p q = 𝕁† type-1 base-1 p q
 where
  type-1 = λ {x₀ x₁ : X} (p : x₀ ≡ x₁) → {x₂ : X} →  (q : rhs p ≡ x₂) → ap f (p ∙ q) ≡ (ap f p) ∙ (ap f q)
  base-1 : (x : X) → {x₂ : X} (q : x ≡ x₂) → ap f ((refl x) ∙ q) ≡ (ap f (refl x)) ∙ (ap f q)
  base-1 x q =  ap f ((refl x) ∙ q)      ≡⟨ refl (ap f q) ⟩
                ap f q                   ≡⟨ refl (ap f q) ⟩
                refl (f x) ∙ (ap f q)    ≡⟨ refl (ap f q) ⟩
                ap f (refl x) ∙ (ap f q) ∎
  -- we're lucky here because pretty much everything holds judgementally :D


ap-⁻¹ : {X : 𝓤 *} {Y : 𝓥 *} (f : X → Y) {x₀ x₁ : X} → (p : x₀ ≡ x₁) → ap f (p ⁻¹) ≡ (ap f p) ⁻¹
ap-⁻¹ {X = X} f p = 𝕁† type base p
 where
  type = λ {x₀ x₁ : X} (p : x₀ ≡ x₁) → ap f (p ⁻¹) ≡ (ap f p) ⁻¹
  base : (x : X) → ap f ((refl x) ⁻¹) ≡ (ap f (refl x)) ⁻¹
  base x = refl (refl (f x))

-- p. 95
-- Lemma 2.3.2

lift : {A : 𝓤 *} {P : A → 𝓥 *} {x₀ x₁ : A} (p : x₀ ≡ x₁) → (u₀ : P x₀) →  (x₀ , u₀) ≡ (x₁ , transport P p u₀)
lift {A = A} {P} {x₀} {x₁} p u₀ = 𝕁† type base p u₀
 where
  type = λ {x₀ x₁ : A} (p : x₀ ≡ x₁) → (u₀ : P x₀) → (x₀ , u₀) ≡ (x₁ , transport P p u₀)
  base : (x : A) → (u : P x) → (x , u) ≡ (x , transport P (refl x) u)
  base x u = refl (x , u) -- since transport P (refl x) u is judgementally equal to u

lift-nicely :  {A : 𝓤 *} {P : A → 𝓥 *} {x₀ x₁ : A} (p : x₀ ≡ x₁) → (u₀ : P x₀) → ap (fst {X = A} {Y = P}) (lift p u₀) ≡ p
lift-nicely {A = A} {P} p u₀ = 𝕁† type base p u₀
 where
  type = λ {x₀ x₁ : A} (p : x₀ ≡ x₁) → (u₀ : P x₀) →  ap (fst {X = A} {Y = P}) (lift p u₀) ≡ p
  base : (x : A) → (u : P x) →  ap fst (lift (refl x) u₀) ≡ (refl x)
  base x u = refl (refl x)

-- p. 96

-- get an independent function out of a dependent one
indep : (X : 𝓤 *) (P : X → 𝓥 *) → ((x : X) → P x) → X → Σ (λ (x : X) → P x)
indep X P f x = x , f x

fst-∘-indep : {X : 𝓤 *} {P : X → 𝓥 *} → (f : (x : X) → P x) → fst ∘ (indep X P f) ≡ 𝑖𝑑 X
snd-∘-indep : {X : 𝓤 *} {P : X → 𝓥 *} → (f : (x : X) → P x) → snd ∘ (indep X P f) ≡ f
fst-∘-indep f = refl id
snd-∘-indep f = refl f

-- this needs an explanation !
-- As usual, `=` denotes judgemental equality:
-- fst ∘ (indep f) =  λ x → fst ((indep f) x)
-- But fst ((indep f ) x) = fst (x , f x) = x
-- Thus fst ∘ (indep f) = λ x → x = id.
-- In other terms: before doing the lambda abstraction defining the composite, we have a judgemental equality fst ((indep f) x) = x,
-- and abstracting preserves judgemental equality.
-- Same argument for snd-∘-indep.


path-over : {X : 𝓤 *} {P : X → 𝓥 *}
            (f : (x : X) → P x)
            {x₀ x₁ : X}
            (p : x₀ ≡ x₁)
          → ap fst (ap (indep X P f) p) ≡ p
path-over f {x} {x} (refl x) = refl (refl x)
-- using 𝕛 : all we need to consider is x₀ = x = x₁ and p = refl x, in which case ap maps to other refls
{- path-over {X = X} {P} f {x₀} {x₁} p = ap fst (ap (indep X P f) p) ≡⟨ ap-∘ {! (indep X P f)!} {! fst!} {! p!} ⟩
                            -- ap (fst ∘ (indep X P f)) p   ≡⟨ ap (λ ϕ → ap ϕ p) (fst-∘-indep f) ⟩
                            ap (fst ∘ (indep X P f)) p   ≡⟨ refl (ap id p) ⟩
                            ap (𝑖𝑑 X) p              ≡⟨ ap-id p ⟩
                            p                       ∎
-}



-- p. 97
-- lemma 2.3.5

transportconst : {A : 𝓤 *} {B : 𝓥 *} {x₀ x₁ : A} (p : x₀ ≡ x₁) (b : B) → transport (λ x → B) p b ≡ b
transportconst {x₀ = x} {x₁ = x} (refl x) b = refl b

-- p. 98
-- lemma 2.3.9

transport-∙ : {A : 𝓤 *} {P : A → 𝓥 *} {x₀ x₁ x₂ : A} (p : x₀ ≡ x₁) (q : x₁ ≡ x₂) (u : P x₀) → (transport P q) ( (transport P p) u) ≡ transport P (p ∙ q) u
transport-∙ {P = P} {x₀ = x} {x₁ = x} {x₂ = x₂} (refl x) q u = refl (transport P q u)
-- using 𝕁 : in that case `transport P p u` is just judgementally `u`.


-- p. 99
-- lemma 2.3.10
transport-type-comp : {A : 𝓤 *} {B : 𝓥 *} {f : A → B} {P : B → 𝓦 *} {x₀ x₁ : A} (p : x₀ ≡ x₁) (u : P (f x₀)) → transport (P ∘ f) p u ≡ transport P (ap f p) u
transport-type-comp {x₀ = x} {x₁ = x} (refl x) u =  refl u
-- using 𝕁 again: in that case `transport (P ∘ f) (refl x) u ` is `u`, etc

-- p. 99
-- lemma 2.3.11

-- TODO


-- p. 99
-- Section 2.4 Homotopies and equivalences

-- homotopy of functions
_∼_ : {A : 𝓤 *} {P : A → 𝓥 *} (f g : (x : A) → P x) → (𝓤 ⊔ 𝓥) *
_∼_ {A = A} {P = P} f g = (x : A) →  f x ≡ g x

_Π≡_ : {A : 𝓤 *} {P : A → 𝓥 *} (f g : (x : A) → P x) → (𝓤 ⊔ 𝓥) *
_Π≡_ = _∼_

infix   0 _∼_
infix   0 _Π≡_

-- homotopy of functions is an equivalence relation

∼-refl : {A : 𝓤 *} {P : A → 𝓥 *} (f : (x : A) → P x) → f ∼ f
∼-symm : {A : 𝓤 *} {P : A → 𝓥 *} (f g : (x : A) → P x) → f ∼ g → g ∼ f
∼-tran : {A : 𝓤 *} {P : A → 𝓥 *} (f g h : (x : A) → P x) → f ∼ g → g ∼ h → f ∼ h

∼-refl f x = refl (f x)
∼-symm f g α x = (α x) ⁻¹
∼-tran f g h α β x = (α x) ∙ (β x)

Π≡-refl : {A : 𝓤 *} {P : A → 𝓥 *} (f : (x : A) → P x) → f ∼ f
Π≡-symm : {A : 𝓤 *} {P : A → 𝓥 *} {f g : (x : A) → P x} → f ∼ g → g ∼ f
Π≡-tran : {A : 𝓤 *} {P : A → 𝓥 *} {f g h : (x : A) → P x} → f ∼ g → g ∼ h → f ∼ h

Π≡-refl = ∼-refl
Π≡-symm {f = f} {g = g} = ∼-symm f g
Π≡-tran {f = f} {g = g} {h = h} = ∼-tran f g h


-- p. 100
-- lemma 2.4.3

∼-is-natural : {A : 𝓤 *} {P : 𝓥 *} (f g : (x : A) → P) (H : f ∼ g) {x₀ x₁ : A} (p : x₀ ≡ x₁) → (ap f p) ∙ (H x₁) ≡ (H x₀) ∙ (ap g p)
∼-is-natural f g H {x₀ = x} {x₁ = x} (refl x) = (ap f (refl x)) ∙ (H x)   ≡⟨ refl (H x) ⟩
                                                 (refl (f x)) ∙ (H x)     ≡⟨  refl (H x) ⟩
                                                 H x                      ≡⟨ (∙-refl-right (H x)) ⁻¹ ⟩
                                                 (H x) ∙ (refl (g x))     ≡⟨ refl ((H x) ∙ (refl (g x))) ⟩
                                                 (H x) ∙ (ap g (refl x))  ∎

postulate ∼-is-natural' : {A : 𝓤 *} {P : 𝓥 *} (f g : (x : A) → P) (H : f ∼ g) {x₀ x₁ : A} (p : x₀ ≡ x₁) →  (H x₀)⁻¹ ∙ (ap f p) ∙ (H x₁) ≡ (ap g p)
{-
∼-is-natural' f g H (refl x) = (H x)⁻¹ ∙ (ap f (refl x)) ∙ (H x) ≡⟨ refl ( (H x)⁻¹ ∙ (ap f (refl x)) ∙ (H x)) ⟩
                               (H x)⁻¹  ∙  refl (f x) ∙ (H x) ≡⟨ {!!} ⟩
                               (H x)⁻¹  ∙ (H x) ≡⟨ {!!} ⟩
                               refl (g x) ≡⟨ {!!}  ⟩
                               ap g (refl x) ∎
-}

-- p. 101
-- Def 2.4.6
-- They call it "qinv" but it makes more sense to call it "hasqinv" imo
-- TODO: revert this naming, because I'm told it's bad :(

hasqinv : {A : 𝓤 *} {B : 𝓥 *} → (A → B) → (𝓤 ⊔ 𝓥) *
hasqinv {A = A} {B = B} f = Σ (λ (g : B → A) → (f ∘ g ∼ id) × (g ∘ f ∼ id))
--                                                 α              β

hasqinv-inverse : {A : 𝓤 *} {B : 𝓥 *} → {f : A → B} → (i : hasqinv f) → (B → A)
hasqinv-inverse (g , α , β) = g

hasqinv-section : {A : 𝓤 *} {B : 𝓥 *} → {f : A → B} → (i : hasqinv f) → ((hasqinv-inverse i) ∘ f ∼ id)
hasqinv-section (g , α , β) = β

hasqinv-retraction : {A : 𝓤 *} {B : 𝓥 *} → {f : A → B} → (i : hasqinv f) → (f ∘ (hasqinv-inverse i) ∼ id)
hasqinv-retraction (g , α , β) = α

inv-of-qinv-is-qinv : {A : 𝓤 *} {B : 𝓥 *} → (f : A → B) → (i : hasqinv f) → hasqinv (fst i)
inv-of-qinv-is-qinv f (g , α , β) = (f , β , α) 

qinv-∘ : {A : 𝓤 *} {B : 𝓥 *} {C : 𝓦 *}
       → (f : A → B) → hasqinv f
       → (g : B → C) → hasqinv g
       → hasqinv (g ∘ f)
qinv-∘ {A = A} {B} {C} f i g j = h' , ϕ , ψ 
 where
  f' = hasqinv-inverse i
  β = hasqinv-section i
  α = hasqinv-retraction i
  g' = hasqinv-inverse j
  δ = hasqinv-section j
  γ = hasqinv-retraction j
  
  h = g ∘ f
  h' = f' ∘ g'
  ϕ : h ∘ h' ∼ (𝑖𝑑 C) 
  ϕ c = (h ∘ h') c ≡⟨  refl ((h ∘ h') c) ⟩
        g (f (f' (g' c))) ≡⟨ ap g (α (g' c)) ⟩
        g (g' c) ≡⟨ (γ c) ⟩
        c         ∎
  ψ : h' ∘ h ∼ (𝑖𝑑 A)
  ψ a = (h' ∘ h ) a ≡⟨ refl ((h' ∘ h) a) ⟩
        f' (g' (g (f a))) ≡⟨ ap f' (δ (f a)) ⟩
        f' (f a) ≡⟨ (β a) ⟩
        a         ∎

-- p. 102
-- Example 2.4.7

id-is-qinv : {A : 𝓤 *} → hasqinv (𝑖𝑑 A)
id-is-qinv {A = A} = (𝑖𝑑 A) , refl , refl

-- p. 102
-- Example 2.4.8

∙-left-is-qinv : {A : 𝓤 *} {x y z : A} (p : x ≡ y) → hasqinv {A = y ≡ z} {B = x ≡ z} (p ∙_)
∙-left-is-qinv {x = x} {y = y} {z = z} p = (p ⁻¹ ∙_) , α , β
 where
  α : (q : x ≡ z) → ( (p ∙_) ∘ ((p ⁻¹) ∙_) ) q ≡ q
  α q = ( (p ∙_) ∘ (p ⁻¹ ∙_) ) q ≡⟨ refl ( p  ∙ (p ⁻¹  ∙ q)) ⟩
        p ∙  (p ⁻¹ ∙ q)            ≡⟨ (∙-assoc p (p ⁻¹) q) ⁻¹ ⟩
        (p ∙ p ⁻¹) ∙ q            ≡⟨  ap ( _∙ q ) (∙-⁻¹-right p) ⟩
        (refl x) ∙ q               ≡⟨ refl q ⟩
        q                          ∎
  β : (q : y ≡ z) → ( (p ⁻¹ ∙_) ∘ (p ∙_) ) q ≡ q
  β q = ( (p ⁻¹ ∙_) ∘ (p ∙_) ) q ≡⟨ refl ( p ⁻¹ ∙ (p ∙ q)) ⟩
        p ⁻¹ ∙  (p ∙ q)            ≡⟨ (∙-assoc (p ⁻¹) p q) ⁻¹ ⟩
        (p ⁻¹ ∙ p) ∙ q            ≡⟨  ap ( _∙ q ) (∙-⁻¹-left p) ⟩
        (refl y) ∙ q               ≡⟨ refl q ⟩
        q                          ∎

-- same idea but can use fewer `refl`s since for instance `q ∙ (refl y) ≡ q` but not judgementally
postulate ∙-right-is-qinv : {A : 𝓤 *} {x y z : A} (p : x ≡ y) → hasqinv {A = z ≡ x} {B = z ≡ y} ( _∙ p)
{- ∙-right-is-qinv = {!!}
-}




-- p. 102
-- Example 2.4.9

transport-is-qinv : {A : 𝓤 *} {P : A → 𝓥 *} {x y : A} (p : x ≡ y) → hasqinv {A = P x} {B = P y} (transport P p)
transport-is-qinv {A = A} {P = P} {x = x} {y = x} (refl x) = id-is-qinv {A = P x}





-- p. 102
-- Eq 2.4.10

isequiv : {A : 𝓤 *} {B : 𝓥 *} → (A → B) → (𝓤 ⊔ 𝓥) *
isequiv {A = A} {B = B} f = Σ (λ (g : B → A) → (f ∘ g ∼ id)) × Σ (λ (h : B → A) → (h ∘ f ∼ id))
--                                                    α                                  β

hasqinv→isequiv :  {A : 𝓤 *} {B : 𝓥 *} (f : A → B) → hasqinv f → isequiv f
hasqinv→isequiv f (g , α , β) = (g , α) , (g , β)

isequiv→hasqinv :   {A : 𝓤 *} {B : 𝓥 *} (f : A → B) → isequiv f → hasqinv f
isequiv→hasqinv f ((g , α) , (h , β)) = g , α , β'
 where
  β' :  g ∘ f ∼ id
  β' x = (g ∘ f) x       ≡⟨ refl (g (f x)) ⟩
         g (f x)         ≡⟨ (β (g (f x)))⁻¹ ⟩
         h (f (g (f x))) ≡⟨ ap h (α (f x)) ⟩
         h (f x)         ≡⟨ β x ⟩
         x               ∎


-- p. 103
-- Eq. 2.4.11

_≃_ : (A : 𝓤 *) (B : 𝓥 *) → (𝓤 ⊔ 𝓥)*
-- A ≃ B = Σ (λ (f : A → B) → isequiv f)
A ≃ B = Σ (isequiv {A = A} {B = B}) -- fancy

-- Just for potential later use
≃→fwd : {A : 𝓤 *} → {B : 𝓥 *} → (A ≃ B) → A → B
≃→fwd e = fst e

≃→bwd : {A : 𝓤 *} → {B : 𝓥 *} → (A ≃ B) → B → A
≃→bwd e = fst (fst (snd e))



≃-refl : {A : 𝓤 *} → A ≃ A
≃-symm : {A : 𝓤 *} {B : 𝓥 *} → A ≃ B → B ≃ A
≃-tran : {A : 𝓤 *} {B : 𝓥 *} {C : 𝓦 *} → A ≃ B → B ≃ C → A ≃ C

≃-refl = id , hasqinv→isequiv id id-is-qinv -- maybe relying too much on implicits, but it's pretty
≃-symm {A = A} {B = B} (f , ((g , α) , (h , β))) = g , hasqinv→isequiv g j 
 where
  i = isequiv→hasqinv f ((g , α ) , (h , β))
  j = inv-of-qinv-is-qinv f i
  
≃-tran (f , d) (g , e) = h , hasqinv→isequiv h k
 where
  h = g ∘ f
  i = isequiv→hasqinv f d
  j = isequiv→hasqinv g e
  k = qinv-∘ f i g j

-- follows from the `is-qinv` versions
postulate ∙-left-isequiv : {A : 𝓤 *} {x y z : A} (p : x ≡ y) → isequiv {A = y ≡ z} {B = x ≡ z} (p ∙_)
postulate ∙-right-isequiv : {A : 𝓤 *} {x y z : A} (p : x ≡ y) → isequiv {A = z ≡ x} {B = z ≡ y} ( _∙ p)
postulate ∙-left→equiv : {A : 𝓤 *} {x y z : A} (p : x ≡ y) → (y ≡ z) ≃ (x ≡ z)
postulate ∙-right→equiv : {A : 𝓤 *} {x y z : A} (p : x ≡ y) → (z ≡ x) ≃ (z ≡ y)

-- p. 105
-- Section 2.5 Cartesian product type

-- `p₀ Σ≡ p₁` means that the first components are equal, and transporting this equality makes the second components equal.
-- The goal is to show that `p₀ Σ≡ p₁` are equivalent types

_Σ≡_ : {A : 𝓤 *} {P : A → 𝓥 *} (p₀ p₁ : Σ P) → (𝓤 ⊔ 𝓥) *
_Σ≡_ {A = A} {P} p₀ p₁ = Σ λ (e : fst p₀ ≡ fst p₁) →  ((transport P e (snd p₀)) ≡ snd p₁)

≡→Σ≡ : {A : 𝓤 *} {P : A → 𝓥 *} (p₀ p₁ : Σ P)
    → p₀ ≡ p₁
    → p₀ Σ≡ p₁
-- not using 𝕁
≡→Σ≡ {A = A} {P = P} p p (refl p) = refl (fst p) , refl (snd p)

Σ≡→≡ : {A : 𝓤 *} {P : A → 𝓥 *} (p₀ p₁ : Σ P)
     → p₀ Σ≡ p₁
     → p₀ ≡ p₁
-- Let's use 𝕁 for exercise
Σ≡→≡ {A = A} {P = P} p₀ p₁ (e , f) = (Σ-uniq p₀) ∙ (𝕁 A type base x₀ x₁ e u₀ u₁ f) ∙ (Σ-uniq p₁) ⁻¹
 where
  x₀ = fst p₀
  u₀ = snd p₀
  x₁ = fst p₁
  u₁ = snd p₁

  type = λ (x₀ x₁ : A) (e : x₀ ≡ x₁) → (u₀ : P x₀) (u₁ : P x₁) (f : (transport P e u₀) ≡ u₁) → (x₀ , u₀) ≡ (x₁ , u₁)
  base : (x : A) → (u₀ u₁ : P x) (f : u₀ ≡ u₁) → (x , u₀) ≡ (x , u₁)
  base x u₀ u₁ f = 𝕁 (P x) type' base' u₀ u₁ f
   where
    type' = λ (u₀ u₁ : P x) (f : u₀ ≡ u₁) →  (x , u₀) ≡ (x , u₁)
    base' : (u : P x) → (x , u) ≡ (x , u)
    base' u = refl (x , u)

-- p. 110
-- Theorem 2.7.2 (not following their argument)


Σ≡-α :  {A : 𝓤 *} {P : A → 𝓥 *} (p₀ p₁ : Σ P) →  (≡→Σ≡ p₀ p₁) ∘ (Σ≡→≡ p₀ p₁) ∼ id
Σ≡-α {A = A} {P} (x₀ , u₀) (x₁ , u₁) (e , s) = 𝕁 A type base x₀ x₁ e u₀ u₁ s
 where
  type = λ (x₀ x₁ : A) (e : x₀ ≡ x₁)
        → (u₀ : P x₀) → (u₁ : P x₁) → (s : transport P e u₀ ≡ u₁)
        → (((≡→Σ≡ (x₀ , u₀) (x₁ , u₁)) ∘ (Σ≡→≡ (x₀ , u₀) (x₁ , u₁))) (e , s) ) ≡ (e , s)
  base : (x : A)
        → (u₀ : P x) → (u₁ : P x) (s : u₀ ≡ u₁)
        →  ((≡→Σ≡ (x , u₀) (x , u₁)) ∘ (Σ≡→≡ (x , u₀) (x , u₁))) (refl x , s) ≡ (refl x , s)
  base x u₀ u₁ s = 𝕁 (P x) type' base' u₀ u₁ s
   where
    type' = λ (u₀ u₁ : P x) (s : u₀ ≡ u₁)
        → (≡→Σ≡ (x , u₀) (x , u₁) ∘ (Σ≡→≡ (x , u₀) (x , u₁))) (refl x , s) ≡ (refl x , s)
    base' : (u : P x)
        → (≡→Σ≡ (x , u) (x , u) ∘ (Σ≡→≡ (x , u) (x , u))) (refl x , refl u) ≡ (refl x , refl u)
    base' u = refl (refl x , refl u)
-- α (x , u) (x , u) (refl x , refl u) =  refl (refl x , refl u)

Σ≡-β :  {A : 𝓤 *} {P : A → 𝓥 *} (p₀ p₁ : Σ P) →  (Σ≡→≡ p₀ p₁) ∘ (≡→Σ≡ p₀ p₁) ∼ id
Σ≡-β {A = A} {P} p₀ p₁ e = 𝕁 (Σ P) type base p₀ p₁ e
 where
  type = λ (p₀ p₁ : Σ P) (e : p₀ ≡ p₁) → ((Σ≡→≡ p₀ p₁) ∘ (≡→Σ≡ p₀ p₁)) e ≡ e
  base : (p : Σ P) → ((Σ≡→≡ p p) ∘ (≡→Σ≡ p p)) (refl p) ≡ refl p
  base (x , y) =  refl (refl (x , y))  

≡-≃-Σ≡ : {A : 𝓤 *} {P : A → 𝓥 *} (p₀ p₁ : Σ P)
        → (p₀ ≡ p₁) ≃  (p₀ Σ≡ p₁)
≡-≃-Σ≡ p₀ p₁ = ≡→Σ≡ p₀ p₁ , hasqinv→isequiv (≡→Σ≡ p₀ p₁) (Σ≡→≡ p₀ p₁ , Σ≡-α p₀ p₁ , Σ≡-β p₀ p₁)


-- p. 111
-- Corollary 2.7.3
Σ-uniq' : {A : 𝓤 *} {P : A → 𝓥 *} (p : Σ P) → p ≡ (fst p , snd p)
Σ-uniq' p = Σ≡→≡ p (fst p , snd p) (refl (fst p) , refl (snd p))




-- See Lemma 2.3.9
ap-tr-⁻¹ : {A : 𝓤 *} {P : A → 𝓥 *} {x₀ x₁ : A} (p : x₀ ≡ x₁)
                → (u : P x₀)
                → (tr P (p ⁻¹)) (tr P p u) ≡ u
-- ap-tr-⁻¹ (refl x) u = refl u  
ap-tr-⁻¹ {P = P} p u = (tr P (p ⁻¹)) (tr P p u)  ≡⟨ transport-∙ p (p ⁻¹) u ⟩
              tr P (p ∙ p ⁻¹) u                  ≡⟨ ap (λ - → tr P - u) (∙-⁻¹-right p) ⟩
              tr P (refl (lhs p)) u             ≡⟨ refl u ⟩
              u                        ∎

ap-tr-⁻¹' : {A : 𝓤 *} {P : A → 𝓥 *} {x₀ x₁ : A} (p : x₀ ≡ x₁)
                → (u : P x₁)
                → (tr P p) (tr P (p ⁻¹) u) ≡ u
ap-tr-⁻¹' (refl x) u = refl u 

Σ≡-refl : {A : 𝓤 *} {P : A → 𝓥 *} → (p : Σ P) → p Σ≡ p
Σ≡-refl p = (refl (fst p) , refl (snd p))

Σ≡-symm : {A : 𝓤 *} {P : A → 𝓥 *} → (p₀ p₁ : Σ P) → p₀ Σ≡ p₁ → p₁ Σ≡ p₀
Σ≡-symm (x , u) (x , u) (refl x , refl u) = refl x , refl u 

Σ≡-symm' : {A : 𝓤 *} {P : A → 𝓥 *} → (p₀ p₁ : Σ P) → p₀ Σ≡ p₁ → p₁ Σ≡ p₀
Σ≡-symm' {P = P} (x₀ , u₀) (x₁ , u₁) (f , t) = f' , t'
 where
  f' : x₁ ≡ x₀
  f' = f ⁻¹
  t' : tr P f' u₁ ≡ u₀
  t' = (ap (tr P f') t) ⁻¹ ∙ (ap-tr-⁻¹ f u₀)


Σ≡-tran : {A : 𝓤 *} {P : A → 𝓥 *} → (p₀ p₁ p₂ : Σ P) → p₀ Σ≡ p₁ → p₁ Σ≡ p₂ → p₀ Σ≡ p₂
Σ≡-tran (x , u) (x , u) (x , u) (refl x , refl u) (refl x , refl u) = refl x , refl u 

Σ≡-tran' : {A : 𝓤 *} {P : A → 𝓥 *} → (p₀ p₁ p₂ : Σ P) → p₀ Σ≡ p₁ → p₁ Σ≡ p₂ → p₀ Σ≡ p₂
Σ≡-tran' {P = P} (x₀ , u₀) (x₁ , u₁) (x₂ , u₂) (f₀ , t₀) (f₁ , t₁) = f' , t'
 where
  f' = f₀ ∙ f₁
  t' : tr P f' u₀ ≡ u₂
  α  : tr P f' u₀ ≡ tr P f₁ (tr P f₀ u₀)
  α = (transport-∙ f₀ f₁ u₀) ⁻¹
  β  : tr P f₁ (tr P f₀ u₀) ≡ tr P f₁ u₁
  β = ap (tr P f₁) t₀
  γ  : tr P f₁ u₁ ≡ u₂
  γ = t₁
  t' =  α ∙ β ∙ γ

Σ≡-id : {A : 𝓤 *} {P : A → 𝓥 *} (p : Σ P)
        → (refl p) ≡ Σ≡→≡ p p (refl (fst p) , refl (snd p))
Σ≡-id {A = A} {P} (x , y) =  Σ≡-β (x , y) (x , y) (refl (x , y))  

Σ≡-⁻¹   : {A : 𝓤 *} {P : A → 𝓥 *} {p₀ p₁ : Σ P}
        → (e : p₀ ≡ p₁)
        → e ⁻¹ ≡ Σ≡→≡ p₁ p₀ (Σ≡-symm p₀ p₁ (≡→Σ≡ p₀ p₁ e))
Σ≡-⁻¹ (refl (x , y)) = Σ≡-id (x , y)

Σ≡-⁻¹'   : {A : 𝓤 *} {P : A → 𝓥 *} {p₀ p₁ : Σ P}
        → (e : p₀ ≡ p₁)
        → e ⁻¹ ≡ Σ≡→≡ p₁ p₀ (Σ≡-symm' p₀ p₁ (≡→Σ≡ p₀ p₁ e))
Σ≡-⁻¹' (refl (x , y)) =  Σ≡-id (x , y) 


Σ≡-∙    : {A : 𝓤 *} {P : A → 𝓥 *} {p₀ p₁ p₂ : Σ P}
        → (e : p₀ ≡ p₁) → (f : p₁ ≡ p₂)
        → e ∙ f ≡ Σ≡→≡ p₀ p₂ ((Σ≡-tran p₀ p₁ p₂ (≡→Σ≡ p₀ p₁ e)) (≡→Σ≡ p₁ p₂ f))
Σ≡-∙ (refl (x , y)) (refl (x , y)) =  Σ≡-id (x , y)  

Σ≡-∙'    : {A : 𝓤 *} {P : A → 𝓥 *} {p₀ p₁ p₂ : Σ P}
        → (e : p₀ ≡ p₁) → (f : p₁ ≡ p₂)
        → e ∙ f ≡ Σ≡→≡ p₀ p₂ ((Σ≡-tran' p₀ p₁ p₂ (≡→Σ≡ p₀ p₁ e)) (≡→Σ≡ p₁ p₂ f))
Σ≡-∙' (refl (x , y)) (refl (x , y)) =  Σ≡-id (x , y) 





-- TODO: 2.7.4 and remainder for Σ
-- CF Notes/Chapter2.agda for partial work
  

-- p. 112
-- The unit type

-- p. 112
-- Theorem 2.8.1

≡-≃𝟙 : (x₀ x₁ : 𝟙) → (x₀ ≡ x₁) ≃ 𝟙
≡-≃𝟙 x₀ x₁ = (f x₀ x₁) , hasqinv→isequiv (f x₀ x₁) (g x₀ x₁ , α x₀ x₁ , β x₀ x₁)
 where
  f : (x₀ x₁ : 𝟙) → x₀ ≡ x₁ → 𝟙
  f _ _ _ = ⋆

  g : (x₀ x₁ : 𝟙) → 𝟙 → x₀ ≡ x₁
  g ⋆ ⋆ ⋆ = (refl ⋆)
  -- Given `x₀ x₁ : 𝟙` and `p : 𝟙`, need to construct an element of `x₀ ≡ x₁`.
  -- By 𝟙-induction, one may assume `x₀`, `x₁`, `p` are all `⋆`, in which case
  -- `x₀ ≡ x₁` is `⋆ ≡ ⋆`, and we can just return `refl ⋆`

  α : (x₀ x₁ : 𝟙) →  (f x₀ x₁) ∘ (g x₀ x₁) ∼ id
  α ⋆ ⋆ ⋆ = refl ⋆
  -- Given `x₀ x₁ : 𝟙` and `p : 𝟙`, need to construct an element of `f x₀ x₁ (g x₀ x₁ p) ≡ p`.
  -- By 𝟙-induction, one may assume `x₀` `x₁` `p` are all `⋆`,
  -- in which case `g x₀ x₁ p` is judgementally equal to `refl ⋆`,
  -- and `f x₀ x₁ ( … )` is judgementally equal to `⋆`.
  -- (we don't really need to know what `g …` is, since `f` is constant).
  -- Thus, we need to construct an element of `⋆ ≡ ⋆`, and `refl ⋆` does teh job.
  

  β : (x₀ x₁ : 𝟙) →  (g x₀ x₁) ∘ (f x₀ x₁) ∼ id
  β ⋆ ⋆ (refl ⋆) = (refl (refl ⋆)) 
  -- Given `x₀ x₁ : 𝟙` and `p : x₀ ≡ x₁`, need to construct an element of `g x₀ x₁ (f x₀ x₁ p) ≡ p`.
  -- By 𝕁, one may assume `x₀` `x₁` are equal to `x` and `p` is `refl x`,
  -- By 𝟙-induction, one may assume that `x` is `⋆`.
  -- `f x₀ x₁ p` is judgementally `⋆` and `g x₀ x₁ (…)` is `refl ⋆` which is judgementally equal to `p`.


-- p.112
-- Theorem 2.8.1
-- Done with the induction principles properly

≡-≃𝟙' : (x₀ x₁ : 𝟙) → (x₀ ≡ x₁) ≃ 𝟙
≡-≃𝟙' x₀ x₁ = (f x₀ x₁) , hasqinv→isequiv (f x₀ x₁) (g x₀ x₁ , α x₀ x₁ , β x₀ x₁)
 where
  f : (x₀ x₁ : 𝟙) → x₀ ≡ x₁ → 𝟙
  f _ _ _ = ⋆

  g : (x₀ x₁ : 𝟙) → 𝟙 → x₀ ≡ x₁
  g x₀ x₁ p = 𝟙-induction type base x₀ x₁ p
   where
    type = λ x₀ → (x₁ : 𝟙) → 𝟙 → x₀ ≡ x₁
    base : (x₁ : 𝟙) → 𝟙 → ⋆ ≡ x₁
    base = λ x₁ p → 𝟙-induction type' base' x₁ p
     where
      type' = λ x₁ →  𝟙 → ⋆ ≡ x₁
      base' : 𝟙 → ⋆ ≡ ⋆
      base' = λ p → refl ⋆ 
      
  α : (x₀ x₁ : 𝟙) →  (f x₀ x₁) ∘ (g x₀ x₁) ∼ id
  α x₀ x₁ p = 𝟙-induction type base p x₀ x₁
   where
    type = λ (p : 𝟙) → (x₀ x₁ : 𝟙) → ((f x₀ x₁) ∘ (g x₀ x₁)) p ≡ p
    base : (x₀ x₁ : 𝟙) → ((f x₀ x₁) ∘ (g x₀ x₁)) ⋆ ≡ ⋆ 
    base x₀ x₁ = refl ⋆
    -- use the fact that f is constant, so that f … ∘ g … also is

  β : (x₀ x₁ : 𝟙) →  (g x₀ x₁) ∘ (f x₀ x₁) ∼ id
  β x₀ x₁ p = 𝕁 𝟙 type base x₀ x₁ p
   where
    type = λ (x₀ x₁ : 𝟙) (p : x₀ ≡ x₁) → ((g x₀ x₁) ∘ (f x₀ x₁)) p ≡ p
    base : (x : 𝟙) →  ((g x x) ∘ (f x x)) (refl x) ≡ refl x
    base x = 𝟙-induction type' base' x
     where
      type' = λ x → ((g x x) ∘ (f x x)) (refl x) ≡ refl x
      base' : ((g ⋆ ⋆) ∘ (f ⋆ ⋆)) (refl ⋆) ≡ refl ⋆
      base' = refl (refl ⋆)
      



-- p. 112
-- Section 2.9 Π-types and the function extensionality axiom

-- p. 113
-- Eq. 2.9.2

happly : {A : 𝓤 *} {B : A → 𝓥 *} {f g : (x : A) → B x}
       → (f ≡ g) → f ∼ g
happly {f = h} {g = h} (refl h) x = refl (h x)
-- Simple path induction


-- p. 113
-- Axiom 2.9.3
FunctionExtensionality : (𝓢 𝓣 : Universe) → (𝓢 ⊔ 𝓣)⁺ *
FunctionExtensionality 𝓢 𝓣 = {A : 𝓢 *} {B : A → 𝓣 *} {f g : (x : A) → B x}
                             → isequiv {𝓤 = 𝓢 ⊔ 𝓣} {𝓥 = 𝓢 ⊔ 𝓣} (happly {𝓢} {𝓣} {A = A} {B = B} {f} {g})

-- So, from now on, to assume function extensionality to hold for `𝓢` and `𝓣`,
-- it is enough to take an element of type `FunctionExtensionality 𝓢 𝓣` as an argument.
-- Need some care with universes though

postulate FE : FunctionExtensionality 𝓤 𝓥


funext : {A : 𝓤 *} {B : A → 𝓥 *} {f g : (x : A) → B x}
        → f ∼ g → f ≡ g
funext {𝓤} {𝓥} = hasqinv-inverse (isequiv→hasqinv h FE)
 where
  h = happly


FE-α : {A : 𝓤 *} {B : A → 𝓥 *} {f g : (x : A) → B x}
        → happly ∘ funext ∼ 𝑖𝑑 (f ∼ g)
FE-α {𝓤} {𝓥} = hasqinv-retraction (isequiv→hasqinv h FE)
 where
  h = happly

FE-β : {A : 𝓤 *} {B : A → 𝓥 *} {f g : (x : A) → B x}
        → funext ∘ happly ∼ 𝑖𝑑 (f ≡ g)
FE-β {𝓤} {𝓥} = hasqinv-section (isequiv→hasqinv h FE)
 where
  h = happly
 
-- p. 113
-- Path reflectivity, inverses and concatenation can be done pointwise.



Π≡-id : {A : 𝓤 *} {B : A → 𝓥 *}
            → (f : (x : A) → B x)
            → refl f ≡ (funext (λ x → refl (f x)))
-- Note: `happly (refl f) = λ x → refl (f x)` (judgementally) by definition
-- So the result type for `funext-refl` could be `refl f ≡ funext (happly (refl f))`
Π≡-id f = (FE-β (refl f))⁻¹

Π≡-⁻¹   : {A : 𝓤 *} {B : A → 𝓥 *}
            → (f g : (x : A) → B x)
            → (α : f ≡ g)
            → α ⁻¹ ≡ (funext (Π≡-symm (happly α) ))
Π≡-⁻¹ f f (refl f) =  Π≡-id f
-- Comment: By `𝕁`, can assume that `f = g` and `α = refl f`.
-- In that case, the type we want to inhabit is `(refl f)⁻¹ ≡ funext (λ x → (happly (refl f) x)⁻¹)`
-- But since `refl - ⁻¹ = refl -`, the type is  `refl f ≡ funext (λ x → (happly (refl f) x))`
-- which we already inhabited above.

Π≡-∙    : {A : 𝓤 *} {B : A → 𝓥 *}
            → (f g h : (x : A) → B x)
            → (α : f ≡ g) → (β : g ≡ h)
            → α ∙ β ≡ (funext (Π≡-tran (happly α) (happly β) ))
Π≡-∙ f f f (refl f) (refl f) = Π≡-id f
-- Comment: Same idea as above 


-- p. 114
-- TODO

-- p. 115
-- Lemma 2.9.6
-- Lemma 2.9.7
-- TODO

-- Section 2.10 Universes and the univalence axiom

-- Just to respect the convention we had for 𝟙, Σ and Π,
-- the "natural" identity type for types of a common universe
-- is written U≡.

_U≡_ : (A : 𝓤 *) (B : 𝓥 *) → 𝓤 ⊔ 𝓥 *
A U≡ B = A ≃ B

-- Recall that we have defined
-- ≃-refl
-- ≃-symm
-- ≃-tran

-- p. 116
-- Lemma 2.10.1
-- Eq. 2.10.2

-- Short version
id→eqv' : {A B : 𝓤 *} → A ≡ B → A ≃ B
id→eqv' (refl A) = ≃-refl

-- Note quite as in their proof
id→eqv'' : {A B : 𝓤 *} → A ≡ B → A ≃ B
id→eqv'' {𝓤} {A = A} {B = B} p = f , (g , α) , (g , β)
 where
  f : A → B
  f = tr (𝑖𝑑 (𝓤 *)) p 
  g : B → A
  g = tr (𝑖𝑑 (𝓤 *)) (p ⁻¹) 
  α : f ∘ g ∼ 𝑖𝑑 B
  α b = f (g b)                      ≡⟨ transport-∙ (p ⁻¹) p b ⟩
        tr (𝑖𝑑 (𝓤 *)) (p ⁻¹ ∙ p ) b  ≡⟨ ap ((λ - → tr id - b)) (∙-⁻¹-left p) ⟩   
        b ∎ 
  β : g ∘ f ∼ 𝑖𝑑 A
  β a = g (f a)                    ≡⟨ transport-∙ p (p ⁻¹) a ⟩
        tr (𝑖𝑑 (𝓤 *)) (p ∙ p ⁻¹ ) a ≡⟨ ap ((λ - → tr id - a)) (∙-⁻¹-right p) ⟩   
        a ∎ 

≡→map : {A B : 𝓤 *} → A ≡ B → A → B
≡→map {𝓤} p = tr (𝑖𝑑 (𝓤 *)) p

applying-tr-to-type-equality-yields-equivalence : {A B : 𝓤 *} → (p : A ≡ B) → isequiv (≡→map p)
applying-tr-to-type-equality-yields-equivalence (refl A) = snd (≃-refl)

-- As in the book.
-- It's written a bit backwards.
-- First, we note that given a type equality, we get a map by transport (that's `≡→map`)
-- Then, that this map is an equivalence, which is easy using path induction
-- And we get our map from equality to equivalence.

id→eqv : {A B : 𝓤 *} → A ≡ B → A ≃ B
id→eqv {A = A} {B = B} p = (≡→map p) , applying-tr-to-type-equality-yields-equivalence p  

-- Axiom 2.10.3

Univalence : (𝓢 : Universe) → (𝓢)⁺ *
Univalence 𝓢 = {A B : 𝓢 *} → isequiv (id→eqv {𝓤 = 𝓢} {A = A} {B = B})

postulate UV : Univalence 𝓤

ua : {A B : 𝓤 *}
        → A U≡ B → A ≡ B
ua = hasqinv-inverse (isequiv→hasqinv id→eqv UV)

UV-α : {A B : 𝓤 *}
        → id→eqv ∘ ua ∼ 𝑖𝑑 (A U≡ B)
UV-α {𝓤} = hasqinv-retraction (isequiv→hasqinv id→eqv (UV))

UV-β : {A B : 𝓤 *}
        → ua ∘ id→eqv ∼ 𝑖𝑑 (A ≡ B)
UV-β {𝓤} = hasqinv-section (isequiv→hasqinv id→eqv (UV))


U≡-id : (A : 𝓤 *) → (refl A) ≡ ua (id→eqv (refl A))
U≡-id A = (UV-β (refl A))⁻¹

U≡-⁻¹ : {A B : 𝓤 *} → (p : A ≡ B) → p ⁻¹ ≡ ua (≃-symm (id→eqv p))
U≡-⁻¹ (refl A) = U≡-id A 

U≡-∙ : {A B C : 𝓤 *} → (p : A ≡ B) → (q : B ≡ C) → p ∙ q ≡ ua (≃-tran (id→eqv p) (id→eqv q) )
U≡-∙ (refl A) (refl A) = U≡-id A

  -- p. 117
  -- Lemma 2.10.5

lemma-2-10-5 : {A : 𝓤 *} {B : A → 𝓤 *} {x y : A} (p : x ≡ y) (u : B x)
            → (tr B p u ≡ ≡→map (ap B p) u)
            × (≡→map (ap B p) u ≡ (fst (id→eqv (ap B p))) u)
lemma-2-10-5 (refl x) u = refl u , refl u
-- By path induction, everything collapses


-- p. 117
-- Section 2.11 Identity Types

-- p. 118
≡-preserves-equiv : {A : 𝓤 *} {B : 𝓥 *} (f : A → B) →  isequiv f → (a₀ a₁ : A) → isequiv {A = a₀ ≡ a₁} {B = f a₀ ≡ f a₁} (ap f {x₀ = a₀} {x₁ = a₁})
≡-preserves-equiv {A = A} {B = B} f e a₀ a₁ = hasqinv→isequiv f' ( g' , α' , β')
 where
  i : hasqinv f
  i = isequiv→hasqinv f e

  g : B → A
  g = hasqinv-inverse i
  α : f ∘ g ∼ 𝑖𝑑 B
  α = hasqinv-retraction i
  β : g ∘ f ∼ 𝑖𝑑 A
  β = hasqinv-section i

  f' : a₀ ≡ a₁ → f a₀ ≡ f a₁
  f' = ap f {x₀ = a₀} {x₁ = a₁}

  g' : f a₀ ≡ f a₁ → a₀ ≡ a₁
  g' p = (β a₀) ⁻¹ ∙ (ap g p) ∙ (β a₁)

  postulate α' : f' ∘ g' ∼ id
  β' : g' ∘ f' ∼ id
  {-
  α' q = (f' ∘ g') q ≡⟨ refl ( (f' ∘ g') q) ⟩
         ap f ((β a₀) ⁻¹  ∙ (ap g q) ∙ (β a₁))                                                             ≡⟨ {!!} ⟩
          (α (f a₀))⁻¹ ∙ (α (f a₀)) ∙ (ap f ((β a₀) ⁻¹  ∙ (ap g q) ∙ (β a₁))) ∙ (α (f a₁))⁻¹ ∙ (α (f a₁))  ≡⟨ {!!} ⟩
          (α (f a₀))⁻¹ ∙ (ap (f ∘ g) (ap f ((β a₀) ⁻¹  ∙ (ap g q) ∙ (β a₁))))  ∙ (α (f a₁))                ≡⟨ {!!} ⟩
          (α (f a₀))⁻¹ ∙ (ap f (ap (g ∘ f) ((β a₀) ⁻¹  ∙ (ap g q) ∙ (β a₁))))  ∙ (α (f a₁))                ≡⟨ {!!} ⟩
          (α (f a₀))⁻¹ ∙ (ap f (ap g q))   ∙ (α (f a₁))                                                    ≡⟨ {!!} ⟩
          (α (f a₀))⁻¹ ∙ (ap (f ∘ g) q)   ∙ (α (f a₁))                                                     ≡⟨ {!!} ⟩
         q ∎
  -}
  β' p = (g' ∘ f') p                          ≡⟨ refl ( (g' ∘ f') p) ⟩
         (β a₀)⁻¹  ∙ (ap g (ap f p)) ∙ (β a₁)  ≡⟨ ap (λ - → (β a₀)⁻¹  ∙ - ∙ (β a₁)) (ap-∘ f g p)⁻¹ ⟩ 
         (β a₀)⁻¹  ∙ (ap (g ∘ f) p) ∙ (β a₁)   ≡⟨ ∼-is-natural' (g ∘ f) (𝑖𝑑 A) β {x₀ = a₀} {x₁ = a₁} p  ⟩ 
         ap (𝑖𝑑 A) p                           ≡⟨ ap-id p ⟩
         p ∎

-- Assuming univalence, we could get `A ≡ B` from `A ≃ B`, and use the following (and then transform hasqinv to isequiv) to prove the above:
≡-preserves-≡ : {A B : 𝓤 *} (p : A ≡ B)
              → (a₀ a₁ : A)
              → hasqinv (ap (≡→map p) {x₀ = a₀} {x₁ = a₁})
≡-preserves-≡ (refl A) a₀ a₁ = f , α , α
 where
  f : a₀ ≡ a₁ → a₀ ≡ a₁ 
  f = ap (𝑖𝑑 A)

  α : f ∘ f ∼ (𝑖𝑑 (a₀ ≡ a₁))
  α p = (f ∘ f) p ≡⟨ refl ((f ∘ f) p) ⟩
        ap (𝑖𝑑 A) (ap (𝑖𝑑 A) p) ≡⟨ (ap-∘ (𝑖𝑑 A) (𝑖𝑑 A) p)⁻¹ ⟩
        ap (𝑖𝑑 A) p ≡⟨ ap-id p ⟩
        p           ∎

-- TODO: make this work!!!!!!

-- p. 119
-- Lemma 2.11.2

transport-path-start : {A : 𝓤 *} {a : A} {x₀ x₁ : A}
                     → (p : x₀ ≡ x₁) (q : a ≡ x₀)
                     → tr (λ x → a ≡ x) p q ≡ q ∙ p
transport-path-start (refl x) q =  (∙-refl-right q) ⁻¹
transport-path-end   : {A : 𝓤 *} {a : A} {x₀ x₁ : A}
                     → (p : x₀ ≡ x₁) (q : x₀ ≡ a)
                     → tr (λ x → x ≡ a) p q ≡ p ⁻¹ ∙ q
transport-path-end (refl x) q =  (∙-refl-left q) ⁻¹
transport-path-both   : {A : 𝓤 *} {a : A} {x₀ x₁ : A}
                     → (p : x₀ ≡ x₁) (q : x₀ ≡ x₀)
                     → tr (λ x → x ≡ x) p q ≡ p ⁻¹ ∙ q ∙ p 
transport-path-both (refl x) q =  (∙-refl-right q) ⁻¹ -- Don't need  `∙-refl-left q` because this part holds judgementally

-- I need it later
transport-path-self  : {A : 𝓤 *} {a : A} {x₀ : A}
                     → (p : x₀ ≡ a) 
                     → tr (λ x → x ≡ a) p p ≡ refl a
transport-path-self {a = a} p = tr (λ x → x ≡ a) p p   ≡⟨ transport-path-end p p ⟩
                                      p ⁻¹ ∙ p ≡⟨ ∙-⁻¹-left p ⟩ 
                                       refl a ∎ 


-- p. 119
-- Lemma 2.11.3

transport-path-functions : {A : 𝓤 *} {B : 𝓥 *} (f g : A → B)
                         → {a₀ a₁ : A} → (p : a₀ ≡ a₁) → (q : f a₀ ≡ g a₀)
                         → tr (λ x → f x ≡ g x) p q ≡ (ap f p)⁻¹ ∙ q ∙ (ap g p)
transport-path-functions f g (refl a) q =  (∙-refl-right q) ⁻¹

-- Lemma 2.11.2 using 2.11.3
-- Only the first one, the others are similar
{-
transport-path-start' : {A : 𝓤 *} {a : A} {x₀ x₁ : A}
                     → (p : x₀ ≡ x₁) (q : a ≡ x₀)
                     → tr (λ x → a ≡ x) p q ≡ q ∙ p
transport-path-start' {A = A} {a} {x₀} {x₁} p q = tr (λ x → a ≡ x) p q ≡⟨ refl {!!} ⟩ 
                                                  tr (λ x → f x ≡ g x) p q ≡⟨  transport-path-functions f g p q ⟩
                                                  (ap f p)⁻¹ ∙ q ∙ (ap g p) ≡⟨ {!!} ⟩
                                                  (refl a)  ∙ q ∙ p ≡⟨ {!!} ⟩
                                                  q ∙ p ∎
 where
  f = λ (x : A) → a
  g = λ (x : A) → x
-}

-- p. 119
-- Theorem 2.11.4
-- TODO

-- p. 119
-- Theorem 2.11.5

-- need this
∙-left-yields-≃ : {A : 𝓤 *} {x y z : A} (p : x ≡ y) → (y ≡ z) ≃ (x ≡ z)
∙-left-yields-≃ p = (p ∙_ ) , hasqinv→isequiv (p ∙_ ) (∙-left-is-qinv p)

-- and this
∙-right-yields-≃ : {A : 𝓤 *} {x y z : A} (p : x ≡ y) → (z ≡ x) ≃ (z ≡ y)
∙-right-yields-≃ p = (_∙ p) , hasqinv→isequiv (_∙ p) (∙-right-is-qinv p)

Theorem-2-11-5 : {A : 𝓤 *} {a₀ a₁ : A}
               → (p : a₀ ≡ a₁) → (q : a₀ ≡ a₀) → (r : a₁ ≡ a₁)
               → (tr (λ x → x ≡ x) p q ≡ r) ≃ (q ∙ p ≡ p ∙ r)
-- When `p = refl a`, the output type is `(q ≡ r) ≃ (q  ∙ (refl a) ≡ (refl a) ∙ r )`
Theorem-2-11-5 {A = A} {a} {a} (refl a) q r = ≃-tran fst-equiv snd-equiv
 where
  α : q ∙ (refl a) ≡ q
  α = ∙-refl-right q 
  β : (refl a) ∙ r ≡ r
  β = ∙-refl-left r

  fst-equiv : (q ≡ r) ≃ (q  ∙ (refl a) ≡ r)
  snd-equiv : (q  ∙ (refl a) ≡ r) ≃ (q  ∙ (refl a) ≡ (refl a) ∙ r)

  fst-equiv = ∙-left-yields-≃ α 
  snd-equiv = ∙-right-yields-≃ β


-- p. 120
-- Section 2.12 Coproducts

-- p. 121


-- I thought the following would be useful, but it turns out not to be.
-- leaving it here anyway
→𝟘-implies-≃𝟘 : {A : 𝓤 *} → (f : A → 𝟘) → A ≃ 𝟘
→𝟘-implies-≃𝟘 {A = A} f = f , hasqinv→isequiv f (g , α , β)
 where
  g : 𝟘 → A
  g = 𝟘-recursion A
  α : f ∘ g ∼ 𝑖𝑑 𝟘
  α = 𝟘-induction (λ b → (f ∘ g) b ≡ b)
  β : g ∘ f ∼ 𝑖𝑑 A
  β a = 𝟘-induction (λ x → (g ∘ f) a ≡ a) (f a)


-- Define the codes (can't get variable universes to work)
+-code-≡ : {A : 𝓤₀ *} {B : 𝓤₀ *} (x : A + B) (y : A + B) → 𝓤₀ *
+-code-≡ {A = A} {B = B} x y = +-recursion left right x y
 where
  left : A → A + B → 𝓤₀ *
  left a =  +-recursion leftl rightl 
   where
    leftl : A → 𝓤₀ *
    leftl a' = a ≡ a'
    rightl : B → 𝓤₀ *
    rightl b = 𝟘
  right : B → A + B → 𝓤₀ *
  right b =  +-recursion leftr rightr 
   where
    leftr : A → 𝓤₀ *
    leftr a = 𝟘
    rightr : B → 𝓤₀ *
    rightr b' = b ≡ b'

{- -- The following is way easier on the eyes, but I want to train using the induction principles…
+-code-≡ (inl a₁) (inl a₂) = a₁ ≡ a₂
+-code-≡ (inr b₁) (inr b₂) = b₁ ≡ b₂ 
+-code-≡ (inl a) (inr b) = 𝟘
+-code-≡ (inr b) (inl a) = 𝟘
-}

-- Map from equality type to corresponding code
+-enc-≡ : {A : 𝓤₀ *} {B : 𝓤₀ *}
        → (x : A + B) (y : A + B)
        → (p : x ≡ y) → +-code-≡ x y
+-enc-≡ {A = A} {B = B} x y p = 𝕁 (A + B) type base x y p
 where
  type = λ (x y : A + B) (p : x ≡ y) → +-code-≡ x y
  base : (x : A + B) → +-code-≡ x x
  base = +-induction (λ (x : A + B) → +-code-≡ x x)
                     (λ (a : A) → refl a)
                     (λ (b : B) → refl b)
{- -- Again, pattern matching is cleaner, but we want to learn!!!
   -- But… actually… in this case… it's not so much better.
+-enc-≡ (inl a) (inl a) (refl (inl a)) = refl a
+-enc-≡ (inr b) (inr b) (refl (inr b)) = refl b
-}

-- Map from code to equality
+-dec-≡ : {A : 𝓤₀ *} {B : 𝓤₀ *}
        → (x : A + B) (y : A + B)
        → (q : +-code-≡ x y) → x ≡ y
+-dec-≡ {A = A} {B = B} x y q = +-induction type left right x y q
 where
  type = λ (x : A + B) → (y : A + B) →  (q : +-code-≡ x y) → x ≡ y
  left : (a : A) → (y : A + B) →  +-code-≡ (inl a) y → (inl a) ≡ y
  right : (b : B) → (y : A + B) →  +-code-≡ (inr b) y → (inr b) ≡ y
  left a = +-induction typel leftl rightl
   where
    typel = λ (y : A + B) →  +-code-≡ (inl a) y → (inl a) ≡ y
    leftl : (a' : A) →  +-code-≡ {A = A} {B = B} (inl a) (inl a') → (inl a) ≡ (inl a')
    rightl : (b : B) → +-code-≡ (inl a) (inr b) → (inl a) ≡ (inr b)
    leftl a' q = 𝕁 A (λ a₀ a₁ q → (inl a₀) ≡ (inl a₁)) (λ a → refl (inl a)) a a' q
    rightl b = 𝟘-recursion {𝓤 = 𝓤₀} (inl a ≡ inr b) -- because the type `+-code-≡ (inl a) (inr b)` is 𝟘
    
  right b = +-induction typer leftr rightr
   where
    typer = λ (y : A + B) →  +-code-≡ (inr b) y → (inr b) ≡ y
    leftr : (a : A) →  +-code-≡ (inr b) (inl a) → (inr b) ≡ (inl a)
    rightr : (b' : B) → +-code-≡ {A = A} {B = B} (inr b) (inr b') → (inr b) ≡ (inr b')
    leftr a = 𝟘-recursion {𝓤 = 𝓤₀} (inr b ≡ inl a)
    rightr b' q = 𝕁 B (λ b₀ b₁ q → (inr b₀) ≡ (inr b₁)) (λ b → refl (inr b)) b b' q
{-
+-dec-≡ (inl a₁) (inl a₂) q = ap inl q
+-dec-≡ (inr b₁) (inr b₂) q = ap inr q
+-dec-≡ (inl a) (inr b) = 𝟘-recursion (inl a ≡ inr b)
+-dec-≡ (inr b) (inl a) = 𝟘-recursion (inr b ≡ inl a)
-}


+-α-≡ :  {A : 𝓤₀ *} {B : 𝓤₀ *}
        → (x : A + B) (y : A + B)
      → (+-enc-≡ x y) ∘ (+-dec-≡ x y) ∼ 𝑖𝑑 (+-code-≡ x y)

+-α-≡ {A = A} {B = B} (inl a₀) (inl a₁) p = 𝕁 A type base a₀ a₁ p
 where
  type = λ a₀ a₁ p → +-enc-≡  (inl a₀) (inl a₁) (+-dec-≡ (inl a₀) (inl a₁) p) ≡ p
  base : (a : A) → +-enc-≡  {A = A} {B = B}  (inl a) (inl a) (+-dec-≡  (inl a) (inl a) (refl a)) ≡ (refl a)
  base a = refl (refl a)
+-α-≡ {A = A} {B = B} (inr b₀) (inr b₁) p = 𝕁 B type base b₀ b₁ p
 where
  type = λ b₀ b₁ p → +-enc-≡  (inr b₀) (inr b₁) (+-dec-≡  (inr b₀) (inr b₁) p) ≡ p
  base : (b : B) → +-enc-≡  {A = A} {B = B}  (inr b) (inr b) (+-dec-≡ (inr b) (inr b) (refl b)) ≡ (refl b)
  base b = refl (refl b)
+-α-≡ {A = A} {B = B} (inl a) (inr b) p = 𝟘-recursion (+-enc-≡ (inl a) (inr b) (+-dec-≡ (inl a) (inr b) p)  ≡ p) p
+-α-≡ {A = A} {B = B} (inr b) (inl a) p =  𝟘-recursion (+-enc-≡ (inr b) (inl a) (+-dec-≡ (inr b) (inl a) p) ≡ p) p
{- 
+-α-≡ (inl a) (inl a) (refl a) = refl (refl a)
+-α-≡ (inr b) (inr b) (refl b) = refl (refl b)
-}

-- The other side of the qinv'ness
+-β-≡ :  {A : 𝓤₀ *} {B : 𝓤₀ *}
        → (x : A + B) (y : A + B)
      → (+-dec-≡ x y) ∘ (+-enc-≡ x y) ∼ 𝑖𝑑 (x ≡ y)

+-β-≡ (inl a) (inl a) (refl (inl a)) = refl (refl (inl a)) 
+-β-≡ (inr b) (inr b) (refl (inr b)) = refl (refl (inr b))
-- This one is simpler: first use 𝕁 to reduce the arguments to `x x (refl x)` and then +-induction on `x`

-- Putting it all together:
+-qinv-≡ : {A : 𝓤₀ *} {B : 𝓤₀ *} (x : A + B) (y : A + B)
            → hasqinv (+-enc-≡ x y)
+-qinv-≡ x y = +-dec-≡ x y , +-α-≡ x y , +-β-≡ x y
+-equiv-≡ : {A : 𝓤₀ *} {B : 𝓤₀ *} (x : A + B) (y : A + B)
            →  (x ≡ y) ≃ (+-code-≡ x y)
+-equiv-≡ x y = f , hasqinv→isequiv f i
 where
  f = +-enc-≡ x y
  i = +-qinv-≡ x y

Eq-2-12-1 : {A : 𝓤₀ *} {B : 𝓤₀ *} (a₁ a₂ : A)
          → ((inl {𝓤 = 𝓤₀} {𝓥 = 𝓤₀} {X = A} {Y = B} a₁) ≡ (inl a₂)) ≃ (a₁ ≡ a₂)
Eq-2-12-2 : {A : 𝓤₀ *} {B : 𝓤₀ *} (b₁ b₂ : B)
          → ((inr {𝓤 = 𝓤₀} {𝓥 = 𝓤₀}  {X = A} {Y = B}  b₁) ≡ (inr b₂)) ≃ (b₁ ≡ b₂)
Eq-2-12-3 : {A : 𝓤₀ *} {B : 𝓤₀ *} (a : A) (b : B)
          → ((inl a) ≡ (inr b)) ≃ 𝟘
Eq-2-12-4 : {A : 𝓤₀ *} {B : 𝓤₀ *} (b : B) (a : A)
          → ((inr b) ≡ (inl a)) ≃ 𝟘
Eq-2-12-1 a₁ a₂ = +-equiv-≡ (inl a₁) (inl a₂) 
Eq-2-12-2 b₁ b₂ = +-equiv-≡ (inr b₁) (inr b₂) 
Eq-2-12-3 a₁ b₂ = +-equiv-≡ (inl a₁) (inr b₂) 
Eq-2-12-4 b₁ a₂ = +-equiv-≡ (inr b₁) (inl a₂) -- does not exist in the book but useful 

-- p. 123
-- Remark 2.12.6
₀≢₁ : ₀ ≢ ₁
₀≢₁ = fst (Eq-2-12-3 ⋆ ⋆ )


-- p. 123
-- End of the section

p-123 : {X : 𝓤 *} {A B : X → 𝓥 *}
  → {x₀ x₁ : X}
  → (p : x₀ ≡ x₁)
  → (a : A x₀)
  → transport (λ x → (A x) + (B x)) p (inl a) ≡ inl (transport A p a)
p-123 (refl x) a = refl (inl a)


-- p. 123
-- Section 2.13 Natural numbers

ℕ-code-≡ : (m n : ℕ) → 𝓤₀ *
{-
ℕ-code-≡ zero zero = 𝟙
ℕ-code-≡ zero (succ m) = 𝟘
ℕ-code-≡ (succ n) zero = 𝟘
ℕ-code-≡ (succ n) (succ m) = ℕ-code-≡ n m
-}
ℕ-code-≡ m n = ℕ-recursion (ℕ → 𝓤₀ *) z s m n
 where
  -- We define the code by induction on `m` and then on `n`.
  -- That means, with `m` fixed, we must define a function `code(m,-): ℕ → 𝓤₀ *`

  -- If `m = 0`:
  -- The function is defined by induction on `n`:
  -- If `n = 0`, the code is `𝟙`
  -- And if `n` is successor, no matter what the code for `n-1` was, the code is `𝟘`
  z : ℕ → 𝓤₀ *
  z n = ℕ-recursion (𝓤₀ *) zz zs n
   where
    zz = 𝟙
    zs = λ (n : ℕ) (T : 𝓤₀ *) → 𝟘

  -- If `m` is successor:
  -- Then the function is again defined by induction on `n`.
  -- By the induction hypothesis, we are given `m` and `S = code(m,-)` and must define `code(m+1,-)`.
  -- For `n=0` the code is `𝟘`, and for `n+1`, we have `code(m+1,n+1) = code(m,n)`.
  s : (m : ℕ) → (S : ℕ → 𝓤₀ *) → ℕ → 𝓤₀ *
  s m S =  ℕ-recursion (𝓤₀ *) sz ss
   where
    sz = 𝟘
    ss = λ (n : ℕ) (T : 𝓤₀ *) → S n
    -- Here `T = code(succ m, n)`, which we don't care about

-- Note: by the defining equalities for ℕ-induction, we have, judgementally:
--   code 0 0 = 𝟙
--   code 0 (succ n) = 𝟘
--   code (succ m) 0 = 𝟘
--   code (succ m) (succ n) = code m n
--
-- Indeed `code 0 = z` and `z 0 = 𝟙` and `z (succ n) = zs n (z n) = 𝟘` (all judgementally)
-- And similarly for the "successor m" cases:
-- Judgementally:
--   code (succ m) = s m (code m)
-- So that
--   code (succ m) 0 = s m (code m) 0 = sz = 𝟘
-- and
--   code (succ m) (succ n) = s m (code m) (succ n) = ss n (s m (code m n)) = code m n 

ℕ-enc-≡ : (m n : ℕ) → (p :  m ≡ n) → ℕ-code-≡ m n
ℕ-enc-≡ zero zero (refl zero) = ⋆ 
ℕ-enc-≡ (succ m) (succ m) (refl (succ m)) = ℕ-enc-≡ m m (refl m)
-- First path induction, then ℕ-induction

ℕ-dec-≡ : (m n : ℕ) → (c : ℕ-code-≡ m n) → m ≡ n
ℕ-dec-≡ zero zero ⋆ = refl zero
ℕ-dec-≡ zero (succ m) = 𝟘-recursion (zero ≡ succ m)
ℕ-dec-≡ (succ n) zero = 𝟘-recursion (succ n ≡ zero)
ℕ-dec-≡ (succ n) (succ m) c = ap succ (ℕ-dec-≡ n m c)

ℕ-α-≡ : (m n : ℕ) → (ℕ-enc-≡ m n) ∘ (ℕ-dec-≡ m n) ∼ 𝑖𝑑 (ℕ-code-≡ m n)
ℕ-α-≡ zero zero ⋆ = refl ⋆
ℕ-α-≡ (succ m) zero = 𝟘-induction (λ c → ((ℕ-enc-≡ (succ m) zero) ∘ (ℕ-dec-≡ (succ m) zero)) c ≡ c) 
ℕ-α-≡ zero (succ n) = 𝟘-induction (λ c → ((ℕ-enc-≡ zero (succ n)) ∘ (ℕ-dec-≡ zero (succ n))) c ≡ c)
ℕ-α-≡ (succ m) (succ n) c = ℕ-enc-≡ m' n' (ℕ-dec-≡ m' n' c)          ≡⟨ ap (λ - → ℕ-enc-≡ m' n' -) (refl (ℕ-dec-≡ m' n' c))  ⟩
                            ℕ-enc-≡ m' n' (ap succ (ℕ-dec-≡ m n c))  ≡⟨  lol m n (ℕ-dec-≡ m n c) ⟩
                            ℕ-enc-≡ m n (ℕ-dec-≡ m n c)              ≡⟨ IH ⟩
                             c ∎
 where
  m' = succ m
  n' = succ n
  IH = ℕ-α-≡ m n c
  lol : (m n : ℕ) → (p : m ≡ n) → ℕ-enc-≡ (succ m) (succ n) (ap succ p) ≡ ℕ-enc-≡ m n p
  lol zero zero (refl zero) = refl ⋆
  lol (succ m) (succ m) (refl (succ m)) = refl (ℕ-enc-≡ m m (refl m))


ℕ-β-≡ : (m n : ℕ) → (ℕ-dec-≡ m n) ∘ (ℕ-enc-≡ m n) ∼ 𝑖𝑑 (m ≡ n)
ℕ-β-≡ zero zero (refl zero) = refl (refl zero)
ℕ-β-≡ (succ m) (succ m) (refl (succ m)) =
 ℕ-dec-≡ m' m'  (ℕ-enc-≡ m' m' p')     ≡⟨ ap (λ - → ℕ-dec-≡ m' m' -) (refl ((ℕ-enc-≡ m m p))) ⟩
 ℕ-dec-≡ m' m' (ℕ-enc-≡ m m p)         ≡⟨ refl (ℕ-dec-≡ m' m' (ℕ-enc-≡ m m p)  )  ⟩
 ap succ (ℕ-dec-≡ m m (ℕ-enc-≡ m m p)) ≡⟨ ap (λ - → ap succ -) IH ⟩
 ap succ p                              ≡⟨ refl p' ⟩
 p' ∎
 where
  m' = succ m
  p' = refl (succ m)
  p = refl m
  IH = ℕ-β-≡ m m p


ℕ-qinv-≡ : (m n : ℕ) → hasqinv (ℕ-enc-≡ m n)
ℕ-qinv-≡ m n = (ℕ-dec-≡ m n) , (ℕ-α-≡ m n) , (ℕ-β-≡ m n)

ℕ-equiv-≡ : (m n : ℕ) → (m ≡ n) ≃ (ℕ-code-≡ m n)
ℕ-equiv-≡ m n = (ℕ-enc-≡ m n) , hasqinv→isequiv (ℕ-enc-≡ m n) (ℕ-qinv-≡ m n)

-- p. 125
-- Eq. 2.13.2

zero-not-succ : (n : ℕ) → zero ≢ (succ n)
zero-not-succ n = ℕ-enc-≡ zero (succ n)


-- p. 125
-- Eq. 2.13.3
succ-≡ : (m n : ℕ) → succ m ≡ succ n → m ≡ n
succ-≡ m n p = ℕ-dec-≡ m n (ℕ-enc-≡ (succ m) (succ n) p)  


-- p. 129
-- Section 2.15 Universal properties

-- Eq. 2.15.1

×-fwd : {X : 𝓤 *} {A : 𝓥 *} {B : 𝓦 *}
      → (X → A × B)
        -------------------
      → (X → A) × (X → B)
×-fwd f = fst ∘ f , snd ∘ f
×-bwd : {X : 𝓤 *} {A : 𝓥 *} {B : 𝓦 *}
      → (X → A) × (X → B)
        -------------------
      → (X → A × B)
×-bwd (g , h) = λ x → (g x , h x)

×-α   : {X : 𝓤 *} {A : 𝓥 *} {B : 𝓦 *}
      → (FE : (𝓢 𝓣 : Universe) → FunctionExtensionality 𝓢 𝓣)
      → (ϕ : (X → A) × (X → B))
        -------------------
      → ×-fwd (×-bwd ϕ) ≡ ϕ
×-α {X = X} {A} {B} FE (f , g) =  refl (f , g)
-- Let's explain:
--   ×-bwd (f , g) = λ x → (f x , g x)
-- so that
--   ×-fwd ∘ ×-bwd (f , g) = (fst ∘ (λ x → (f x , g x))) , (snd ∘ (λ x → (f x , g x)))
--                         =  λ x → fst (f x , g x)      , λ x → snd (f x , g x)
-- This is because `∘` is defined like that.
--                         =  λ x → f x                  , λ x → g x
-- This is because `fst` and `snd` are defined like that.
--                         = f                           , g
-- This is because of eta-equivalence.
-- Everything holds judgementally, and we're good.

×-β   : {X : 𝓤 *} {A : 𝓥 *} {B : 𝓦 *}
      → (f : X → A × B)
        -------------------
      → ×-bwd (×-fwd f) ≡ f
×-β {X = X} {A} {B} f = funext β∼ 
 where
  β∼ :  ×-bwd (×-fwd f) ∼ f
  β∼ x =  (Σ-uniq (f x))⁻¹
  -- We have:
  --   ×-bwd (×-fwd f) = λ x → (fst (f x) , snd (f x))
  -- So
  --   β∼ x : fst (f x) , snd (f x) ≡ f x
  -- But this holds judgementally

{- stopped working after I made funext a postulate
×-UMP : {X : 𝓤 *} {A : 𝓥 *} {B : 𝓦 *}
      → hasqinv (×-fwd {X = X} {A = A} {B = B})
×-UMP {X = X} {A = A} {B = B} = ×-bwd , ×-α , ×-β
-} 
-- p. 130
-- Theorem 2.15.7


-- Eq. 2.15.6
Σ-fwd : {X : 𝓤 *} {A : X → 𝓥 *} {P : (x : X) → A x → 𝓦 *}
      → ( (x : X) → Σ (λ (a : A x) → P x a))
      --------------------------------------------------
      → Σ (λ (g : (x : X) → A x) → (x : X) → P x (g x) )
Σ-fwd {X = X} {A = A} {P = P} F = (λ x → fst (F x)) , (λ x → snd (F x)) -- Would like to write just (fst ∘ F, snd ∘ F) but implicit arguments make it painful

Σ-bwd : {X : 𝓤 *} {A : X → 𝓥 *} {P : (x : X) → A x → 𝓦 *}
      → Σ (λ (g : (x : X) → A x) → (x : X) → P x (g x) )
      --------------------------------------------------
      → ( (x : X) → Σ (λ (a : A x) → P x a))
Σ-bwd {X = X} {A = A} {P = P} (f , ϕ) = (λ x → f x , ϕ x)

Σ-α   : {X : 𝓤 *} {A : X → 𝓥 *} {P : (x : X) → A x → 𝓦 *}
      → (F : Σ (λ (g : (x : X) → A x) → (x : X) → P x (g x) ))
      → Σ-fwd {X = X} {A} {P} (Σ-bwd {X = X} {A} {P} F) ≡ F
Σ-α (f , ϕ) =  refl (f , ϕ) 
-- Without loss of generality `F = (f , ϕ)` and we want to show that `Σ-fwd (Σ-bwd (f , ϕ))  ≡ (f , ϕ)`.
-- Actually, this is going to hold judgementally, i.e.               `Σ-fwd (Σ-bwd (f , ϕ)) = (f , ϕ)`
-- We have:
--   Σ-fwd (Σ-bwd (f , ϕ)) = Σ-fwd (λ x → (f x , ϕ x))
--                         = λ x → fst (f x , ϕ x) , λ x → snd (f x , ϕ x)       (by def)
--                         = λ x → f x             , λ x → ϕ x                   (since fst (a , b) = a judgementally and snd (a , b) = b judgementally)
--                         = f                     , ϕ                           (by eta)
Σ-β   : {X : 𝓤 *} {A : X → 𝓥 *} {P : (x : X) → A x → 𝓦 *}
      → (F : (x : X) → Σ (λ (a : A x) → P x a))
      → Σ-bwd (Σ-fwd F) ≡ F
Σ-β {X = X} {A} {P} F =  funext β∼ 
 where
  β∼ :  Σ-bwd (Σ-fwd F) ∼ F
  β∼ x = (Σ-uniq' (F x)) ⁻¹ 
-- Here we start with `F` and want to show `Σ-bwd (Σ-fwd F) ≡ F`.
-- By extensionality, it suffices to show `∼`.
-- So, given `x`, we need to show that `bwd (fwd F) x ≡ F x`.
-- But
--   fwd F = (λ x → fst F x) , (λ x → snd F x)
-- So
--   bwd fwd F = λ x →  (λ x → fst F x) (x) , (λ x → snd F x) (x) = λ x →  (fst F x , snd F x)
-- And we thus have to show that, given any `x`, we have
--   F x ≡ (fst F x , snd F x)
-- And this holds by Σ-uniq'.


Σ-UMP : {X : 𝓤 *} {A : X → 𝓥 *} {P : (x : X) → A x → 𝓦 *}
      → hasqinv (Σ-fwd {X = X} {A = A} {P = P})
Σ-UMP {X = X} {A} {P} = Σ-bwd , (Σ-α  {X = X} {A} {P}) , Σ-β
