# Personal training repo for HoTT using:

* Escardo's tutorial: https://www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes/HoTT-UF-Agda.html
* The HoTT book
* The 1lab
* People of ##dependent on libera.chat

## Differences with Escardo' tutorial:

* No eta-equality for Σ-types

