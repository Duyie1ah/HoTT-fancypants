{-# OPTIONS --without-K --exact-split  --allow-unsolved-metas  --show-implicit #-}



module Chapter-05 where


open import Chapter-04 public

-- W type, i.e. well-founded trees

data W {𝓤 𝓥} {A : 𝓤 *} (B : A → 𝓥 *) : 𝓤 ⊔ 𝓥 * where
 sup : (a : A) → (f : B (a) → W B) → W B

+W : {𝓤 𝓥 : Universe} (X : 𝓤 *) (Y : X → 𝓥 *) → 𝓤 ⊔ 𝓥 *
+W X Y = W Y

-- notation stolen from 1lab
syntax +W X (λ x → y) = W[ x ∈ X ]  y


  
W-induction : {A : 𝓤 *} {B : A → 𝓥 *} (E : (x : W B) → 𝓦 *)
              (e : (a : A) (f : B a → W B) → (sub : (x : B a) → E (f x)) →  E (sup a f))
            → (x : W B) → E x
W-induction E e (sup a f) = e a f (λ x → W-induction E e (f x))

W-induction-def : {A : 𝓤 *} {B : A → 𝓥 *} (E : (x : W B) → 𝓦 *)
              (e : (a : A) (f : B a → W B) → (g : (x : B a) → E (f x)) →  E (sup a f))
            → (a : A) (f : B a → W B) 
            → W-induction E e (sup a f) ≡ e a f (λ (x : B a) → W-induction E e (f x))
W-induction-def E e a f = refl (W-induction E e (sup a f))



-- p. 205
-- Theorem 5.3.1

Theorem-5-3-1 : {A : 𝓤 *} {B : A → 𝓥 *} (E : (x : W B) → 𝓦 *)
                (e : (a : A) (f : B a → W B) → (sub : (x : B a) → E (f x)) →  E (sup a f))
                (ϕ ψ : (x : W B) → E x)
              → ( (a : A) (f : B a → W B) →  ϕ (sup a f) ≡ e a f (λ (x : B a) → ϕ (f x)))
              → ( (a : A) (f : B a → W B) →  ψ (sup a f) ≡ e a f (λ (x : B a) → ψ (f x)))
              → (x : W B) → ϕ x ≡ ψ x
Theorem-5-3-1 {A = A} {B = B} E e ϕ ψ ϕ-ind ψ-ind x = W-induction {A = A} {B = B}
  ( λ (x : W B) → ϕ x ≡ ψ x)
  ( λ a f sub → ϕ (sup a f)                   ≡⟨ ϕ-ind a f ⟩
                e a f (λ (x : B a) → ϕ (f x)) ≡⟨ ap (λ - → e a f -) (funext (λ (x : B a) → sub x)) ⟩
                e a f (λ (x : B a) → ψ (f x)) ≡⟨ (ψ-ind a f) ⁻¹ ⟩
                ψ (sup a f)                   ∎
  )
  x

-- Let's try defining lists 
List : (A : 𝓤₀ *) → 𝓤₀ *
List A = W[ a ∈ 𝟙 + A ] +-induction (λ a → 𝓤₀ *) (λ p →  𝟘) (λ a → 𝟙) a

List-nil : {A : 𝓤₀ *} → List A
List-nil {A = A} = sup (inl ⋆) (!𝟘 (List A))
List-cons : {A : 𝓤₀ *} → List A → A → List A
List-cons {A = A} l a = sup (inr a) (λ p → l)

ε : {A : 𝓤₀ *} → List A
ε = List-nil

_::_ : {A : 𝓤₀ *} → A → List A → List A
a :: l = List-cons l a

List-induction : {A : 𝓤₀ *} {E : List A → 𝓤 *} (e-nil : E ε) (e-cons : (a : A) → (tail : List A) → E tail → E (a :: tail)) → (l : List A) → E l
List-induction {A = A} {E = E} e-nil e-cons l = W-induction E base l
 where
  B = +-induction (λ a → 𝓤₀ *) (λ p →  𝟘) (λ a → 𝟙)
  base : (a : 𝟙 + A) → (f : B a → W B) → (g : (x : B a) → E (f x)) →  E (sup a f)
  base = +-induction (λ (a : 𝟙 + A) →  (f : B a → W B) → (g : (x : B a) → E (f x)) →  E (sup a f)) left right
   where
    -- Note: `List A = W B` by def
    -- `B (inr a) = 𝟙` by def
    -- `B (inl p) = 𝟘` by def
    left : (p : 𝟙) → (f : 𝟘 → List A) → (g : (x : 𝟘) → E (f x)) →  E (sup (inl p) f)
    left ⋆ f g = tr (λ - → E (sup (inl ⋆) - )) lol e-nil
     where
      lol : !𝟘 (List A) ≡  f 
      lol = funext (λ x → !𝟘 (!𝟘 (List A) x ≡ f x) x)
      
    right : (a : A) → (f : 𝟙 → List A) → (g : (x : 𝟙) → E (f x)) →  E (sup (inr a) f)
    right a f g = tr (λ - → E (sup (inr a) -)) lol (e-cons a (f ⋆) (g ⋆))
     where
      lol∼ : (p : 𝟙) → (f ⋆) ≡ f p 
      lol∼ ⋆ = refl (f ⋆)
      lol : (λ (p : 𝟙) → (f ⋆)) ≡ f 
      lol = funext lol∼


List-len : {A : 𝓤₀ *} → List A → ℕ
List-len = List-induction  zero (λ a tail tail-len →  succ tail-len)


List-map : {A B : 𝓤₀ *} → (f : A → B) → List A → List B
List-map {A = A} {B = B} f l = List-induction {A = A} (ε {A = B}) (λ a tail f-tail → (f a) :: f-tail) l

List-append : {A : 𝓤₀ *} → List A → List A → List A
List-append l₁ l₂ = List-induction l₂ (λ h t t++l₂ → h :: t++l₂) l₁
_:::_ = List-append

module Identity-Systems (𝓤 𝓥 𝓦 𝓣 : Universe)
 where

  Unbased = (A : 𝓤 *) (R : A → A → 𝓥 *) (r₀ : (a : A) → R a a)
          → (D : (a b : A) (r : R a b) → 𝓦 * )
          → (d : (a : A) → D a a (r₀ a) )
          → Σ[ f ∈ ((a b : A) (r : R a b) → D a b r) ] Π[ a ∈ A ] ((f a a (r₀ a)) ≡ (d a))

  -- only defined because I get stupid universe troubles otherwise
  Unbased' = (A : 𝓤 *) (R : A → A → 𝓥 *) (r₀ : (a : A) → R a a)
          → (D : (a b : A) (r : R a b) → (𝓤 ⊔ 𝓥 ⊔ 𝓦 ⁺) * )
          → (d : (a : A) → D a a (r₀ a) )
          → Σ[ f ∈ ((a b : A) (r : R a b) → D a b r) ] Π[ a ∈ A ] ((f a a (r₀ a)) ≡ (d a))


  Based   = (A : 𝓤 *) (R : A → A → 𝓥 *) (r₀ : (a : A) → R a a)
          → (a : A)
          → (C : (b : A) (r : R a b) → 𝓦 * )
          → (c : C a (r₀ a) )
          → Σ[ g ∈ ((b : A) (r : R a b) → C b r) ] (g a (r₀ a) ≡ c)

  Based→Unbased : Based → Unbased
  Based→Unbased ϕ A R r₀ D d = f , f-at-r₀ 
   where
    f : (a b : A) (r : R a b) → D a b r
    f a = fst (ϕ A R r₀ a (D a) (d a))

    f-at-r₀ : (a : A) → f a a (r₀ a) ≡ d a
    f-at-r₀ a = snd (ϕ A R r₀ a (D a) (d a))


  Unbased→Based : Unbased' → Based
  Unbased→Based Ψ A R r₀ a C c = g , g-at-r₀
   where
   
    D : (a b : A) (r : R a b) → ( 𝓤 ⊔ 𝓥 ⊔ 𝓦 ⁺ ) *
    D a b r = (C : (b' : A) (r : R a b') → 𝓦 *) → C a (r₀ a) → C b r
    d : (a : A) → D a a (r₀ a)
    d a C = id

    f : (a b : A) (r : R a b) (C : (b' : A) (r : R a b') → 𝓦 *) → C a (r₀ a) → C b r
    f = fst (Ψ A R r₀ D d)

    f-at-r₀ : (a : A) → f a a (r₀ a) ≡ d a
    f-at-r₀ = snd (Ψ A R r₀ D d)

    g : (b : A) (r : R a b) → C b r
    g b r = f a b r C c

    g-at-r₀ : g a (r₀ a) ≡ c
    g-at-r₀ = ap (λ - → - C c) (f-at-r₀ a) 


module _ (A B : 𝓤 *) where
  -- Two different ways to get from `f ≡ g` to `f ∼ g`: do they agree?
  happly₁ : (f g : A → B) (p : f ≡ g) → f ∼ g
  happly₁ f g p = 𝕁' (λ f g p → f ∼ g) (λ f x → refl (f x)) p

  happly₂ : (f g : A → B) (p : f ≡ g) → f ∼ g
  happly₂ f g p = tr (λ g → f ∼ g) p λ x → refl (f x)

  lol : (f g : A → B) (p : f ≡ g) →  happly₁ f g p ≡ happly₂ f g p
  -- lol f g p = refl (happly₁ f g p) -- not judgementally
  lol f f (refl f) = refl (happly₁ f f (refl f)) -- but propositionally
