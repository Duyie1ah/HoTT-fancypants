{-# OPTIONS --without-K --exact-split --safe #-}

module Exercises-01 where

open import Chapter-01 public

-- Ex. 1.1
pipe : {A B C : 𝓤 *} → (A → B) → (B → C) → (A → C)
pipe {A = A} {B = B} {C = C} f g = λ (a : A) → g (f a)
_&_ = pipe

&-assoc : {A B C D : 𝓤 *} (f : A → B) → (g : B → C) → (h : C → D)
        → (f & (g & h)) ≡ ((f & g) & h)
&-assoc f g h = refl ( f & (g & h))

-- Using = for "judgemental equality":
-- Let R = f & (g & h)
--     L = (f & g) & h
-- Then:
-- R x = (f & (g & h)) x = (g & h) (f x) = h (g (f x))
-- L x = ((f & g) & h) x = h ((f & g) x) = h (g (f x))
-- So that R = (λ x → R x) = (λ x → L x) = L
-- where the first and last are by eta conversion,
-- and the second since "equals can be replaced by equals"

-- Ex 1.5

-- simplified universe manipulation otherwise I can't get it to work: everything in bottom universe
𝔹-rec : (A : 𝓤₁ *) → A → A → (b : 𝔹) → A
𝔹-rec A if else 𝕥 = if 
𝔹-rec A if else 𝕗 = else 

_+'_ : (A : 𝓤₀ *) (B : 𝓤₀ *) → Set
_+'_  A B = Σ (𝔹-rec (𝓤₀ *) A B) 

inl' : {A B : 𝓤₀ *} (a : A) → A +' B
inl' a = 𝕥 , a

inr' : {A B : 𝓤₀ *} (b : B) → A +' B
inr' b = 𝕗 , b

+'-ind : {A : 𝓤₀ *} {B : 𝓤₀ *} (C : A +' B → 𝓦 *)
            → ((x : A) → C (inl' x))
            → ((y : B) → C (inr' y))
            → (v : A +' B) → C v

-- by pattern matching, and definitional equations hold by def
-- +'-ind {A = A} {B = B} C left right (𝕥 , a) = left a
-- +'-ind {A = A} {B = B} C left right (𝕗 , b) = right b
+'-ind {A = A} {B = B} C left right v = Σ-induction {X = 𝔹} {Y = λ (x : 𝔹) → 𝔹-rec (𝓤₀ *) A B x} {A = λ p → C p} Σ-base v
 where
  Σ-base : (x : 𝔹) → (y : 𝔹-rec (𝓤₀ *) A B x) → C (x , y)
  Σ-base x y = 𝔹-induction (λ (x : 𝔹) → (y : 𝔹-rec (𝓤₀ *) A B x) → C (x , y))
                            (λ a → left a)
                            (λ b → right b) x y

_×'_ : (A : 𝓤₀ *) (B : 𝓤₀ *) → Set
_×'_  A B = Π (𝔹-rec (𝓤₀ *) A B)

_,'_ :  {A B : 𝓤₀ *} → A → B → A ×' B
_,'_ {A = A} {B = B} a b = 𝔹-induction (𝔹-rec (𝓤₀ *) A B) a b


{-
×'-ind : {A B : 𝓤₀ *} → (C : A ×' B → 𝓦 *) → (c : (a : A) → (b : B) → C (a ,' b)) → (p : A ×' B) → C p
×'-ind {A = A} {B = B} C c p = c (p 𝕥) (p 𝕗)
-}


-- Ex 1.11

¬→¬² : {A : 𝓤 *} → A → ¬¬ A
¬→¬² a = λ x → x a

¬³→¬ : {A : 𝓤 *} → ¬¬¬ A → ¬ A
¬³→¬ ϕ a = ϕ (¬→¬² a)

-- Ex 1.12

module Ex-1-12 where

  i  : {A B : 𝓤 *} →  A → (B → A)
  ii : {A B : 𝓤 *} → A → ¬¬ A
  iii : {A B : 𝓤 *} → (¬ A) + (¬ B) → ¬ (A × B)

  i a b = a
  ii a = λ x → x a
  --iii (inl f) = f ∘ fst
  --iii (inr f) = f ∘ snd
  iii {A = A} {B = B} = +-induction (λ - → ¬ (A × B)) (λ f → f ∘ fst) (λ g → g ∘ snd) 

-- Ex 1.13
¬²LEM : {A : 𝓤 *} → ¬¬ (A + ¬ A)
-- need to construct ((A + (A → 𝟘)) → 𝟘) → 𝟘
-- that is, given f : (A + (A → 𝟘)) → 𝟘, need to construct elem of 𝟘
¬²LEM f = (f ∘ inr) (f ∘ inl)

-- this is actually not dependent on having 𝟘 as target type:  
lemma : {A B : 𝓤 *} → ((A + (A → B)) → B) → B
lemma {A = A} {B = B} f = h g
 where
  g : A → B
  h : (A → B) → B
  g = f ∘ inl
  h = f ∘ inr
