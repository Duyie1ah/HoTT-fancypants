
module HoTT where




  open import Spartan-MLTT public
  open import Basics public
  open import Based-Path-Induction public
  open import Chapter-01 public
  open import Exercises-01 public
  open import Chapter-02 public
  open import Exercises-02 public
  open import Chapter-03 public
  open import About-Sets public
  open import Retraction public
  open import FunExt-and-Contr public
  open import Exercises-03 public
  open import Chapter-04 public
  open import Chapter-05 public
  open import Chapter-10 public

  open import Rijke
