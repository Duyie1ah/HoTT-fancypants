{-# OPTIONS --without-K --exact-split --show-implicit #-}



module About-Sets where


open import Chapter-03 public

-- The way the HoTT book explains the negation of LEM was too confusing.
-- An easier idea for me is that:
-- * If `A` is not a set then by Hedberg its identity types (i.e. `a₀ ≡ a₁` for `a₀ a₁ : A`)
--   don't satisfy LEM.
-- * If furthermore `A` lies in the universe `𝓤`, then so do its identity types, which means that
--   `𝓤` cannot satisfy LEM.
-- But for this, we need Hedberg.
--
-- This is based on the 1lab page about sets.

-- LOL I'm giving up for now

-- We know that Hedberg implies sethood… does the converse hold?

𝕂 : (𝓣 : Universe)
  → (A : 𝓤 *) → (𝓤 ⊔ (𝓣 ⁺)) *
𝕂 𝓣 A = {x : A} → (C : x ≡ x → 𝓣 *)
     → (c : C (refl x)) → (p : x ≡ x) → C p

has𝕂→isSet : (A : 𝓤 *) → 𝕂 𝓤 A → A isSet
has𝕂→isSet A K x x (refl x) p = K (λ p → (refl x ≡ p)) (refl (refl x)) p
-- can be done using 𝕁 easily if I'm not mistaken
-- Note: we only need to use `𝕂` for the same universe as `A` lives in

isSet→Has𝕂 : (𝓣 : Universe) → (A : 𝓤 *) → A isSet → 𝕂 𝓣 A
isSet→Has𝕂 𝓣 A s {x} C c p = tr C (s x x (refl x) p) c
-- this is just transport using the fact that every path at `x` is equal to `refl x` :D 

postulate
  Rijke : {A : 𝓤 *} → (R : A → A → 𝓥 *)
      → ((x y : A) → (R x y) isProp)
      → ((x : A) → R x x)
      → ((x y : A) → R x y → x ≡ y)
      → ((x y : A) → (x ≡ y) ≃ R x y)

Rijke-Set : {A : 𝓤 *} → (R : A → A → 𝓥 *)
      → ((x y : A) → R x y isProp)
      → ((x : A) → R x x)
      → ((x y : A) →  R x y → x ≡ y)
      → A isSet
Rijke-Set {A = A} R props refle imple x y  = ≃-preserves-Prop ≃Rxy (props x y)
 where
  ≃Rxy : (x ≡ y) ≃ R x y
  ≃Rxy = Rijke {A = A} R props refle imple x y

Hedberg : 𝓤 hasHedberg
Hedberg {𝓤}  A id-lem = Rijke-Set {𝓤} {𝓤} {A = A} R props refle imple
 where
  R = λ x y → ¬¬ (x ≡ y)
  props = λ x y → ¬¬-isProp (x ≡ y)
  refle =  λ x → →-¬¬ (refl x) 
  imple : (x y : A) → R x y → x ≡ y
  imple x y = lem→dne {𝓤 = 𝓤} (x ≡ y) (id-lem x y)  

{- 
-- Recall how we phrased Hedberg (I don't think this is ideal, but let's go with it)
_hasHedberg : (𝓢 : Universe) →  𝓢 ⁺ *
𝓢 hasHedberg
        = (A : 𝓢 *)
        → ((x₀ x₁ : A) → (x₀ ≡ x₁) + ¬ (x₀ ≡ x₁))
        → A isSet
-- an element of type `Hedberg 𝓢` is a proof that Hedberg holds for `𝓢`
-}
